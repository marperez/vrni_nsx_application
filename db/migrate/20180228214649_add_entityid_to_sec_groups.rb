class AddEntityidToSecGroups < ActiveRecord::Migration[5.1]
  def change
    add_column :sec_groups, :object_entity_id, :string
  end
end
