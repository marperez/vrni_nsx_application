class AddNameToTier < ActiveRecord::Migration[5.1]
  def change
    add_column :tiers, :name, :string
  end
end
