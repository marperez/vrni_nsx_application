class AddCurrentToSettingsChildren < ActiveRecord::Migration[5.1]
  def change
    add_column :vrni_applications, :current, :boolean
    add_column :nsx_managers, :current, :boolean
  end
end
