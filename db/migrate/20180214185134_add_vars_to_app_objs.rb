class AddVarsToAppObjs < ActiveRecord::Migration[5.1]
  def change
    add_column :app_objs, :obj_type, :string
    add_column :app_objs, :obj_name, :string
  end
end
