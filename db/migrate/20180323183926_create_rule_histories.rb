class CreateRuleHistories < ActiveRecord::Migration[5.1]
  def change
    create_table :rule_histories do |t|
      t.string :name
      t.string :action
      t.string :direction
      t.string :pack_type
      t.string :user_id

      t.timestamps
    end
  end
end
