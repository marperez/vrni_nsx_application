class AddObjEntityIdToVms < ActiveRecord::Migration[5.1]
  def change
    add_column :vms, :obj_entity_id, :string
  end
end
