class CreateNestedSecGps < ActiveRecord::Migration[5.1]
  def change
    create_table :nested_sec_gps do |t|
      t.string :object_id
      t.string :object_type
      t.string :sec_group_id

      t.timestamps
    end
  end
end
