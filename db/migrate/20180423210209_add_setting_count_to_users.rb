class AddSettingCountToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :settings, :setting_count, :integer, default: 0
  end
end
