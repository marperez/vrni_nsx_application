class AddUserNameToRuleHistories < ActiveRecord::Migration[5.1]
  def change
    add_column :rule_histories, :username, :string
  end
end
