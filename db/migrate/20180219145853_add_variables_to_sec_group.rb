class AddVariablesToSecGroup < ActiveRecord::Migration[5.1]
  def change
    add_column :sec_groups, :object_id, :string
    add_column :sec_groups, :name, :string
  end
end
