class AddIdsToRuleHistoryChildren < ActiveRecord::Migration[5.1]
  def change
    add_column :destinations, :rule_history_id, :string
    add_column :sources, :rule_history_id, :string
    add_column :protocols, :rule_history_id, :string
    add_column :port_ranges, :rule_history_id, :string
    add_column :services, :rule_history_id, :string
  end
end
