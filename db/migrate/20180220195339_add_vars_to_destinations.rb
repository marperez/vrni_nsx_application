class AddVarsToDestinations < ActiveRecord::Migration[5.1]
  def change
    add_column :destinations, :dest_exclude, :string
    add_column :destinations, :dest_value, :string
    add_column :destinations, :dest_type, :string
    add_column :destinations, :dest_is_valid, :string
  end
end
