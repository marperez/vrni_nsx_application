class AddIdsToTempFireRule < ActiveRecord::Migration[5.1]
  def change
    add_column :temp_fire_rules, :sec_group_id, :string
    add_column :temp_fire_rules, :app_id, :string
  end
end
