class AddSugidToAppliedTos < ActiveRecord::Migration[5.1]
  def change
    add_column :applied_tos, :sug_rule_id, :string
  end
end
