class CreateSecGroupTemplates < ActiveRecord::Migration[5.1]
  def change
    create_table :sec_group_templates do |t|
      t.string :sec_group_id

      t.timestamps
    end
  end
end
