class AddEntityvarsToDestinations < ActiveRecord::Migration[5.1]
  def change
    add_column :destinations, :entity_id, :string
    add_column :destinations, :entity_type, :string
  end
end
