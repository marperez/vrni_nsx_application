class AddNameToSugRule < ActiveRecord::Migration[5.1]
  def change
    add_column :sug_rules, :name, :string
  end
end
