class AddSectionIdAndEtagToSecGroup < ActiveRecord::Migration[5.1]
  def change
  	add_column :sec_groups, :section_id, :string
  	add_column :sec_groups, :section_etag, :string
  end
end
