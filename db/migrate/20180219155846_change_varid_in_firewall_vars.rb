class ChangeVaridInFirewallVars < ActiveRecord::Migration[5.1]
  def change
    add_column :sources, :sug_rule_id, :string
    add_column :destinations, :sug_rule_id, :string
    add_column :port_ranges, :sug_rule_id, :string
  end
end
