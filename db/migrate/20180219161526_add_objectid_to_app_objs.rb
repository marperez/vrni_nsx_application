class AddObjectidToAppObjs < ActiveRecord::Migration[5.1]
  def change
    add_column :app_objs, :obj_id, :string
  end
end
