class AddUniquenessToSecTags < ActiveRecord::Migration[5.1]
  def change
  	add_index :sec_tags, [:tag_name, :app_obj_id], unique: true
  end
end
