class CreatePortRanges < ActiveRecord::Migration[5.1]
  def change
    create_table :port_ranges do |t|
  		t.string :fire_rule_id

      t.timestamps
    end
  end
end
