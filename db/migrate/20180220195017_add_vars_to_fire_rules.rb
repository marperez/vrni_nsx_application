class AddVarsToFireRules < ActiveRecord::Migration[5.1]
  def change
    add_column :fire_rules, :name, :string
    add_column :fire_rules, :action, :string
    add_column :fire_rules, :direction, :string
    add_column :fire_rules, :pack_type, :string
  end
end
