class AddProtocolNameToFlows < ActiveRecord::Migration[5.1]
  def change
    add_column :flows, :protocol_name, :string
  end
end
