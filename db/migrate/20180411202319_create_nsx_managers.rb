class CreateNsxManagers < ActiveRecord::Migration[5.1]
  def change
    create_table :nsx_managers do |t|
      t.string :ip_addr
      t.references :setting, foreign_key: true

      t.timestamps
    end
  end
end
