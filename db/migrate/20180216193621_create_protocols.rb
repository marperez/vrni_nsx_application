class CreateProtocols < ActiveRecord::Migration[5.1]
  def change
    create_table :protocols do |t|
  		t.string :fire_rule_id

      t.timestamps
    end
  end
end
