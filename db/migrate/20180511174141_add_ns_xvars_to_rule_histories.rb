class AddNsXvarsToRuleHistories < ActiveRecord::Migration[5.1]
  def change
    add_column :rule_histories, :rule_name, :string
    add_column :rule_histories, :section_name, :string
    add_column :rule_histories, :section_id, :string
    add_column :rule_histories, :section_etag, :string
    add_column :rule_histories, :undo_rule, :string
    add_column :rule_histories, :nsx_rule_id, :string
  end
end
