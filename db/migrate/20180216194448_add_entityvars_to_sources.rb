class AddEntityvarsToSources < ActiveRecord::Migration[5.1]
  def change
    add_column :sources, :entity_id, :string
    add_column :sources, :entity_type, :string
  end
end
