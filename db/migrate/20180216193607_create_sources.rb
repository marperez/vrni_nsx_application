class CreateSources < ActiveRecord::Migration[5.1]
  def change
    create_table :sources do |t|
  		t.string :fire_rule_id

      t.timestamps
    end
  end
end
