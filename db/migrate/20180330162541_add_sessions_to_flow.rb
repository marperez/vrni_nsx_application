class AddSessionsToFlow < ActiveRecord::Migration[5.1]
  def change
    add_column :flows, :sessions, :string
  end
end
