class AddHistoryIdToAppliedTos < ActiveRecord::Migration[5.1]
  def change
    add_column :applied_tos, :rule_history_id, :string
  end
end
