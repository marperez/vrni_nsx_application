class AddObjEntityIdToAppObjs < ActiveRecord::Migration[5.1]
  def change
    add_column :app_objs, :obj_entity_id, :string
  end
end
