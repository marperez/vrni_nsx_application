class AddVmidToTempFireRule < ActiveRecord::Migration[5.1]
  def change
    add_column :temp_fire_rules, :rule_vm_id, :string
  end
end
