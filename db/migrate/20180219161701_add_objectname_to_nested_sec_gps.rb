class AddObjectnameToNestedSecGps < ActiveRecord::Migration[5.1]
  def change
    add_column :nested_sec_gps, :object_name, :string
  end
end
