class AddVarsToAppliedTos < ActiveRecord::Migration[5.1]
  def change
    add_column :applied_tos, :apl_name, :string
    add_column :applied_tos, :apl_value, :string
    add_column :applied_tos, :apl_type, :string
  end
end
