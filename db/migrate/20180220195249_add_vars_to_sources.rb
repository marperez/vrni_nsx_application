class AddVarsToSources < ActiveRecord::Migration[5.1]
  def change
    add_column :sources, :sour_exclude, :string
    add_column :sources, :sour_value, :string
    add_column :sources, :sour_type, :string
    add_column :sources, :sour_is_valid, :string
  end
end
