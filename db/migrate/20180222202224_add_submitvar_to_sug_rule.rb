class AddSubmitvarToSugRule < ActiveRecord::Migration[5.1]
  def change
    add_column :sug_rules, :submit_sug, :boolean
  end
end
