class AddIdForNewlyCreatedSecGpSecGroup < ActiveRecord::Migration[5.1]
  def change
    add_column :sec_groups, :newly_created, :boolean
  end
end
