class RemoveColumnsFromRuleHistories < ActiveRecord::Migration[5.1]
  def change
    remove_column :rule_histories, :name, :string
    remove_column :rule_histories, :direction, :string
    remove_column :rule_histories, :pack_type, :string
    add_column :rule_histories, :source, :string
    add_column :rule_histories, :destination, :string
    add_column :rule_histories, :protocol, :string
    add_column :rule_histories, :port, :string
  end
end
