class AddUseridToSetting < ActiveRecord::Migration[5.1]
  def change
    add_column :settings, :user_id, :string
  end
end
