class AddStartstopvarsToPortRanges < ActiveRecord::Migration[5.1]
  def change
    add_column :port_ranges, :startport, :string
    add_column :port_ranges, :endport, :string
  end
end
