class AddIndexToName < ActiveRecord::Migration[5.1]
  def change
    add_index :fire_rules, :name
	add_index :applied_tos, :apl_name
  end
end
