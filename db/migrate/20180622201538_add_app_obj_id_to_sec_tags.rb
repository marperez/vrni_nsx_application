class AddAppObjIdToSecTags < ActiveRecord::Migration[5.1]
  def change
    add_column :sec_tags, :app_obj_id, :string
  end
end
