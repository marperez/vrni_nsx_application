class AddUseridToSecGroups < ActiveRecord::Migration[5.1]
  def change
    add_column :sec_groups, :user_id, :string
  end
end
