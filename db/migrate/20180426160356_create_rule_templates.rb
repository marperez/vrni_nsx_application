class CreateRuleTemplates < ActiveRecord::Migration[5.1]
  def change
    create_table :rule_templates do |t|
      t.string :name

      t.timestamps
    end
  end
end
