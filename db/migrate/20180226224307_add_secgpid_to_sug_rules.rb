class AddSecgpidToSugRules < ActiveRecord::Migration[5.1]
  def change
    add_column :sug_rules, :sec_group_id, :string
  end
end
