class CreateUserSecGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :user_sec_groups do |t|
      t.string :user_id
      t.string :sec_group_id

      t.timestamps
    end
  end
end
