class AddTaskgroupnameToFlows < ActiveRecord::Migration[5.1]
  def change
    add_column :flows, :task_group_name, :string
    add_column :flows, :immediate_parent_name, :string
  end
end
