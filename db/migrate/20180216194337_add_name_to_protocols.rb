class AddNameToProtocols < ActiveRecord::Migration[5.1]
  def change
    add_column :protocols, :name, :string
  end
end
