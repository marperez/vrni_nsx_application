class AddRuleTemplateIdToFireRules < ActiveRecord::Migration[5.1]
  def change
    add_column :fire_rules, :rule_template_id, :string
  end
end
