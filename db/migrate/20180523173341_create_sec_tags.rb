class CreateSecTags < ActiveRecord::Migration[5.1]
  def change
    create_table :sec_tags do |t|
      t.string :tag_name

      t.timestamps
    end
  end
end
