class CreateFlows < ActiveRecord::Migration[5.1]
  def change
    create_table :flows do |t|
      t.string :osi_layer
      t.string :blocked
      t.string :protocol
      t.string :sent_bytes
      t.string :received_bytes
      t.string :source
      t.string :destination
      t.string :port

      t.timestamps
    end
  end
end
