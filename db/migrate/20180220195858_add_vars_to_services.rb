class AddVarsToServices < ActiveRecord::Migration[5.1]
  def change
    add_column :services, :serv_name, :string
    add_column :services, :serv_value, :string
    add_column :services, :serv_type, :string
    add_column :services, :serv_is_valid, :string
  end
end
