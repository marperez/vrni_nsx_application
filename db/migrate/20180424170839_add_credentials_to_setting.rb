class AddCredentialsToSetting < ActiveRecord::Migration[5.1]
  def change
    add_column :settings, :nsx_username, :string
    add_column :settings, :vrni_username, :string
    add_column :settings, :nsx_password, :string
    add_column :settings, :vrni_password, :string
  end
end
