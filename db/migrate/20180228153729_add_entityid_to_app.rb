class AddEntityidToApp < ActiveRecord::Migration[5.1]
  def change
    add_column :apps, :app_entity_id, :string
  end
end
