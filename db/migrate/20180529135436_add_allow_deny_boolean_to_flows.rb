class AddAllowDenyBooleanToFlows < ActiveRecord::Migration[5.1]
  def change
    add_column :flows, :allow_deny, :boolean
  end
end
