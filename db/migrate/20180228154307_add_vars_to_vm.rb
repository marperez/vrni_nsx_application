class AddVarsToVm < ActiveRecord::Migration[5.1]
  def change
    add_column :vms, :vm_moid, :string
    add_column :vms, :vm_name, :string
  end
end
