class CreateVrniApplications < ActiveRecord::Migration[5.1]
  def change
    create_table :vrni_applications do |t|
      t.string :vrni_addr
      t.references :setting, foreign_key: true

      t.timestamps
    end
  end
end
