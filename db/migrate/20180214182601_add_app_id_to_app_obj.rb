class AddAppIdToAppObj < ActiveRecord::Migration[5.1]
  def change
  	create_table :app_objs do |t|
  		t.string :tier_id

  		t.timestamps
  	end
  end
end
