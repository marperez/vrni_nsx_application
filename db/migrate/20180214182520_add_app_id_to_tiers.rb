class AddAppIdToTiers < ActiveRecord::Migration[5.1]
  def change
    add_column :tiers, :app_id, :string
  end
end
