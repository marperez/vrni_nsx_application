class AddNameToDestinations < ActiveRecord::Migration[5.1]
  def change
    add_column :destinations, :dest_name, :string
  end
end
