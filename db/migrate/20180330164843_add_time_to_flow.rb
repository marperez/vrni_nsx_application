class AddTimeToFlow < ActiveRecord::Migration[5.1]
  def change
    add_column :flows, :time_out, :string
    add_column :flows, :time_rec, :string
  end
end
