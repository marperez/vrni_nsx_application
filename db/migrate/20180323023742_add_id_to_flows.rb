class AddIdToFlows < ActiveRecord::Migration[5.1]
  def change
    add_column :flows, :nested_sec_gp_id, :string
  end
end
