# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180622204611) do

  create_table "app_objs", force: :cascade do |t|
    t.string "tier_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "obj_type"
    t.string "obj_name"
    t.string "obj_id"
    t.string "sec_group_id"
    t.string "nested_sec_gp_id"
    t.string "obj_entity_id"
  end

  create_table "applied_tos", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "apl_name"
    t.string "apl_value"
    t.string "apl_type"
    t.string "fire_rule_id"
    t.string "temp_fire_rule_id"
    t.string "sug_rule_id"
    t.string "rule_history_id"
    t.index ["apl_name"], name: "index_applied_tos_on_apl_name"
  end

  create_table "apps", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "app_vrni_id"
    t.string "app_entity_id"
  end

  create_table "destinations", force: :cascade do |t|
    t.string "fire_rule_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "entity_id"
    t.string "entity_type"
    t.string "sug_rule_id"
    t.string "dest_exclude"
    t.string "dest_value"
    t.string "dest_type"
    t.string "dest_is_valid"
    t.string "temp_fire_rule_id"
    t.string "dest_name"
    t.string "rule_history_id"
  end

  create_table "fire_rules", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "action"
    t.string "direction"
    t.string "pack_type"
    t.string "rule_template_id"
    t.index ["name"], name: "index_fire_rules_on_name"
  end

  create_table "flows", force: :cascade do |t|
    t.string "osi_layer"
    t.string "blocked"
    t.string "protocol"
    t.string "sent_bytes"
    t.string "received_bytes"
    t.string "source"
    t.string "destination"
    t.string "port"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "nested_sec_gp_id"
    t.string "sessions"
    t.string "time_out"
    t.string "time_rec"
    t.string "sec_group_id"
    t.string "task_group_name"
    t.string "immediate_parent_name"
    t.boolean "allow_deny"
    t.string "protocol_name"
  end

  create_table "nested_sec_gps", force: :cascade do |t|
    t.string "object_id"
    t.string "object_type"
    t.string "sec_group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "object_name"
    t.string "object_entity_id"
  end

  create_table "nsx_managers", force: :cascade do |t|
    t.string "ip_addr"
    t.integer "setting_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "current"
    t.index ["setting_id"], name: "index_nsx_managers_on_setting_id"
  end

  create_table "port_ranges", force: :cascade do |t|
    t.string "fire_rule_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "startport"
    t.string "endport"
    t.string "sug_rule_id"
    t.string "temp_fire_rule_id"
    t.string "rule_history_id"
  end

  create_table "protocols", force: :cascade do |t|
    t.string "fire_rule_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "sug_rule_id"
    t.string "temp_fire_rule_id"
    t.string "rule_history_id"
  end

  create_table "rule_histories", force: :cascade do |t|
    t.string "action"
    t.string "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "source"
    t.string "destination"
    t.string "protocol"
    t.string "port"
    t.string "username"
    t.string "rule_name"
    t.string "section_name"
    t.string "section_id"
    t.string "section_etag"
    t.string "undo_rule"
    t.string "nsx_rule_id"
  end

  create_table "rule_templates", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sec_group_templates", force: :cascade do |t|
    t.string "sec_group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "rule_template_id"
  end

  create_table "sec_groups", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "object_id"
    t.string "name"
    t.string "object_entity_id"
    t.boolean "newly_created"
    t.string "user_id"
    t.string "section_id"
    t.string "section_etag"
  end

  create_table "sec_tags", force: :cascade do |t|
    t.string "tag_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "app_obj_id"
    t.index ["tag_name", "app_obj_id"], name: "index_sec_tags_on_tag_name_and_app_obj_id", unique: true
  end

  create_table "sec_tags", force: :cascade do |t|
    t.string "tag_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "services", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "serv_name"
    t.string "serv_value"
    t.string "serv_type"
    t.string "serv_is_valid"
    t.string "fire_rule_id"
    t.string "temp_fire_rule_id"
    t.string "sug_rule_id"
    t.string "rule_history_id"
  end

  create_table "settings", force: :cascade do |t|
    t.boolean "current"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "user_id"
    t.integer "setting_count", default: 0
    t.string "nsx_username"
    t.string "vrni_username"
    t.string "nsx_password"
    t.string "vrni_password"
  end

  create_table "sources", force: :cascade do |t|
    t.string "fire_rule_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "entity_id"
    t.string "entity_type"
    t.string "sug_rule_id"
    t.string "sour_exclude"
    t.string "sour_value"
    t.string "sour_type"
    t.string "sour_is_valid"
    t.string "temp_fire_rule_id"
    t.string "sour_name"
    t.string "rule_history_id"
  end

  create_table "sug_rules", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "action"
    t.boolean "submit_sug"
    t.string "sec_group_id"
    t.string "tier_id"
    t.string "name"
  end

  create_table "temp_fire_rules", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "action"
    t.string "direction"
    t.string "pack_type"
    t.string "rule_vm_id"
    t.string "sec_group_id"
    t.string "app_id"
  end

  create_table "tiers", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "app_id"
    t.string "tier_vrni_id"
    t.string "sec_gp_moid"
  end

  create_table "user_apps", force: :cascade do |t|
    t.string "user_id"
    t.string "app_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_groups", force: :cascade do |t|
    t.string "user_id"
    t.string "sec_group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_sec_groups", force: :cascade do |t|
    t.string "user_id"
    t.string "sec_group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_confirmation"
    t.boolean "admin"
    t.string "password_hash"
  end

  create_table "vms", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "vm_moid"
    t.string "vm_name"
    t.string "obj_entity_id"
    t.string "vm_type"
  end

  create_table "vrni_applications", force: :cascade do |t|
    t.string "vrni_addr"
    t.integer "setting_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "current"
    t.index ["setting_id"], name: "index_vrni_applications_on_setting_id"
  end

end
