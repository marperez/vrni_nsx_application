class NotificationsMailer < ApplicationMailer


	default from: User.all.where(admin: true).first.email

	def task_reminder(user)	
		@user = user
		# I am overriding the 'to' default
		from_user = User.all.where(admin: true).first
		from_email = from_user.email
		puts(mail(from: from_email, to: @user.email, subject: "NSX/VRNI Dashboard open Tasks", reply_to: 'noreply@null.com'))
	end

	def task_sucessfully_completed_firewall_rules(user)
		# I am overriding the 'to' default
		@user = user
		from_user = User.all.where(admin: true).first
		from_email = from_user.email
		puts(mail(from: from_email, to: user.email, subject: "NSX/VRNI Successful Firewall Section Deploy", reply_to: 'noreply@null.com'))
	end

end
