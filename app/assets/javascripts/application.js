// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require popper
//= require turbolinks
//= require Chart.bundle
//= require chartkick
//= require jquery
//= require jquery_ujs
//= require_tree .

		
function myLoop (num, element) {         
	setTimeout(function () {
		if (num <= 1 & num > 0) { 
			opactiyfunction(num, element);
			num -= .01;
		   	// alert(num);
		   	myLoop(num, element);
		} else {
			element.html = "";
			element.css("display", "none")
		}
	}, 100)
	setTimeout(function() {}, 5000);
}

function opactiyfunction(num, element){
	element.opacity = num;
}

function myFunction(element) {
	if ($(element).hasClass('no_bk_color')) {
		$(element).removeClass('no_bk_color');
		$(element).addClass('bk_green'); 
		$(element).children(".js_target").css("color", "var(--white-color)");
	} else {
		$(element).removeClass('bk_green');
		$(element).addClass('no_bk_color');
		$(element).children(".js_target").css("color", "var(--black-color)");
	}
}


function openModal(element) {
	$('.modal').css("display", "block !important");
}

function closeModal(element) {
	$('.modal').css("display", "none !important");
}

function login_signup_ui(element) {
	if ($(element).hasClass('login_area')) {
		$('.signup_area').removeClass("selected").addClass('unselected');
		$(element).addClass('selected').removeClass('unselected');
	} else {
		$('.login_area').removeClass("selected").addClass('unselected');
		$(element).addClass('selected').removeClass('unselected');
	}
}

function attachSpinnerOnExistingIcon(element) {
	$(element).children("i").addClass('fa-spinner fa-spin');
}

function colorHighlightBlue(element) {
	var text_items = element.querySelectorAll('a div p')
	text_items.forEach(function(item){
		item.style.color = "#5AAEE2";
	});
}

function colorHighlightWhite(element) {
	var text_items = element.querySelectorAll('a div p')
	text_items.forEach(function(item){
		item.style.color = "#fff";
	});
}

$(document).on('turbolinks:load', function() {


	var nav_height = $('.sug_rule_nav_overlay').offset();
	var nav_target = $('.sug_rule_nav_overlay');

	$('.error_message_popup_text').change(function(){
		alert("sa");
		// if ($('#notice').hasClass('hide_element')) {
		// 	alert("show");
		// 	$(this).removeClass('hide_element');
		// } else {
		// 	alert("hide");
		// 	$(this).addClass('hide_element');
		// }
	});

	$(window).on('scroll', function(){
	    if ($(window).scrollTop() > nav_height) {
	        nav_target.addClass('navbar-fixed-top');
	    }
	    else {
	        nav_target.removeClass('navbar-fixed-top');
	    }
	});

	$('.cover_page').toggle();

	$('.upload_button').on('click', function(){
		$("#import_app").removeClass("do_not_show");
	});

	$('.nav_sys_sec_groups_container').scrollspy({ target: '#nav_sys_sec_groups_header'})

	$('.dropdown-menu').on("click.bs.dropdown", function (e) { 
		e.stopPropagation(); 
	});

	$('.user_import_popup').on("click", function() {
		$(".user_import_app_modal").removeClass("user_import_hidden");
		$(".user_import_app_modal").addClass("show");
	});

	$('.user_import_submit').on("click", function() {
		$(".user_import_app_modal").addClass("user_import_hidden");
		$(".user_import_app_modal").removeClass("show");
	});

	$('#booked').click(function(){
		$.ajax({
			type : 'GET',
			url : '/sgp_task/:task_id/:user_id/add_a_sec_gp_to_user.js.erb',
			dataType : 'script'
		});
		return false;
	});

	$(function () {
  		$('[data-toggle="tooltip"]').tooltip({
    	container : 'body'
  		});
	});

});
