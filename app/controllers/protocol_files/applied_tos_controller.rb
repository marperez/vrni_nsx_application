module ProtocolFiles
  class AppliedTosController < ApplicationController
  before_action :set_applied_to, only: [:show, :edit, :update, :destroy]

  # GET /applied_tos
  # GET /applied_tos.json
  def index
    @applied_tos = AppliedTo.all
  end

  # GET /applied_tos/1
  # GET /applied_tos/1.json
  def show
  end

  # GET /applied_tos/new
  def new
    @applied_to = AppliedTo.new
  end

  # GET /applied_tos/1/edit
  def edit
  end

  # POST /applied_tos
  # POST /applied_tos.json
  def create
    @applied_to = AppliedTo.new(applied_to_params)

    respond_to do |format|
      if @applied_to.save
        format.html { redirect_to @applied_to, notice: 'Applied to was successfully created.' }
        format.json { render :show, status: :created, location: @applied_to }
      else
        format.html { render :new }
        format.json { render json: @applied_to.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /applied_tos/1
  # PATCH/PUT /applied_tos/1.json
  def update
    respond_to do |format|
      if @applied_to.update(applied_to_params)
        format.html { redirect_to @applied_to, notice: 'Applied to was successfully updated.' }
        format.json { render :show, status: :ok, location: @applied_to }
      else
        format.html { render :edit }
        format.json { render json: @applied_to.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /applied_tos/1
  # DELETE /applied_tos/1.json
  def destroy
    @applied_to.destroy
    respond_to do |format|
      format.html { redirect_to applied_tos_url, notice: 'Applied to was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_applied_to
      @applied_to = AppliedTo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def applied_to_params
      params.fetch(:applied_to, {})
    end
end
end