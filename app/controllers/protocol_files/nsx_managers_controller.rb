module ProtocolFiles
  class NsxManagersController < ApplicationController
    before_action :set_nsx_manager, only: [:show, :edit, :update, :destroy]

    def index
      @nsx_managers = NsxManager.all
    end

    def new
      @nsx_manager = NsxManager.new
    end

    def edit
    end

    def current
      @nsx = NsxManager.find_by_id(params[:id])

      increased = false
      current_user.setting.nsx_managers.all.each do |x|
        # if the nsx ip address exists and is current or not set (then decrease or increase the count)
        if (x.current == true)        
          x.current = false
        elsif (x.id == @nsx.id)
          x.current = true
          increased = true
        end
        x.save
      end

      within = false
      current_user.setting.vrni_applications.all.each do |x|
        if (x.current == true)        
          within = true
        end
      end

      if within == true && increased == true
        current_user.setting.setting_count = 2
      elsif (within != true && increased == true) || (within == true && increased != true)
        current_user.setting.setting_count = 1
      elsif (within != true && increased != true)
        current_user.setting.setting_count = 0
      end

      current_user.setting.save
      @nsx.save

      @type = "nsx"

      respond_to() do |format|
        format.js { 
            render :template => 'admin_files/settings/settings_resp.js.erb',
            :layout => false
        }
      end
    end

    def create
      @nsx_manager = NsxManager.new
      @nsx_manager.ip_addr = params[:ip_addr]
      @nsx_manager.current = false
      current_user.setting.nsx_managers << @nsx_manager
      current_user.save
      @type = "nsx"

      respond_to() do |format|
        format.js { 
            render :template => 'admin_files/settings/settings_resp.js.erb',
            :layout => false
        }
      end
    end

    def update
      respond_to do |format|
        if @nsx_manager.update(nsx_manager_params)
          format.html { redirect_to @nsx_manager, notice: 'Nsx manager was successfully updated.' }
          format.json { render :show, status: :ok, location: @nsx_manager }
        else
          format.html { render :edit }
          format.json { render json: @nsx_manager.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
      nsx = NsxManager.find_by_id(params[:id])

      if nsx.current == true # remember both the nsx info and vrni need to be selected to proceed past the settings page
        current_user.setting.setting_count -= 1
      end

      current_user.setting.save

      nsx.destroy

      @type = "nsx"

      respond_to() do |format|
        format.js { 
            render :template => 'admin_files/settings/settings_resp.js.erb',
            :layout => false
        }
      end
    end

    private
      def set_nsx_manager
        @nsx_manager = NsxManager.find(params[:id])
      end

      def nsx_manager_params
        params.require(:nsx_manager).permit(:ip_addr, :setting_id)
      end
  end
end