module ProtocolFiles
  class PortRangesController < ApplicationController
    before_action :set_port_range, only: [:show, :edit, :update, :destroy]

    # GET /port_ranges
    # GET /port_ranges.json
    def index
      @port_ranges = PortRange.all
    end

    # GET /port_ranges/1
    # GET /port_ranges/1.json
    def show
    end

    # GET /port_ranges/new
    def new
      @port_range = PortRange.new
    end

    # GET /port_ranges/1/edit
    def edit
    end


    def add_to_service_table
      @type = "service"
      # what about non UDP TCP ?

      byebug
      sysobj = Vm.find_by(id: params[:vmware_obj_id])
      @which_controller = params[:which_controller]

      case(@which_controller)
        when "rule_template"
          @rule = FireRule.find_by_id(params[:rule_id])
          @template_id = params[:template_id]

          serv_type = sysobj.vm_type
          serv_value = sysobj.vm_moid
          serv_name = sysobj.vm_name
          serv_valid = true

          protocol = Service.new(serv_type: serv_type, serv_value: serv_value, serv_name: serv_name, serv_is_valid: serv_valid)
          @rule.services << protocol
          @rule.save

          respond_to() do |format|
            format.js { render :template => 'layouts/new_rule_js/reload_rule_columns.js.erb'}
          end

        when "sec_group"
          @suggested = SugRule.find_by_id(session[:selected_suggestion])
          if @suggested.nil?
            @suggested = SugRule.find_by_id(session[:clone_suggestion_id])
          end

          serv_type = sysobj.vm_type
          serv_value = sysobj.vm_moid
          serv_name = sysobj.vm_name
          serv_valid = true
      
          @rule = SugRule.find_by_id(params[:rule_id])
          
          protocol = Service.new(serv_type: serv_type, serv_value: serv_value, serv_name: serv_name, serv_is_valid: serv_valid)
         
          @suggested.services << protocol
          @suggested.save

          byebug

          respond_to() do |format|
            format.js { render :template => 'layouts/new_rule_js/reload_rule_columns.js.erb'}
          end
      end
    end


    def add_to_ports_column # connected to the method above
      startport = params[:source]
      endport = params[:destination]
      @type = "service"
      
      @which_controller = params[:which_controller]
      case @which_controller
        when "sec_group"

          @rule = SugRule.find_by_id(params[:rule_id])

          @suggested = SugRule.find_by_id(session[:clone_suggestion_id])

          port_range = PortRange.new(startport: startport, endport: endport)
          @suggested.port_ranges << port_range
          @suggested.save

          respond_to() do |format|
            format.js { 
              render :template => 'layouts/new_rule_js/reload_rule_columns.js.erb',
              :layout => false
            }
          end
        when "rule_template"


          @rule = FireRule.find_by_id(params[:rule_id])

          @template_id = params[:template_id]
          port_range = PortRange.new(startport: startport, endport: endport)
          @rule.port_ranges << port_range

          respond_to() do |format|
            format.js { 
              render :template => 'fire_wall_files/rule_templates/create_rule_table_cells.js.erb',
              :layout => false
            }
          end
      end
    end

    # POST /port_ranges
    # POST /port_ranges.json
    def create
      @port_range = PortRange.new(port_range_params)

      respond_to do |format|
        if @port_range.save
          format.html { redirect_to @port_range, notice: 'Port range was successfully created.' }
          format.json { render :show, status: :created, location: @port_range }
        else
          format.html { render :new }
          format.json { render json: @port_range.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /port_ranges/1
    # PATCH/PUT /port_ranges/1.json
    def update
      respond_to do |format|
        if @port_range.update(port_range_params)
          format.html { redirect_to @port_range, notice: 'Port range was successfully updated.' }
          format.json { render :show, status: :ok, location: @port_range }
        else
          format.html { render :edit }
          format.json { render json: @port_range.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /port_ranges/1
    # DELETE /port_ranges/1.json
    def destroy
      @port_range.destroy
      respond_to do |format|
        format.html { redirect_to port_ranges_url, notice: 'Port range was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_port_range
        @port_range = PortRange.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def port_range_params
        params.fetch(:port_range, {})
      end
    end
end