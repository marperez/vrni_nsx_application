module SecurityFiles
  class NestedSecGpsController < ApplicationController
    before_action :set_nested_sec_gp, only: [:show, :edit, :update, :destroy]

    # GET /nested_sec_gps
    # GET /nested_sec_gps.json
    def index
      @nested_sec_gps = NestedSecGp.all
    end

    # GET /nested_sec_gps/1
    # GET /nested_sec_gps/1.json
    def show
    end

    # GET /nested_sec_gps/new
    def new
      @nested_sec_gp = NestedSecGp.new
    end

    # GET /nested_sec_gps/1/edit
    def edit
    end

    # POST /nested_sec_gps
    # POST /nested_sec_gps.json
    def create
      @nested_sec_gp = NestedSecGp.new(nested_sec_gp_params)

      respond_to do |format|
        if @nested_sec_gp.save
          format.html { redirect_to @nested_sec_gp, notice: 'Nested sec gp was successfully created.' }
          format.json { render :show, status: :created, location: @nested_sec_gp }
        else
          format.html { render :new }
          format.json { render json: @nested_sec_gp.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /nested_sec_gps/1
    # PATCH/PUT /nested_sec_gps/1.json
    def update
      respond_to do |format|
        if @nested_sec_gp.update(nested_sec_gp_params)
          format.html { redirect_to @nested_sec_gp, notice: 'Nested sec gp was successfully updated.' }
          format.json { render :show, status: :ok, location: @nested_sec_gp }
        else
          format.html { render :edit }
          format.json { render json: @nested_sec_gp.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /nested_sec_gps/1
    # DELETE /nested_sec_gps/1.json
    def destroy
      @nested_sec_gp.destroy
      respond_to do |format|
        format.html { redirect_to nested_sec_gps_url, notice: 'Nested sec gp was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_nested_sec_gp
        @nested_sec_gp = NestedSecGp.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def nested_sec_gp_params
        params.require(:nested_sec_gp).permit(:object_id, :object_type, :sec_group_id)
      end
  end
end