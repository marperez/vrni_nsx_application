module SecurityFiles
  class SecGroupsController < ApplicationController
    before_action :set_sec_group, only: [:show, :edit, :update, :destroy]
    @sec_group = " "
    @type = " "

    def index
      Vm.find_all_sys_objs(current_user)

    # find all security groups that VRNI and NSX agree on
      SecGroup.retrieve_sec_groups(current_user)

      FireRule.insert_all_fire_rules(current_user, "")
      # FireRuleImport.perform_async(current_user)
      @templates = RuleTemplate.all
      @sec_groups = SecGroup.all
      @temp_rules =  TempFireRule.all
      @universal_rules = FireRule.get_rules_outside_of_sec_groups
      # @universal_rules = FireRule.includes(:applied_tos).where("applied_tos.apl_value = 'DISTRIBUTED_FIREWALL'") 
      # "select all from fire_rules inner join applied"
      #   ActiveRecord::Base.connection.execute(insert_statement[0...-1])
      # @associated_rules = []

      @sys_objs = nil
      if params[:service]
        @sys_objs = Vm.search(params[:service]).order("vm_name DESC")
        session[:selected_table] = "service"
        respond_to() do |format|
          format.js { 
            render :template => 'edit_sug_source_dest_search.js.erb',
            :layout => false
          }
        end
      else
        session[:current_group_id] = ""
      end
    end

    def add_sec_to_user

      task = SecGroup.find_by_id(params[:task_id])
      @user = User.find_by_id(params[:user_id])

      @user.sec_groups << task
      @user.save

      @type = "added_task"

      respond_to() do |format|
        format.js
      end

    end

    def show
    end

    def new
      @sec_group = SecGroup.new
    end

    def edit
    end

    def override_or_create_rules

      @sec_group = SecGroup.find_by_id(params[:id]) # Let's get a reference to the user security group relationship

      session[:current_group_id] = @sec_group.id

      current_user.sec_groups << @sec_group unless current_user.sec_groups.include?(@sec_group)

      @objects = SecGroup.includes(:app_objs).where(app_objs: { sec_group_id: params[:id]} )

    # delete associated firewall rules to grab them fresh from NSX
      @sec_group.temp_fire_rules.delete_all 

      @associated = FireRule.insert_all_fire_rules(current_user, @sec_group.name)
      # @associated = FireRule.get_fire_rules_for_sec_group(@objects, @sec_group, current_user)

      @sug_rules = SugRule.create_sug_rules(params[:id], "sec_group", current_user) # create the suggested rules
      
      Flow.find_flows_for_a_sec_group(@sec_group, current_user) # find the flows

      flow_select_statement = "id, allow_deny, protocol_name, source, destination, port, sum(sent_bytes) as sum_sent_bytes, sum(received_bytes) as sum_rec_bytes, sum(sessions) sum_sessions, min(time_out) as earliest_outbound, max(time_rec) as latest_inbound"
      where_clause = "sec_group_id = %s" % [@sec_group.id]
      @group_flows = Flow.where(where_clause).select(flow_select_statement).group(:destination, :source, :port, :protocol_name)

      @filter_array = []

      respond_to() do |format|
        format.js
      end
    end

    def get_assoc_fw_rules
      @sec_group = current_user.sec_groups.find_by_id(params[:id]) # Let's get a reference to the user security group relationship
      session[:current_group_id] = @sec_group.id
      
      @group_flows = @sec_group.flows.select("id, protocol_name, source, destination, port, sum(sent_bytes) as sum_sent_bytes, sum(received_bytes) as sum_rec_bytes, sum(sessions) as sum_sessions, min(time_out) as earliest_outbound, max(time_rec) as latest_inbound").group("source, destination, port, protocol_name")

      @associated_rules = FireRule.insert_all_fire_rules(current_user, @sec_group.name)

      SecGroup.retrieve_sec_groups(current_user) # find all security groups that VRNI and NSX agree on
      @sug_rules = @sec_group.sug_rules.all
      respond_to() do |format|
        format.js
      end
    end

    def create_sg

      SecGroup.create(name: params[:name])

      respond_to() do |format|
        format.js { 
          render :template => 'sec_groups/left_nav/reload_all_sec_groups.js.erb',
          :layout => false
        }
      end      
    end

    def delete_assoc_sug_rule
      @rule = SugRule.find_by_id(params[:id])
      id = @rule.id.clone
      @html_row_id = "sug_rule_row_" + id.to_s

      SugRule.delete_associated_rule(@rule)

      respond_to() do |format|
        format.js
      end
    end

    def user_sec_group_destroy # from local database: undesignate user's selected task
      @sec_group = SecGroup.find_by_id(params[:id])

      User.current_user.sec_groups.delete(@sec_group)

      @origin = "user_sec_groups"

      respond_to() do |format|
        format.js {
          render :template => "sec_groups/delete_security_group",
          :layout => false
        }
      end
    end

    def delete_security_group # from NSX Manager
      @sec_group = SecGroup.find_by_id(params[:id])

      ipaddr = Setting.current_nsx # get current user's active nsx

      # general
      auth = {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
      headers = {'Content-Type' => 'application/xml'}

      # delete the security group's distributed firewall section and then
      if @sec_group.section_id != ""
        url = "https://%s/api/4.0/firewall/globalroot-0/config/layer3sections/%s" % [ipaddr, @sec_group.section_id]
        delete_sec_tag_resp = HTTParty.delete(url, :basic_auth => auth)
        sec_group_section_var = delete_sec_tag_resp.response.inspect.scan(/\d/).join('')
        if sec_group_section_var == "201" || sec_group_section_var == "200"
          @notification = "%s and the associated DWF Section" % [@notification]
        end
      end

      # delete group on nsx and then locally
      if @sec_group.object_id.nil?
        id = Vm.find_by(vm_name: group.name)
        id = id.vm_moid
        url = "https://%s/api/2.0/services/securitygroup/%s" % [ipaddr, id]
      else
        url = "https://%s/api/2.0/services/securitygroup/%s" % [ipaddr, @sec_group.object_id.to_s]
      end
     
      request_base_resp = HTTParty.delete(url, :headers => headers, :basic_auth => auth)
      sec_group_var = request_base_resp.response.inspect.scan(/\d/).join('')

      doc = Nokogiri::XML(request_base_resp.body)   
      @notification = doc.xpath('//error/details').text

      if @notification.include? "could not be found."
        @sec_group.destroy
      end

      if sec_group_var == "201" || sec_group_var == "200" # the show must not go on without the group being successfully destroyed
        @notification = "Successfully deleted Security Group %s from NSX" % [@sec_group.name]
        @sec_group.destroy # no user must have this group
      end


      # delete the security tag
      security_tag = Vm.find_by(vm_name: @sec_group.name, vm_type: "SecurityTag")
      if !security_tag.nil?
        url = "https://%s/api/2.0/services/securitytags/tag/%s" % [ipaddr, security_tag.vm_moid]
        delete_sec_tag_resp = HTTParty.delete(url, :basic_auth => auth)
        sec_tag_var = delete_sec_tag_resp.response.inspect.scan(/\d/).join('')
        if sec_tag_var == "201" || sec_tag_var == "200"
          @notification = "%s along with the associated DWF Section" % [@notification]
          security_tag.destroy
        end
      end

      respond_to do |format|
        format.js
      end
    end

    def import_groups_and_objs

      SecGroup.import_group(params[:file].path, current_user)

    end

    def apply_sec_tag

      SecGroup.add_tag_to_VM(params[:file].path, current_user)
      
      respond_to() do |format|
        format.js {
          render :template => "sec_groups/reload_all_sec_groups",
          :layout => false
        }
      end
    end



    def create
      @sec_group = SecGroup.new(sec_group_params)

      respond_to do |format|
        if @sec_group.save
          format.html { redirect_to @sec_group, notice: 'Sec group was successfully created.' }
          format.json { render :show, status: :created, location: @sec_group }
        else
          format.html { render :new }
          format.json { render json: @sec_group.errors, status: :unprocessable_entity }
        end
      end
    end

    def update
      respond_to do |format|
        if @sec_group.update(sec_group_params)
          format.html { redirect_to @sec_group, notice: 'Sec group was successfully updated.' }
          format.json { render :show, status: :ok, location: @sec_group }
        else
          format.html { render :edit }
          format.json { render json: @sec_group.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
      @sec_group.destroy
      respond_to do |format|
        format.html { redirect_to sec_groups_url, notice: 'Sec group was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
    def set_sec_group
      @sec_group = SecGroup.find(params[:id])
    end

    def sec_group_params
      params.require(:sec_group).permit(:created_at, :updated_at, :object_id, :object_entity_id , :name, :newly_created, :user_id, :type)
    end

  end
end