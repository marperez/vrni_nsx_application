module SecurityFiles
  class SecGroupTemplatesController < ApplicationController
    before_action :set_sec_group_template, only: [:show, :edit, :update, :destroy]

    # GET /sec_group_templates
    # GET /sec_group_templates.json
    def index
      @sec_group_templates = SecGroupTemplate.all
    end

    # GET /sec_group_templates/1
    # GET /sec_group_templates/1.json
    def show
    end

    # GET /sec_group_templates/new
    def new
      @sec_group_template = SecGroupTemplate.new
    end

    # GET /sec_group_templates/1/edit
    def edit
    end

    # POST /sec_group_templates
    # POST /sec_group_templates.json
    def create
      @sec_group_template = SecGroupTemplate.new(sec_group_template_params)

      respond_to do |format|
        if @sec_group_template.save
          format.html { redirect_to @sec_group_template, notice: 'Sec group template was successfully created.' }
          format.json { render :show, status: :created, location: @sec_group_template }
        else
          format.html { render :new }
          format.json { render json: @sec_group_template.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /sec_group_templates/1
    # PATCH/PUT /sec_group_templates/1.json
    def update
      respond_to do |format|
        if @sec_group_template.update(sec_group_template_params)
          format.html { redirect_to @sec_group_template, notice: 'Sec group template was successfully updated.' }
          format.json { render :show, status: :ok, location: @sec_group_template }
        else
          format.html { render :edit }
          format.json { render json: @sec_group_template.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /sec_group_templates/1
    # DELETE /sec_group_templates/1.json
    def destroy
      @sec_group_template.destroy
      respond_to do |format|
        format.html { redirect_to sec_group_templates_url, notice: 'Sec group template was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_sec_group_template
        @sec_group_template = SecGroupTemplate.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def sec_group_template_params
        params.require(:sec_group_template).permit(:sec_group_id)
      end
  end
end