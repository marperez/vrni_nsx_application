module SecurityFiles
  class SecTagsController < ApplicationController
    before_action :set_sec_tag, only: [:show, :edit, :update, :destroy]

    # GET /sec_tags
    # GET /sec_tags.json
    def index
      @sec_tags = SecTag.all
    end

    # GET /sec_tags/1
    # GET /sec_tags/1.json
    def show
    end

    # GET /sec_tags/new
    def new
      @sec_tag = SecTag.new
    end

    # GET /sec_tags/1/edit
    def edit
    end

    def search_security_tags
      @filtered_sec_tags = Vm.search_security_tags(params:[text_input])
      format.json { render json: @filtered_sec_tags }
    end

    # POST /sec_tags
    # POST /sec_tags.json
    def create
      @sec_tag = SecTag.new(sec_tag_params)

      respond_to do |format|
        if @sec_tag.save
          format.html { redirect_to @sec_tag, notice: 'Sec tag was successfully created.' }
          format.json { render :show, status: :created, location: @sec_tag }
        else
          format.html { render :new }
          format.json { render json: @sec_tag.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /sec_tags/1
    # PATCH/PUT /sec_tags/1.json
    def update
      respond_to do |format|
        if @sec_tag.update(sec_tag_params)
          format.html { redirect_to @sec_tag, notice: 'Sec tag was successfully updated.' }
          format.json { render :show, status: :ok, location: @sec_tag }
        else
          format.html { render :edit }
          format.json { render json: @sec_tag.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /sec_tags/1
    # DELETE /sec_tags/1.json
    def destroy
      @sec_tag.destroy
      respond_to do |format|
        format.html { redirect_to sec_tags_url, notice: 'Sec tag was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_sec_tag
        @sec_tag = SecTag.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def sec_tag_params
        params.require(:sec_tag).permit(:tag_name)
      end
  end
end