module SecurityFiles
  class FlowsController < ApplicationController
    before_action :set_flow, only: [:show, :edit, :update, :destroy]


    def allow_flow
      @flow = Flow.find_by(time_out: params[:id])
      # @flow = Flow.find_by_id(params[:id])
      @flow.allow_deny = true
      @flow.save

      respond_to() do |format|
        format.js {
          render :template => 'flows/flow_rule.js.erb',
          :layout => false
        }
      end
    end

    def deny_flow

      @flow = Flow.find_by(time_out: params[:id])
      # @flow = Flow.find_by_id(params[:id])
      @flow.allow_deny = false
      @flow.save
      # byebug
      respond_to() do |format|
        format.js {
          render :template => 'flows/flow_rule.js.erb',
          :layout => false
        }
      end
    end


    def remove_flow_from_nsx
      @flow = Flow.find_by(time_out: params[:id])
      @flow.allow_deny = nil
      @flow.save
      # byebug
      respond_to() do |format|
        format.js {
          render :template => 'flows/flow_rule.js.erb',
          :layout => false
        }
      end
    end

    def index
      @flows = Flow.all
    end

    def new
      @flow = Flow.new
    end

    def create
      @flow = Flow.new(flow_params)

      respond_to do |format|
        if @flow.save
          format.html { redirect_to @flow, notice: 'Flow was successfully created.' }
          format.json { render :show, status: :created, location: @flow }
        else
          format.html { render :new }
          format.json { render json: @flow.errors, status: :unprocessable_entity }
        end
      end
    end

    def update
      respond_to do |format|
        if @flow.update(flow_params)
          format.html { redirect_to @flow, notice: 'Flow was successfully updated.' }
          format.json { render :show, status: :ok, location: @flow }
        else
          format.html { render :edit }
          format.json { render json: @flow.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
      @flow.destroy
      respond_to do |format|
        format.html { redirect_to flows_url, notice: 'Flow was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      def set_flow
        @flow = Flow.find(params[:id])
      end

      def flow_params
        params.require(:flow).permit(:osi_layer, :blocked, :protocol, :sent_bytes, :received_bytes, :source, :destination, :port)
      end
  end
end