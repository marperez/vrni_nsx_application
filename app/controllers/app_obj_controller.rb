class AppObjController < ApplicationController
  before_action :set_app_obj, only: [:show, :edit, :update, :destroy]

  # GET /app_objs
  # GET /app_objs.json
  def index
    @app_objs = AppObj.all
  end

  # GET /app_objs/1
  # GET /app_objs/1.json
  def show
  end

  # GET /app_objs/new
  def new
    @app_obj = AppObj.new
  end

  # GET /app_objs/1/edit
  def edit
  end

  # POST /app_objs
  # POST /app_objs.json
  def create
    @app_obj = AppObj.new(app_obj_params)

    respond_to do |format|
      if @app_obj.save
        format.html { redirect_to @app_obj, notice: 'App obj was successfully created.' }
        format.json { render :show, status: :created, location: @app_obj }
      else
        format.html { render :new }
        format.json { render json: @app_obj.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /app_objs/1
  # PATCH/PUT /app_objs/1.json
  def update
    respond_to do |format|
      if @app_obj.update(app_obj_params)
        format.html { redirect_to @app_obj, notice: 'App obj was successfully updated.' }
        format.json { render :show, status: :ok, location: @app_obj }
      else
        format.html { render :edit }
        format.json { render json: @app_obj.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /app_objs/1
  # DELETE /app_objs/1.json
  def destroy
    @app_obj.destroy
    respond_to do |format|
      format.html { redirect_to app_objs_url, notice: 'App obj was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_app_obj
      @app_obj = AppObj.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def app_obj_params
      params.fetch(:app_obj, {})
    end
end
