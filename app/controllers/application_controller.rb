class ApplicationController < ActionController::Base
	before_action :set_current_user
	before_action :set_current_suggestion

	include SessionsHelper

	def set_current_user
		User.current(current_user)
	end

	def set_current_suggestion
		@current_suggestion = SugRule.new
	end

	def filter_columns
		# get the array of filters
		# get the object type 
	end

	def select_filter_columns # this method takes the created filters and makes a single SQL query for the data'
		filter_count = params[:filter_count]
		sql = ""
		having_clause = ""
		sql_week_filter = ""
		order_by_expression = ""
		last_input = ""
		@type = params[:origin]
		@origin = params[:origin]

		@order_array = []
		@filter_array = []
		@col_sort_array = [params["col_sort_choice_1"], params["col_sort_choice_2"], params["col_sort_choice_3"], params["col_sort_choice_4"], params["col_sort_choice_5"]]

		#  we have the chosen filters from the last form and will loop throguh these and append them to a single sql query
		count = 1
		10.to_i.times do |x|
			operator = ""
			column = ""
			selected_operator = params["operator_choice_" + count.to_s]

			if selected_operator.nil?
				selected_operator = ""
			end
			user_input = params["value_choice_" + count.to_s]
			if user_input.nil?
				user_input = ""
			end

			@order_array << params["col_sort_choice_" + count.to_s]

			# The following three case statements create the variables that will make each where clause of the sql statement
			# This should be expanded as more tables use this class method
			return_array = ApplicationRecord.convert_column("filter", selected_operator, params["column_choice_" + count.to_s], user_input, false, @origin)			
			column = return_array[0]
			if column.nil?
				column = ""
			else
				# nothing
			end

			do_not_add_inputs_later = return_array[1]
			# This chould match all the choice possibilities and expand with such
			operator = ApplicationRecord.convert_operator(selected_operator, user_input)
			# This is to make a string to be able to use the operators that the user normally would use on an integer filled column
			case (true if Float(user_input) rescue false)
				when (false) # a string
					if operator != " LIKE '%#{user_input}%' " && column != ""
						if operator != " = " && column != ""
							user_input = user_input.length.to_s
							column = (" length(" + column + ") ")
						else
							user_input = "'" + user_input + "'" # it needs extra quotes to maintain some inside the sql query
						end
					end
				when (true) # an integer
					# nothing
					if params["operator_choice_" + count.to_s] == "Last x Days"
						if count != 1
							sql += " and "
						end
						# sql_week_filter += " (WEEK(#{column}) = WEEK(NOW()) - #{user_input})" 
						sql_week_filter += " #{column} BETWEEN datetime('now', '-#{user_input} days') AND datetime('now', 'localtime')"
					end
				end

				if params["value_choice_" + count.to_s] != "" && operator != ""
				# The user input filters are bound by 'AND' operators (for now); this is appended starting from the second
				if count != 1 && last_input != "" && user_input != ""
					sql += " AND "
					byebug
					last_input = "the last element is not blank"
				elsif count == 1 && user_input != "" && selected_operator != ""
					last_input = "the last element is not blank"
				else
					last_input = ""
				end
				# Building the statement

				if user_input != "" && selected_operator != ""
					if column == "created_at" or column == "time_rec" or column == "time_sent"
						time = user_input.to_i.day.ago.to_date.to_s
						user_input = time
					end

					column = ApplicationRecord.get_full_column_sql_name(column, @origin) # the sql  query needs the full name of created_at

					@filter_array << column
					if column == "sum(sent_bytes)" or  column == "sum(received_bytes)" or  column == "sum(sessions)"
						having_clause += (column + operator + user_input)
					else
						if selected_operator == "Is Like"
							sql += (column + operator)
						elsif !do_not_add_inputs_later # var = true when sql statement needs more depth
							sql += (column + operator + user_input)
						else
							sql += column
						end
					end
				end
			end
			count += 1
		end

		sql += sql_week_filter
		sql_week_filter = ""

		# append any order by fields
		column_order_by_set = Set.new
		sort_index = 0
		@col_sort_array.each do |sort_column|
			sort_column = ApplicationRecord.get_full_column_sql_name(sort_column, @origin)
			case (sort_index)
			when 0
				column_order_selection = ApplicationRecord.convert_column("order","", sort_column, " ", false, @origin)
				column_order_by_set.add(column_order_selection)
				if !column_order_selection[0].nil? 
					order_by_expression += (column_order_selection[0] + " DESC")
					sort_index += 1
				end
			else
				column_order_selection = ApplicationRecord.convert_column("order", "", sort_column, " ", false, @origin)
				if !column_order_by_set.include?(column_order_selection) and !column_order_selection[0].nil?
					column_order_by_set.add(column_order_selection[0])
					order_by_expression += (", " + column_order_selection[0] + " DESC")
					sort_index += 1
				end
			end
		end

		@universal_rules = []
		@sec_group = []

		byebug
		case(@origin) # this method should be tweakable to be able to use on any table
		when "all_dfw_rules"
			#  combining each into an includes cuts the amount time from one sql search per rule to a single database query 
			@universal_rules = FireRule.includes(:sources, :destinations, :services, :protocols, :port_ranges, :applied_tos).where(sources: { sour_value: "any"}, destinations: { dest_value: "any"}).where(sql).order(order_by_expression)
		when "flows_partial", "dash_flows_partial"
			current_group_id = session[:current_group_id]
			if current_group_id != ""
				if @origin == "flows_partial"
					sql = " flows.sec_group_id = #{current_group_id} AND " + sql
				end
				@group_flows = Flow.all.where(sql).select("id, allow_deny, protocol_name, source, destination, port, sum(sent_bytes) as sum_sent_bytes, sum(received_bytes) as sum_rec_bytes, sum(sessions) as sum_sessions, min(time_out) as earliest_outbound, max(time_rec) as latest_inbound").group("source, destination, port, protocol_name").order(order_by_expression).having(having_clause)
			elsif @origin == "dash_flows_partial"
				@group_flows = Flow.all.where(sql).select("id, allow_deny, protocol_name, source, destination, port, sum(sent_bytes) as sum_sent_bytes, sum(received_bytes) as sum_rec_bytes, sum(sessions) as sum_sessions, min(time_out) as earliest_outbound, max(time_rec) as latest_inbound").group("source, destination, port, protocol_name").order(order_by_expression).having(having_clause)
			end
		when "user_table_partial"
			@selected_users = User.includes(:rule_histories, :sec_groups).where(sql).order(order_by_expression).uniq
		when "rule_histories_partial"
			@rule_histories = RuleHistory.left_joins(:sources, :destinations, :protocols, :port_ranges).where(sql).order(order_by_expression).where("undo_rule is not null")
		end


		byebug


		respond_to do |format|
			format.js { 
				render :template => 'layouts/filters/filter_columns.js.erb',
				:layout => false
			}
		end
	end

	def add_column_sort_modal_item

		@column_array = params[:column_array].split("/")

		@origin = params[:origin]
		@filter_count = params[:row_totals].to_i + 1

		@type = "add_column_sort_modal_item"

		respond_to do |format|
			format.js { 
				render :template => 'layouts/filters/filter_columns.js.erb',
				:layout => false
			}
		end
	end

	def show_widget
		@type = params[:type].to_s

		@which_controller = params[:which_controller]

		if @which_controller == "rule_template"
			@template = RuleTemplate.find_by_id(params[:template_id])
			@rule = FireRule.find_by_id(params[:rule_id])
		elsif @which_controller == "sec_group"
			@rule = SugRule.find_by_id(session[:clone_suggestion_id])
		end

		respond_to() do |format|
			format.js { 
				render :template => 'layouts/new_rule_js/open_rule_table_edit_widgets.js.erb',
				:layout => false
			}
		end
	end

	# def get_vmware_objects
		# this is actually found in the vmware controller
	# end

	def add_to_ports_column # connected to the method above
		startport = params[:source]
		endport = params[:destination]
		@type = "service"

		case params[:which_controller]
		when "sec_group"

			@suggested = SugRule.find_by_id(session[:clone_suggestion_id])


			port_range = PortRange.new(startport: startport, endport: endport)
			@suggested.port_ranges << port_range
			@suggested.save

			respond_to() do |format|
				format.js { 
					render :template => 'layouts/add_to_rule_creation_table.js.erb',
					:layout => false
				}
			end
		when "rule_template"

			@rule = FireRule.find_by_id(params[:rule_id])

			port_range = PortRange.new(startport: startport, endport: endport)
			@rule.port_ranges << port_range

			respond_to() do |format|
				format.js { 
					render :template => 'rule_templates/create_rule_table_cells.js.erb',
					:layout => false
				}
			end
		end
	end

	def add_to_table_column

		@suggested = SugRule.find_by_id(params[:rule_id])

		@template_id = params[:template_id]

		if params[:vmware_object_id]
			sysobj = Vm.find_by_id(params[:vmware_object_id])
		end

		@which_controller = params[:which_controller]        

		@type = params[:type]

		if @type.nil?
			@type = params[:which_action]
		end

		case(@which_controller)
		when "sec_group"
			@rule = SugRule.find_by_id(session[:clone_suggestion_id])
		when "rule_template"
			@template = RuleTemplate.find_by_id(params[:template_id])
			@rule = FireRule.find_by_id(params[:rule_id])
		end

		if @type == "source"
			SugRule.add_children_table_objs_to_this_rule(sysobj, "sources", @rule) # cuts the load from this controller and puts it into the model
		elsif @type == "destination"
			SugRule.add_children_table_objs_to_this_rule(sysobj, "destinations", @rule) # cuts the load from this controller and puts it into the model
		elsif @type == "service"
			SugRule.add_children_table_objs_to_this_rule(sysobj, "service", @rule)
		elsif @type == "protocol" || @type.downcase == "http" || @type.downcase == "https" || @type.downcase == "udp" || @type.downcase == "tcp" || @type.downcase == "manual"
			if @type == "manual"
				protocol = Protocol.new(name: params[:text_input])
			else
				protocol = Protocol.new(name: params[:which_action])
			end
			@type = "protocol"
			@rule.protocols << protocol
			@rule.save
		elsif @type == "action"
			@rule.action = params[:which_action]
			@rule.save
		elsif @type == "name"
			@rule.name = params[:text_input]			
			@rule.save
		end

		respond_to() do |format|
			format.js { 
				render :template => 'layouts/new_rule_js/reload_rule_columns.js.erb',
				:layout => false
			}
		end

	end

	def delete_rule_item

		@template_id = params[:template_id]
		@which_controller = params[:which_controller]
		case params[:which_controller]
		when "rule_template"
			@rule = FireRule.find_by_id(params[:rule_id])
			@type = params[:item_type]
		when "sec_group"
			@rule = SugRule.find_by_id(params[:rule_id])
			@type = params[:item_type]
		end

		case(@type)
		when "source"
			source = Source.find_by_id(params[:item_id])
			@rule.sources.destroy(source.id)
		when "destination"
			dest = Destination.find_by_id(params[:item_id])
			@rule.destinations.destroy(dest.id)
		when "service"
			serv = Service.find_by_id(params[:item_id])
			@rule.services.destroy(serv.id)
		when "port"
			port = PortRange.find_by_id(params[:item_id])
			@rule.port_ranges.destroy(port.id)
		when "action"
			@rule.action = ""
		when "protocol"
			prot = Protocol.find_by_id(params[:item_id])
			@rule.protocols.destroy(prot.id)
		end

		if @type == "port"
			@type = "service"
		end

	    respond_to() do |format| # now reload section
	    	format.js { 
	    		render :template => 'layouts/new_rule_js/reload_rule_columns.js.erb',
	    		:layout => false
	    	}
	    end
	end
end
