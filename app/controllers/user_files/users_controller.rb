module UserFiles
  class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  # before_action :get_sys_objects

  def index
    @users = User.all
  end

  def admin_home
    @selected_users = User.all
  end

  def home
    if current_user
      redirect_to admin_home_path
    end
  end

  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  def edit
  end

  def signup_button
    @type = "signup"
    respond_to do |format|
      format.js { render :action => "/sessions/loginsignup.js.erb"}
    end
  end

  def login_button
    @type = "login"
    respond_to do |format|
      format.js { render :action => "/sessions/loginsignup.js.erb"}
    end
  end

  def notification_email
    user = User.find_by_id(params[:id])
    NotificationsMailer.task_reminder(user).deliver_now
    @user = user
    # redirect_to admin_home_path
    # respond_to do |format|
    #   format.js
    # end
  end

  def attach_sec_group_user
    task = SecGroup.find_by_id(params[:task_id])
    @user = User.find_by_id(params[:user_id])

    @user.sec_groups << task unless @user.sec_groups.include?(task)
    @user.save

    @type = "added_task"

    respond_to() do |format|
      format.js { render :action => "attach_sec_group_user.js.erb"}
    end
  end

  def search_security_groups
    @groups = SecGroup.search_groups(params[:search])
    @user_id = params[:user_id]

    @type = "security_group_search" # refreshes the user partial
    respond_to() do |format|
      format.js { render :action => "/admin_dashboard_js_erb/admin_dashboard.js.erb"}
    end
  end

  def remind_each_user
    User.all.each do |user|
      NotificationsMailer.task_reminder(user).deliver_now
    end
  end

  def import_users
    if params[:file].nil?
      @type = "notification"
      @error = "Please attach a file before selecting submit"
    else
      @mispelt_groups_array = Set.new
      @mispelt_groups_array = User.import_users(params[:file].path)
      @selected_users = User.includes(:rule_histories, :sec_groups).all

      @type = "users" # refreshes the user partial
      @mispellings
    end
    respond_to() do |format|
      format.js { render :action => '/admin_dashboard_js_erb/admin_dashboard.js.erb'}
    end
  end

  def manual_new_user

    byebug
    User.create(name: params[:name], email: params[:email], password: "VMware1!", password_confirmation: "VMware1!")
    @selected_users = User.all
    @type = "new_user_in_dashboard" 
    respond_to() do |format|
      format.js { render :action => '/admin_dashboard_js_erb/admin_dashboard.js.erb'}
    end
  end

  def delete_user
    user = User.find_by_id(params[:id])
    if(!user.nil?)
      User.destroy(params[:id])
    end
    @type = "users"
    @method = "delete"
    @user = user
    @selected_users = User.all
    respond_to() do |format|
      format.js { render :action => '/admin_dashboard_js_erb/admin_dashboard.js.erb'}
    end
  end

  def admin_dashboard
    @type = params[:type]
    case(@type)
      when "users"

      when "flows"

      when "system"
        flow_select_statement = "select count(*) as total_grouped_flows from (select count(*) from flows group by flows.source, flows.destination, flows.port, flows.protocol_name) as summation"
        @flows = ActiveRecord::Base.connection.execute(flow_select_statement)
      when "history"
        @rule_histories = RuleHistory.left_joins(:sources, :destinations, :protocols, :port_ranges, :services).order('rule_histories.created_at').where("undo_rule is not null and rule_histories.created_at > ? ", Time.now - 1209600) # two weeks of history only
    end

    @group_flows = Flow.all.select("id, protocol_name, source, destination, port, sum(sent_bytes) as sum_sent_bytes, sum(received_bytes) as sum_rec_bytes, sum(sessions) sum_sessions, min(time_out) as earliest_outbound, max(time_rec) as latest_inbound").group("source, destination, port, protocol_name")

    @selected_users = User.all

    respond_to() do |format|
      format.js { render :action => '/admin_dashboard_js_erb/admin_dashboard.js.erb'}
    end
  end

  def admin_dash_flows

  end

  def admin_dash_sys_and_tasks

  end

  def admin_dash_all_histories

  end

  def create
    user = User.new(user_params)
    byebug
    if user.valid?
      if user.save
        user.setting = Setting.new
        user.save
        # ""
        log_in user # log_in is a method inside the sessions helper class
        session[:user_id] = user.id
        redirect_to settings_index_path
      else
        redirect_to '/signup'
      end
    else
      @user = user

      # redirect to javascript to add/replace partial
      respond_to() do |format|
        format.js { render :action => 'errors/user_create_error.js.erb'}
      end
    end
  end

  def update

    if params[:user][:name].to_s != ""
      @user.name = params[:user][:name]
    end

    if params[:user][:email].to_s != ""
      @user.email = params[:user][:email]
    end

    if (params[:user][:password].to_s != "" && params[:user][:password_confirmation].to_s != "") && (params[:user][:password].to_s == params[:user][:password_confirmation].to_s)
      @user.password = params[:user][:password]
      @user.password_confirmation = params[:user][:password_confirmation]
    end

    if (@user.password_digest && @user.errors[:email].blank? && @user.errors[:name].blank?)
      @user.save
      @type = "user_update"

      respond_to do |format|
        format.js { render :file => 'settings/settings_resp.js.erb'}
      end
    else

      # redirect to javascript to add/replace partial
      respond_to() do |format|
        format.js { render :action => 'errors/user_update_error.js.erb'}
      end
    end
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end

    def get_sys_objects
      if current_user
        Vm.find_all_sys_objs(current_user)
      end
    end
  end
end
