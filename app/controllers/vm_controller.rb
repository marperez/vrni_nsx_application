class VmController < ApplicationController
  before_action :set_vm, only: [:show, :edit, :update, :destroy]

  # this class should be renamed to vmware_objects as it contains every object available
  # this class is currently as of 09/13/2018 populated by an NSX API
  # You can try another way to populate this

  def new
    @vm = Vm.new
  end

  def create
    @vm = Vm.new(vm_params)

    respond_to do |format|
      if @vm.save
        format.html { redirect_to @vm, notice: 'Vm was successfully created.' }
        format.json { render :show, status: :created, location: @vm }
      else
        format.html { render :new }
        format.json { render json: @vm.errors, status: :unprocessable_entity }
      end
    end
  end

  def search_vmware_sys_objs

    @sys_objs = nil
    @which_column = params[:which_column]
    @which_controller = params[:which_controller]
  
    @template_id = params[:template_id]

    case(@which_controller)
      when "sec_group"
        @rule = SugRule.find_by_id(session[:clone_suggestion_id])
      when "rule_template"
        @rule = FireRule.find_by_id(params[:rule_id])
    end

    if params[:text_input]

      if @which_column == "service"
        @sys_objs = Vm.search_applications(params[:text_input]).order("vm_name DESC")
      else
        @sys_objs = Vm.search(params[:text_input]).order("vm_name DESC")
      end

      respond_to() do |format|
        format.js { 
          render :template => 'layouts/new_rule_js/search_vmware_sys_objs.js.erb',
          :layout => false
        }
      end
    end
  
  end

  # PATCH/PUT /vms/1
  # PATCH/PUT /vms/1.json
  def update
    respond_to do |format|
      if @vm.update(vm_params)
        format.html { redirect_to @vm, notice: 'Vm was successfully updated.' }
        format.json { render :show, status: :ok, location: @vm }
      else
        format.html { render :edit }
        format.json { render json: @vm.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vms/1
  # DELETE /vms/1.json
  def destroy
    @vm.destroy
    respond_to do |format|
      format.html { redirect_to vms_url, notice: 'Vm was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vm
      @vm = Vm.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vm_params
      params.fetch(:vm, {})
    end
end