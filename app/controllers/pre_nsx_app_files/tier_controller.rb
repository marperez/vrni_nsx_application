class TierController < ApplicationController
  before_action :set_vm, only: [:show, :edit, :update, :destroy]

  def index
    @tiers = Tier.all
  end

  def show
  end

  def new
    @tier = Tier.new
  end

  def edit
  end

  def create
    @tier = Tier.new(vm_params)

    respond_to do |format|
      if @tier.save
        format.html { redirect_to @tier, notice: 'Tier was successfully created.' }
        format.json { render :show, status: :created, location: @tier }
      else
        format.html { render :new }
        format.json { render json: @tier.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @vm.update(vm_params)
        format.html { redirect_to @tier, notice: 'Tier was successfully updated.' }
        format.json { render :show, status: :created, location: @tier }
      else
        format.html { render :edit }
        format.json { render json: @tier.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @tier.destroy
    respond_to do |format|
      format.html { redirect_to tier_url, notice: 'Tier was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  	def set_vm
      @tier = Tier.find(params[:id])
    end

    def vm_params
      params.fetch(:tier, {})
    end
end