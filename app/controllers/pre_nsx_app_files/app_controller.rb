class AppController < ApplicationController
  before_action :set_app, only: [:show, :edit, :update, :destroy]
  # GET /apps
  # GET /apps.json
  def index
    @apps = App.all
    FireRule.insert_all_fire_rules(current_user, "")
    @related_rules = TempFireRule.all
    @suggestions = SugRule.all
    @current_app = App.new

    
   fw_rules = []
    # both
    fw_rules.push(*FireRule.joins(:sources, :destinations).where("sources.sour_value = 'any' AND destinations.dest_value = 'any'")
        .select("fire_rules.id, fire_rules.name, fire_rules.action, fire_rules.direction, fire_rules.pack_type"))

    # first
    fw_rules.push(*FireRule.joins(:sources, :destinations).where("sources.sour_value = 'any' AND destinations.dest_value != 'any'")
        .select("fire_rules.id, fire_rules.name, fire_rules.action, fire_rules.direction, fire_rules.pack_type"))

    # second
    fw_rules.push(*FireRule.joins(:sources, :destinations).where("sources.sour_value != 'any' AND destinations.dest_value = 'any'")
        .select("fire_rules.id, fire_rules.name, fire_rules.action, fire_rules.direction, fire_rules.pack_type"))

    @universal_rules = fw_rules.uniq
  end

  # GET /apps/1
  # GET /apps/1.json
  def show
  end

  # GET /apps/new
  def new
    @app = App.new
  end

  # GET /apps/1/edit
  def edit
  end

  def get_app_fw_rules
    TempFireRule.delete_all
    SecGroup.delete_all
    app = App.find_by(params[:id]) 
    SecGroup.create!(name: app.name) # used in getting the sugested rules 
    SugRule.create_sug_rules(params[:id], "app", current_user) # create the security group in 

    AppObj.get_app_members(params[:id])
    redirect_to app_index_path
  end

  def collect_apps

  end

  def create_app_sec_gp_and_fw_rules
    # Model method below takes all the selected suggestions and creates a section and populates it with the rules
    SecGroup.add_sec_gp_to_nsx(current_user)
    current_group_id = session[:current_group_id].to_s
    response = FireRule.parcel_selected_fw_rules_for_nsx(current_group_id, current_user)
    flash = ""
    message = "s"
    if(!message.to_s.include? "error")
            # ""
      int = SugRule.where(submit_sug: "t").count
      success_message = "Successfully added " + int.to_s + " rules to the Distributed Firewall"
      response = ConfirmMailMailer.new_confirmation_email.deliver
      respond_to do |format|
        format.html { redirect_to app_index_path }
        format.json { head :no_content }
      #     format.js { message = "Here is my flash notice" }
        format.js   { render :layout => false }
      end
    else
      # flash[:error] = response.to_s
    end

  end


  def import
    App.delete_all
    App.import(params[:file].path)
    TempFireRule.delete_all
    redirect_to app_index_path
  end

  # POST /apps
  # POST /apps.json
  def create
    @app = App.new(app_params)

    respond_to do |format|
      if @app.save
        format.html { redirect_to @app, notice: 'App was successfully created.' }
        format.json { render :show, status: :created, location: @app }
      else
        format.html { render :new }
        format.json { render json: @app.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /apps/1
  # PATCH/PUT /apps/1.json
  def update
    respond_to do |format|
      if @app.update(app_params)
        format.html { redirect_to @app, notice: 'App was successfully updated.' }
        format.json { render :show, status: :ok, location: @app }
      else
        format.html { render :edit }
        format.json { render json: @app.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /apps/1
  # DELETE /apps/1.json
  def destroy
    @app.destroy
    respond_to do |format|
      format.html { redirect_to app_index_path, notice: 'App was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_app
      @app = App.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def app_params
      params.require(:app).permit(:name)
    end
end

