class SessionsController < ApplicationController

	def new
	end

	def create
		user = User.find_by(email: params[:session][:email].downcase)
		# if @user.password == params[:password]
	    if user && user.authenticate(params[:session][:password])
			log_in user # log_in is a method inside the sessions helper class
			
			current_user = user

			session[:selected_suggestion] = ""
			session[:selected_table] = ""
			session[:clone_suggestion_id] = ""
			session[:current_group_id]  = ""
			session[:template_id] = ""
			
			@current_user = current_user

			# delete_old_rule_histories = @current_user.rule_historwhere()

			if user.name.include? "admin"
				redirect_to admin_home_path

			else
				redirect_to settings_index_path
			end

			# Vm.find_all_sys_objs(current_user)
		else
			@validation_text = "Incorrect password"
			@type = "login"
		    respond_to() do |format|
		      format.js { render :template => "/user_files/users/sessions/loginsignup"}
		    end
		end
	end

	def destroy
		log_out
		redirect_to home_path
	end
end
