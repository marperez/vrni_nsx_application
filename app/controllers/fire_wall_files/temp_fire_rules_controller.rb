module FireWallRules
  class TempFireRulesController < ApplicationController
    before_action :set_temp_fire_rule, only: [:show, :edit, :update, :destroy]

    # GET /temp_fire_rules
    # GET /temp_fire_rules.json
    def index
      @temp_fire_rules = TempFireRule.all
    end

    # GET /temp_fire_rules/1
    # GET /temp_fire_rules/1.json
    def show
    end

    # GET /temp_fire_rules/new
    def new
      @temp_fire_rule = TempFireRule.new
    end

    # GET /temp_fire_rules/1/edit
    def edit
    end

    # POST /temp_fire_rules
    # POST /temp_fire_rules.json
    def create
      @temp_fire_rule = TempFireRule.new(temp_fire_rule_params)

      respond_to do |format|
        if @temp_fire_rule.save
          format.html { redirect_to @temp_fire_rule, notice: 'Temp fire rule was successfully created.' }
          format.json { render :show, status: :created, location: @temp_fire_rule }
        else
          format.html { render :new }
          format.json { render json: @temp_fire_rule.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /temp_fire_rules/1
    # PATCH/PUT /temp_fire_rules/1.json
    def update
      respond_to do |format|
        if @temp_fire_rule.update(temp_fire_rule_params)
          format.html { redirect_to @temp_fire_rule, notice: 'Temp fire rule was successfully updated.' }
          format.json { render :show, status: :ok, location: @temp_fire_rule }
        else
          format.html { render :edit }
          format.json { render json: @temp_fire_rule.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /temp_fire_rules/1
    # DELETE /temp_fire_rules/1.json
    def destroy
      @temp_fire_rule.destroy
      respond_to do |format|
        format.html { redirect_to temp_fire_rules_url, notice: 'Temp fire rule was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_temp_fire_rule
        @temp_fire_rule = TempFireRule.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def temp_fire_rule_params
        params.fetch(:temp_fire_rule, {})
      end
  end
end
