class FireRuleController < ApplicationController
  before_action :set_fire_rule, only: [:show, :edit, :update, :destroy]

  def index
    @fire_rules = FireRule.all
  end

  def create
    @fire_rule = FireRule.new(fire_rule_params)

    respond_to do |format|
      if @fire_rule.save
        format.html { redirect_to @fire_rule, notice: 'Fire rule was successfully created.' }
        format.json { render :show, status: :created, location: @fire_rule }
      else
        format.html { render :new }
        format.json { render json: @fire_rule.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @fire_rule.update(fire_rule_params)
        format.html { redirect_to @fire_rule, notice: 'Fire rule was successfully updated.' }
        format.json { render :show, status: :ok, location: @fire_rule }
      else
        format.html { render :edit }
        format.json { render json: @fire_rule.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @fire_rule.destroy
    respond_to do |format|
      format.html { redirect_to fire_rules_url, notice: 'Fire rule was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_fire_rule
      @fire_rule = FireRule.find(params[:id])
    end

    def fire_rule_params
      params.fetch(:fire_rule, {})
    end
end