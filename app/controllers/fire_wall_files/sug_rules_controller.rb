module FireWallFiles
  class SugRulesController < ApplicationController
    before_action :set_sug_rule, only: [:show, :edit, :update, :destroy]

    def index
      @sug_rules = SugRule.all
    end

    def show
    end

    def new
      @sug_rule = SugRule.new
    end

    def edit
    end

    def add_new_sug_rule

      @which_controller = "sec_group"
      if session[:current_group_id] != ""

        @rule = SugRule.new
        current_sg = SecGroup.find_by_id(session[:current_group_id])
        current_sg.sug_rules << @rule
        @rule.save

        session[:selected_suggestion] = "" # there is none
        session[:clone_suggestion_id] = @rule.id

        respond_to() do |format|
          format.js { 
              render :template => 'security_files/sec_groups/open_sys_obj_partial.js.erb',
              :layout => false
          }
        end
      else
        # no security group has been selected from the left hand navigation
      end
    end


    def edit_sug_save # to save the edited suggested rule
      original_untouched_rule = SugRule.find_by_id(session[:selected_suggestion])
      cloned_and_altered_rule = SugRule.find_by_id(session[:clone_suggestion_id])

      current_group_id = session[:current_group_id]
      user_group = SecGroup.find_by_id(current_group_id)

      if original_untouched_rule
         SugRule.destroy(original_untouched_rule.id) # destroy the original
      end
      
      @sug_rules = user_group.sug_rules # find, to then return the remaining rules

      session[:clone_suggestion_id] = ""
      session[:selected_suggestion] = ""

      respond_to() do |format|
        format.js { 
          render :template => 'security_files/sec_groups/edit_sug_rule_return.js.erb',
          :layout => false
        }
      end
    end

    def edit_sug_cancel # to nullify the change to the suggested rule
      id = session[:clone_suggestion_id]
      cloned_and_altered_rule = SugRule.find_by_id(id)
      
      session[:clone_suggestion_id] = ""
      session[:selected_suggestion] = ""

      current_group_id = session[:current_group_id]
      user_group = SecGroup.find_by_id(current_group_id)

      if !cloned_and_altered_rule.nil?
        SugRule.destroy(cloned_and_altered_rule.id) # destroy the clone
        @sug_rules = user_group.sug_rules # find, to then return the remaining rules
        # ""
      end

      respond_to() do |format|
        format.js { 
          render :template => 'security_files/sec_groups/edit_sug_rule_return.js.erb',
          :layout => false
        }
      end
    end

    def open_sys_obj_partial
      @rule = SugRule.find_by_id(params[:id])
      @which_controller = "sec_group"

      @suggested = @rule.dup # start our cloning process
      @suggested.save
      @suggested.protocols << @rule.protocols.collect { |group| group.dup }
      @suggested.port_ranges << @rule.port_ranges.collect { |port| port.dup }
      @suggested.sources << @rule.sources.collect { |sour| sour.dup }
      @suggested.destinations << @rule.destinations.collect { |dest| dest.dup }
      @suggested.services << @rule.services.collect { |serv| serv.dup }
      @suggested.save # this clone will take the place of the rule if we save this edit and it will be deleted if we don't

      session[:selected_suggestion] = @rule.id
      session[:clone_suggestion_id] = @suggested.id
      @current_suggestion = @rule

      respond_to() do |format|
        format.js { 
          render :template => 'security_files/sec_groups/open_sys_obj_partial.js.erb',
          :layout => false
        }
      end
    end

    def delete_sug_item
      @type = params[:type].to_s
      @rule = SugRule.find_by_id(params[:rule_id])
      @which_controller = params[:which_controller]
      
      case(@type)
        when "destination"
          destination = Destination.find_by_id(params[:type_id])
          @rule.destinations.destroy(destination)
        when "source"
          source = Source.find_by_id(params[:type_id])
          @rule.sources.destroy(source)
        when "action"
          @rule.action = ""
        when "protocol"
          protocol = Protocol.find_by_id(params[:type_id])
          @rule.protocols.destroy(protocol)
        when "port"
          ports = PortRange.find_by_id(params[:type_id])
          @rule.port_ranges.destroy(ports)
        when "service"
          service = Service.find_by_id(params[:type_id])
          @rule.services.destroy(service)
          @suggested = SugRule.find_by_id(session[:selected_suggestion])
          if @suggested.nil?
            @suggested = SugRule.find_by_id(session[:clone_suggestion_id])
          end
      end

      respond_to() do |format|
        format.js { 
            render :template => 'layouts/new_rule_js/reload_rule_columns.js.erb',
            :layout => false
        }
      end
    end

    def add_sug_rules
      @rule = SugRule.find(params[:id])
      if @rule.submit_sug == true
        @rule.submit_sug = false
      else
        @rule.submit_sug = true
      end
      @rule.save
      
      respond_to do |format|
          format.js
      end
    end

    def create_fw_rules
      # Model method below takes all the selected suggestions and creates a section and populates it with the rules
      current_group_id = session[:current_group_id].to_s
    	@response = FireRule.parcel_selected_fw_rules_for_nsx(current_group_id, current_user)
      
      respond_to do |format|
          format.js
      end
      
      # redirect_to sec_groups_url
    end

    def create
      @sug_rule = SugRule.new(sug_rule_params)

      respond_to do |format|
        if @sug_rule.save
          format.html { redirect_to @sug_rule, notice: 'Sug rule was successfully created.' }
          format.json { render :show, status: :created, location: @sug_rule }
        else
          format.html { render :new }
          format.json { render json: @sug_rule.errors, status: :unprocessable_entity }
        end
      end
    end

    def update
      respond_to do |format|
        if @sug_rule.update(sug_rule_params)
          format.html { redirect_to @sug_rule, notice: 'Sug rule was successfully updated.' }
          format.json { render :show, status: :ok, location: @sug_rule }
        else
          format.html { render :edit }
          format.json { render json: @sug_rule.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
      sug_rule = SugRule.find_by_id(params[:id])
      sug_rule.destroy
      respond_to do |format|
        format.html { redirect_to sug_rules_url, notice: 'Sug rule was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      def set_sug_rule
        @sug_rule = SugRule.find(params[:id])
      end

      def sug_rule_params
        params.fetch(:sug_rule, {})
      end
  end
end