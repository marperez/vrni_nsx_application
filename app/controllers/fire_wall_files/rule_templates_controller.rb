module FireWallFiles
  class RuleTemplatesController < ApplicationController
    before_action :set_rule_template, only: [:show, :edit, :update, :destroy]

    def index
      @templates = RuleTemplate.distinct
    end

    def new
      @template = RuleTemplate.new
    end

    def edit_template
      @template = RuleTemplate.find_by_id(params[:id])    
      @which_controller = "rule_template"
      @rule = FireRule.new
      @rule.save

      @type = "edit_template"

      respond_to() do |format|
        format.js { 
          render :template => 'fire_wall_files/rule_templates/rule_templates.js.erb',
          :layout => false
        }
      end
    end

    def search_templates
      @templates =  RuleTemplate.search(params[:text_input])

      if params[:origin] == "templates_index"
        @type = "reload_templates_side_nav_templates_index"
      else
        @type = "reload_templates_left_side_sec_group_index"
      end

      respond_to() do |format|
        format.js {
          render :template => 'fire_wall_files/rule_templates/rule_templates.js.erb',
          :layout => false
        }
      end
    end

    def set_template_name
      template = RuleTemplate.find_by_id(params[:id])
      template.name = params[:name]
      template.save
      @templates = RuleTemplate.all

      @type = "reload_templates_side_nav"

      respond_to() do |format|
        format.js { 
          render :template => 'fire_wall_files/rule_templates/rule_templates.js.erb',
          :layout => false
        }
      end
    end

    def create_a_template
      @template = RuleTemplate.new
      @template.save
      session[:template_id] = @template.id
      @type = "create_a_template"    
      @rule = FireRule.new
      @rule.save

      @templates = RuleTemplate.all
      
      @which_controller = "rule_template"

      respond_to() do |format|
        format.js { 
          render :template => 'fire_wall_files/rule_templates/rule_templates.js.erb',
          :layout => false
        }
      end
    end

    def see_template_rules
      @template = RuleTemplate.find_by_id(params[:id])
      @type = "see_template_rules" 

      if !params[:rule].nil?
        rule = FireRule.find_by_id(params[:rule])
        # see if anything was added to this rule otherwise ignore it
        if !rule.name.nil? && rule.name != ""
          if rule.sources.count > 0 || rule.destinations.count > 0 || rule.services.count > 0 || rule.port_ranges.count > 0 || rule.protocols.count > 0     
            
          end
        end
      end
      respond_to do |format|
        format.js {
          render :template => 'fire_wall_files/rule_templates/rule_templates.js.erb',
          :layout => false
        }
      end
    end

    def cancel_rule_to_template
      @template = RuleTemplate.find_by_id(params[:id])

      old_rule = FireRule.find_by_id(params[:rule_id])
      old_rule.destroy

      @rule = FireRule.new
      @rule.save

      @type = "cancel_rule_to_template"
      @which_controller = "rule_template"

      respond_to do |format|
        format.js {
          render :template => 'fire_wall_files/rule_templates/rule_templates.js.erb',
          :layout => false
        }
      end
    end

    def add_rules_to_template 
      @rule = FireRule.find_by_id(params[:rule_id])
      @template = RuleTemplate.find_by_id(params[:template_id])
      @which_controller = "rule_template"
      
      @template.fire_rules << @rule
      @template.save

      @rule = FireRule.new
      @rule.save

      @type = "reload_template_rules"
      respond_to do |format|
        format.js {
          render :template => 'fire_wall_files/rule_templates/rule_templates.js.erb',
          :layout => false
        }
      end
    end

    def delete_a_template_rule
      @template = RuleTemplate.find_by_id(params[:template_id])
      @template_id = @template.id
      @rule = FireRule.find_by_id(params[:rule_id])
      @rule.destroy

      @type = "reload_template_current_rules"
      respond_to do |format|
        format.js {
          render :template => 'fire_wall_files/rule_templates/rule_templates.js.erb',
          :layout => false
        }
      end
    end

    def edit_a_template_rule
      @which_controller = "rule_template"
      @template_id = params[:template_id]
      @template = RuleTemplate.find_by_id(@template_id)
      @rule = FireRule.find_by_id(params[:rule_id])

      @type = "every_column"
      respond_to() do |format|
        format.js { 
          render :template => 'layouts/new_rule_js/reload_rule_columns.js.erb',
          :layout => false
        }
      end
    end 

    def add_template_to_group
      current_group = SecGroup.find_by_id(session[:current_group_id])
      
      @templates = current_group.rule_templates.order('name ASC')
      if !current_group.nil?
        template = RuleTemplate.find_by_id(params[:template_id])
        current_group.rule_templates << template
        current_group.save

        @type = "reload_sec_group_templates_index"
        respond_to() do |format|
          format.js { 
            render :template => 'fire_wall_files/rule_templates/rule_templates.js.erb',
            :layout => false
          }
        end
      end
    end

    def remove_template_from_group
      current_group = SecGroup.find_by_id(session[:current_group_id])
      
      @templates = current_group.rule_templates.order('name ASC')
      if !current_group.nil?
        template = RuleTemplate.find_by_id(params[:template_id])
        current_group.rule_templates.delete(template)

        @type = "reload_sec_group_templates_index"
        respond_to() do |format|
          format.js { 
            render :template => 'fire_wall_files/rule_templates/rule_templates.js.erb',
            :layout => false
          }
        end
      end
    end

    def create
      @rule_template = RuleTemplate.new(rule_template_params)

      respond_to do |format|
        if @rule_template.save
          format.html { redirect_to @rule_template, notice: 'Rule template was successfully created.' }
          format.json { render :show, status: :created, location: @rule_template }
        else
          format.html { render :new }
          format.json { render json: @rule_template.errors, status: :unprocessable_entity }
        end
      end
    end

    def update
      respond_to do |format|
        if @rule_template.update(rule_template_params)
          format.html { redirect_to @rule_template, notice: 'Rule template was successfully updated.' }
          format.json { render :show, status: :ok, location: @rule_template }
        else
          format.html { render :edit }
          format.json { render json: @rule_template.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
      rule_template = RuleTemplate.find_by_id(params[:id])
      rule_template.destroy

      @templates = RuleTemplate.all

      @type = "reload_templates_side_nav"
      respond_to do |format|
        format.js {
          render :template => 'fire_wall_files/rule_templates/rule_templates.js.erb',
          :layout => false
        }
      end
    end

    private
    def set_rule_template
      @rule_template = RuleTemplate.find(params[:id])
    end

    def rule_template_params
      params.require(:rule_template).permit(:name)
    end
  end
end