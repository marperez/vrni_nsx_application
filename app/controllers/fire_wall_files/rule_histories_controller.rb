module FireWallFiles
  class RuleHistoriesController < ApplicationController
    before_action :set_rule_history, only: [:show, :edit, :update, :destroy]

    def index
      @rule_histories = RuleHistory.all
    end

    def show
    end

    def new
      @rule_history = RuleHistory.new
    end

    def edit
    end

    def remove_nsx_rule
      history_item = RuleHistory.find_by_id(params[:rule_id])

      RuleHistory.remove_nsx_rule(current_user, history_item)

      @rule_histories = RuleHistory.all
      @type = "update_histories"
      respond_to() do |format|
        format.js { render :action => "/user_files/users/admin_dashboard_js_erb/admin_dashboard.js.erb"}
      end
    end

    def resubmit_nsx_rule
      RuleHistory.resubmit_nsx_rule(params[:rule_id], current_user)
      
      @rule_histories = RuleHistory.all
      @type = "update_histories"
      respond_to() do |format|
        format.js { render :action => "/user_files/users/admin_dashboard_js_erb/admin_dashboard.js.erb"}
      end
    end

    def create
      @rule_history = RuleHistory.new(rule_history_params)

      respond_to do |format|
        if @rule_history.save
          format.html { redirect_to @rule_history, notice: 'Rule history was successfully created.' }
          format.json { render :show, status: :created, location: @rule_history }
        else
          format.html { render :new }
          format.json { render json: @rule_history.errors, status: :unprocessable_entity }
        end
      end
    end

    def update
      respond_to do |format|
        if @rule_history.update(rule_history_params)
          format.html { redirect_to @rule_history, notice: 'Rule history was successfully updated.' }
          format.json { render :show, status: :ok, location: @rule_history }
        else
          format.html { render :edit }
          format.json { render json: @rule_history.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
      @rule_history.destroy
      respond_to do |format|
        format.html { redirect_to rule_histories_url, notice: 'Rule history was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_rule_history
        @rule_history = RuleHistory.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def rule_history_params
        params.require(:rule_history).permit(:name, :action, :direction, :pack_type, :user_id)
      end
  end
end