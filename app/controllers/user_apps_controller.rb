class UserAppsController < ApplicationController
  before_action :set_user_app, only: [:show, :edit, :update, :destroy]

  # this is a has many through interm class
  # users has many apps through userapps
  def index
    @user_apps = UserApp.all
  end

  def new
    @user_app = UserApp.new
  end

  def create
    @user_app = UserApp.new(user_app_params)

    respond_to do |format|
      if @user_app.save
        format.html { redirect_to @user_app, notice: 'User app was successfully created.' }
        format.json { render :show, status: :created, location: @user_app }
      else
        format.html { render :new }
        format.json { render json: @user_app.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user_app.destroy
    respond_to do |format|
      format.html { redirect_to user_apps_url, notice: 'User app was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_app
      @user_app = UserApp.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_app_params
      params.require(:user_app).permit(:user_id, :app_id)
    end
end
