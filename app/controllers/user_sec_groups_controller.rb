class UserSecGroupsController < ApplicationController
  before_action :set_user_sec_group, only: [:show, :edit, :update, :destroy]

  # this is a has many through interm class
  # users has many secgroups through usersecgroups
  def index
    @user_sec_groups = UserSecGroup.all
  end

  def new
    @user_sec_group = UserSecGroup.new
  end

  def create
    @user_sec_group = UserSecGroup.new(user_sec_group_params)

    respond_to do |format|
      if @user_sec_group.save
        format.html { redirect_to @user_sec_group, notice: 'User sec group was successfully created.' }
        format.json { render :show, status: :created, location: @user_sec_group }
      else
        format.html { render :new }
        format.json { render json: @user_sec_group.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @user_sec_group.destroy
    respond_to do |format|
      format.html { redirect_to user_sec_groups_url, notice: 'User sec group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_sec_group
      @user_sec_group = UserSecGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_sec_group_params
      params.require(:user_sec_group).permit(:user_id, :sec_group_id)
    end
end
