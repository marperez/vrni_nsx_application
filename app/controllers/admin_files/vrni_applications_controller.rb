module AdminFiles
  class VrniApplicationsController < ApplicationController
    before_action :set_vrni_application, only: [:show, :edit, :update, :destroy]

    # this is shown in the settings index page
    def new
      @vrni_application = VrniApplication.new
    end

    def current
      # current_user.setting.setting_count = 0
      @vrni = VrniApplication.find_by_id(params[:id])

      increased = false
      current_user.setting.vrni_applications.all.each do |x|
        # if the nsx ip address exists and is current or not set (then decrease or increase the count)
        if (x.current == true)        
          x.current = false
        elsif (x.id == @vrni.id)
          x.current = true
          increased = true
        end
        x.save
      end

      within = false
      current_user.setting.nsx_managers.all.each do |x|
        if (x.current == true)        
          within = true
        end
      end

      if within == true && increased == true
        current_user.setting.setting_count = 2
      elsif (within != true && increased == true) || (within == true && increased != true)
        current_user.setting.setting_count = 1
      elsif (within != true && increased != true)
        current_user.setting.setting_count = 0
      end

      current_user.setting.save
      @vrni.save

      @type = "vrni"

      respond_to() do |format|
        format.js { 
            render :template => 'admin_files/settings/settings_resp.js.erb',
            :layout => false
        }
      end
    end

    def create
      vrni = VrniApplication.new
      vrni.vrni_addr = params[:vrni_addr]
      vrni.current = false
      vrni.save

      current_user.setting.vrni_applications << vrni
      current_user.save

      @type = "vrni"

      respond_to() do |format|
        format.js { 
            render :template => 'admin_files/settings/settings_resp.js.erb',
            :layout => false
        }
      end
    end

    # PATCH/PUT /vrni_applications/1
    # PATCH/PUT /vrni_applications/1.json
    def update
      respond_to do |format|
        if @vrni_application.update(vrni_application_params)
          format.html { redirect_to @vrni_application, notice: 'Vrni application was successfully updated.' }
          format.json { render :show, status: :ok, location: @vrni_application }
        else
          format.html { render :edit }
          format.json { render json: @vrni_application.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /vrni_applications/1
    # DELETE /vrni_applications/1.json
    def destroy
      vrni = VrniApplication.find_by_id(params[:id])

      if vrni.current == true # remember both the nsx info and vrni need to be selected to proceed past the settings page
        current_user.setting.setting_count -= 1
      end

      current_user.setting.save

      vrni.destroy

      @type = "vrni"

      respond_to() do |format|
        format.js { 
            render :template => 'admin_files/settings/settings_resp.js.erb',
            :layout => false
        }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_vrni_application
        @vrni_application = VrniApplication.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def vrni_application_params
        params.require(:vrni_application).permit(:vrni_addr, :setting_id, :password, :username)
      end
  end
end