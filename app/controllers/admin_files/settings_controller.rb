module AdminFiles
  class SettingsController < ApplicationController

    # GET /settings
    # GET /settings.json
    def index
      @settings = Setting.all
      if current_user.setting.nil?
        setting = Setting.new
        current_user.setting = setting
        setting.save
      end  

      @user = current_user
    end

    # GET /settings/1
    # GET /settings/1.json
    def show
    end

    # GET /settings/new
    def new
      @setting = Setting.new
    end

    # GET /settings/1/edit
    def edit
    end

    def settings_update
      setting = Setting.find_by_id(params[:id])

      if params[:type].to_s == "nsx"
        setting.nsx_password = params[:nsx_password]
        setting.nsx_username = params[:nsx_username]
        setting.save
      else
        setting.vrni_password = params[:vrni_password]
        setting.vrni_username = params[:vrni_username]
        setting.save
      end

      @type = "update_auth"

      respond_to() do |format|
        format.js { 
            render :template => 'admin_files/settings/settings_resp.js.erb',
            :layout => false
        }
      end
    end


    def settings_error
      @error = "Please activate 1 Manager and 1 VRNI Application to proceed"
      @type = "error"

      respond_to() do |format|
        format.js { 
            render :template => 'admin_files/settings/settings_path.js.erb',
            :layout => false
        }
      end
    end

    def settings_save
      @type = "save"
      redirect_back(fallback_location: root_path)
    end

    def settings_cancel

      @type = "cancel"
      redirect_back(fallback_location: root_path)
    end

    def create 
      @setting = Setting.new(setting_params)
      @setting.save
    end

    def update_nsx
      nsx = NsxManager.find_by_id(params[:nsx])
      @type = "nsx"

      nsx.update(ip_addr: params[:ip_addr])
      respond_to() do |format|
        render :action => "admin_files/settings/settings_resp.js.erb"
      end
    end

    def update_vrni
      vrni = VrniApplication.find_by_id(params[:vrni])
      @type = "vrni"

      vrni.update(vrni_addr: params[:vrni_addr])
      respond_to() do |format|
        render :action => "admin_files/settings/settings_resp.js.erb"
      end
    end

    def delete_nsx
      nsx = NsxManager.find_by_id(params[:nsx])

      if nsx.current == true # remember both the nsx info and vrni need to be selected to proceed past the settings page
        current_user.setting.setting_count -= 1
      end

      ""
      
      nsx.destroy

      @type = "nsx"
      respond_to() do |format|
        render :action => "admin_files/settings/settings_resp.js.erb"
      end
    end

    def delete_vrni
      vrni = VrniApplication.find_by_id(params[:vrni])

      if vrni.current == true # remember both the nsx info and vrni need to be selected to proceed past the settings page
        current_user.setting.setting_count -= 1
      end

      vrni.destroy

      @type = "vrni"
      respond_to() do |format|
        render :action => "admin_files/settings/settings_resp.js.erb"
      end
    end

    private

      # Never trust parameters from the scary internet, only allow the white list through.
      def setting_params
        params.require(:setting).permit(:current, nsx_manager_attributes: [:id, :nsx_addr, :setting_id, :password, :username], vrni_application_attributes: [:id, :vrni_addr, :setting_id, :password, :username])
      end
  end
end