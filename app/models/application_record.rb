class ApplicationRecord < ActiveRecord::Base
	self.abstract_class = true

	require 'HTTParty'
	require 'json'
	HTTParty::Basement.default_options.update(verify: false)

	def self.get_dfw_sections(current_user)
		nsx_ip_addr = Setting.current_nsx
		url = "https://" + nsx_ip_addr + "/api/4.0/firewall/globalroot-0/config"
		auth =  {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
		headers = {'Content-Type' => 'application/xml'}
		byebug
		resp = HTTParty.get(url, :headers => headers, :basic_auth => auth)
		return resp
	end

	def self.nsx_mgr_auth(current_user)
		vrni_user = current_user.setting.vrni_username
		vrni_pass = current_user.setting.vrni_password

		current_vrni = Setting.current_vrni_user(current_user)

		url = 'https://' + current_vrni + '/api/ni/auth/token'
		payload = '{ "username": "' + vrni_user + '",' + '"password": "' + vrni_pass + '",' +
		'"domain":  {' + 	'"domain_type": "LOCAL"' + '}' + '}'

		# "------------------------------------------------------------------------------------------")
		response = HTTParty.post(url, :headers => {'Content-Type' => 'application/json', 'Accept' =>'application/json'}, :body => payload)
		# "------------------------------------------------------------------------------------------")
		data = JSON.parse(String(response.body))
		auth = data["token"]

		if data['message'] == "Unauthorized"
			# do something as the user is no longer authorized
			# or just do that every 4 hrs call this method
		end

		puts(auth.to_s + " = Authentication token")
		network_insight = 'NetworkInsight ' + String(auth)
		# network_insight = 'NetworkInsight ' + "r2oiRozo5BtEx1WH+FEQKA=="

		@auth_header = {'Content-Type' => 'application/json', 'Accept' => 'application/json', 'Authorization' => network_insight}

		return @auth_header
	end

	def self.convert_column(reason, selected_operator, selected_column, user_input, do_not_add_inputs_later, origin)
		case selected_column
			when "Name"
				column = "fire_rules.name"
			when "Action"
				if origin == "all_dfw_rules"
					column = "fire_rules.action"
				elsif  origin == "rule_histories_partial"
					column = "action"
				end
			when "Source"
				if origin == "all_dfw_rules"
					column = "sources.sour_value"
				elsif origin == "flows_partial" || origin == "dash_flows_partial"
					column = "flows.source"
				elsif  origin == "rule_histories_partial"
					if selected_operator == "Equal to"
						column = "(select sour_name from sources where sources.rule_history_id = rule_histories.id) in ('#{user_input}')"
						do_not_add_inputs_later = true
					elsif selected_operator == "Is Like"

					end	
				end
			when "Destination"
				if origin == "all_dfw_rules"
					column = "destinations.dest_value"
				elsif origin == "flows_partial" || origin == "dash_flows_partial"
					column = "flows.destination"
				elsif  origin == "rule_histories_partial"
					if selected_operator == "Equal to"
						column = "(select dest_name from destinations where destinations.rule_history_id = rule_histories.id) in ('#{user_input}')"
						do_not_add_inputs_later = true
					elsif selected_operator == "Is Like"

					end	
				end
			when "Service"
				column = "services.serv_name"
			when "Port"
				if origin == "all_dfw_rules"

				elsif origin == "flows_partial" || origin == "dash_flows_partial"
					column = "flows.port"
				elsif  origin == "rule_histories_partial"

				end
			when "Start port"
				if origin == "all_dfw_rules"
					column = "port_ranges.startport"
				elsif origin == "rule_histories_partial"
					column = "port_ranges.start_port"
				end
			when "End port"
				if origin == "all_dfw_rules"
					column = "port_ranges.endport"
				elsif  origin == "rule_histories_partial"
					column = "port_ranges.end_port"
				end
			when "Protocol"
				if origin == "all_dfw_rules"

				elsif origin == "flows_partial" || origin == "dash_flows_partial"
					column = "flows.protocol"
				end
			when "Port"
				column = "port as int"
			when "Σ sent bytes"
				column = "sum(sent_bytes)"
			when "Σ received bytes"
				column = "sum(received_bytes)"
			when "Σ sessions"
				column = "sum(sessions)"
			when "Time sent"
				if origin == "dash_flows_partial" && reason = "order"
					column = "min(time_out)"
				else
					column = "time_out"
				end
			when "Time received"
				if origin == "dash_flows_partial" && reason = "order"
					column = "max(time_rec)"
				else
					column = "time_rec"
				end
			when "Employee Name"
				column = "users.name"
			when "Employee Email"
				column = "users.email"
			when "Σ Active Security Groups"
				column = "(SELECT COUNT(*) FROM sec_groups WHERE sec_groups.user_id = users.id )"
			when "Σ Rules Published"
				column = "(SELECT COUNT(*) FROM rule_histories WHERE rule_histories.user_id = users.id )"
			when "Last Seen"
				column = "users.updated_at"
			when "Published By"
				column = "username"
			when "Rule Name"
				column = "rule_name"
			when "Date Published"
				column = "created_at"
			when "Service"
				column = "services.serv_name"
			when "Undo/Redo"
				column = "undo_rule"
			when "Port Ranges"
				column = "port_range"
		end
		return_array = [column, do_not_add_inputs_later]
		return return_array
	end

	def self.convert_operator(selected_operator, user_input)
		case(selected_operator)
		when "Less than"
			operator = " < "
		when "Equal to"
			operator = " = "
		when "Greater than"
			operator = " > "
		when "Greater than or equal to"
			operator = " >= "
		when "Less than or equal to"
			operator = " <= "
		when "Not Equal to"
			operator = " != "
		when "Is Like"
			operator = " LIKE '%#{user_input}%' "
		end
		return operator
	end

	def self.get_full_column_sql_name(sql_syntax_for_column, origin)
		if sql_syntax_for_column == "Date Published" or sql_syntax_for_column == "created_at"
			case(origin)
				when "all_dfw_rules"
					sql_syntax_for_column = "fire_rules.created_at"
				when "flows_partial", "dash_flows_partial"
					sql_syntax_for_column = "flows.created_at"
				when "user_table_partial"
					sql_syntax_for_column = "users.created_at"
				when "rule_histories_partial"
					sql_syntax_for_column = "rule_histories.created_at"
			end
		end
		return sql_syntax_for_column
	end

	def self.add_children_table_objs_to_this_rule(sysobj, table, rule)
		# rule = SugRule.current

		entity_name = sysobj.vm_name # test123
		entity_value = sysobj.vm_moid # security-group-37
		entity_type = sysobj.vm_type # SecurityGroup

		entity_id = VrniApplication.find_entity_id(entity_type, entity_value) # api call to get the entity id

		if table == "sources"
			source = Source.create!(entity_id: entity_id, sour_type: entity_type, sour_name: entity_name, sour_value: entity_value)
			rule.sources << source
			rule.save
		elsif table == "destinations"
			destination = Destination.create!(entity_id: entity_id, dest_type: entity_type, dest_name: entity_name, dest_value: entity_value)
			rule.destinations << destination
			rule.save
		elsif table == "port_range"

		elsif table == "protocol"

		elsif table == "service"
			protocol = Service.new(serv_type: serv_type, serv_value: serv_value, serv_name: serv_name, serv_is_valid: true)
			@rule.services.add(protocol)
			@rule.services.save
		elsif table == "action"
			
		end
	end

	def self.set_rule_and_group_nsx_vars(current_user, nsx_response_raw, input_id, origin)
		if origin == "rule_history"
			selection = RuleHistory.find_by_id(input_id)
		elsif origin == "security_group"
			selection = SecGroup.find_by_id(input_id)
		end
			
		first_bool = false
		nsx_response_xml = Nokogiri::XML(nsx_response_raw.body)

		nsx_response_xml.xpath('//section').each do |section|

			if origin == "rule_history"
				section.xpath('//rule').each do |rule|
					# CREATE
					if input_id == 0 # if we have multiple rules (on first batch deployment)
						selection = RuleHistory.find_by(rule_name: rule.xpath('name').text)
						selection.nsx_rule_id = rule.attribute("id").text
						selection.rule_name = rule.xpath("name").text
						selection.section_id = section.attribute("id").text
						selection.section_name = section.attribute("name").text
						selection.section_etag = section.attribute("generationNumber").text

						selection.save
					else # we are deleting an existing rule
						if !selection.nil? && selection.rule_name == rule.xpath("name").text
							if !first_bool
								selection.nsx_rule_id = rule.attribute("id").text
								selection.rule_name = rule.xpath("name").text
								selection.section_id = section.attribute("id").text
								selection.section_name = section.attribute("name").text
								selection.section_etag = section.attribute("generationNumber").text
								selection.save
							end
							first_bool = true
						end
					end
				end
				# now we check if there were no matching rules but instead a section meaning the rule was previously deleted
				if first_bool == false   # RE-CREATE RULE IN NSX DFW
					section.xpath('//rule').each do |rule|

						if !selection.nil? && selection.section_id == section.attribute("id").text && first_bool == false
							selection.section_name = section.attribute("name").text
							selection.section_etag = section.attribute("generationNumber").text
							selection.save
							first_bool = true
						end
					end
				end
			elsif origin == "security_group"
				if selection.name = section.attribute('name').text
					selection.section_id = section.attribute("id").text
					selection.section_etag = section.attribute("generationNumber").text
					selection.save
				end
			end
		end
		return selection
	end

end
