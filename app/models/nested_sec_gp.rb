class NestedSecGp < ApplicationRecord
	belongs_to :sec_group, optional: true # if the member is a Security Group it is added to the parent, if not use the App Object Class

	has_many :app_objs, dependent: :destroy
	
	has_many :flows;
	# only use dependent destroy on the has_many, or you will have orphans if a child is deleted. The parents will also be removed but the other children will be spared
end
