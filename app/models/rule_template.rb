class RuleTemplate < ApplicationRecord
	has_many :fire_rules

	has_many :sec_group_templates
	has_many :sec_groups, through: :sec_group_templates

	def self.search(input)
		if input == ""
			return RuleTemplate.all
		else
			test_against = "%#{input}%"
			return RuleTemplate.where("name LIKE ?", test_against)
		end
	end

end
