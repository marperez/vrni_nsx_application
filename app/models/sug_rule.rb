class SugRule < ApplicationRecord
	belongs_to :tier, optional: true
	belongs_to :sec_group, optional: true
	has_many :protocols, dependent: :destroy
	has_many :applied_tos
	has_many :destinations, dependent: :destroy
	has_many :sources, dependent: :destroy
	has_many :port_ranges, dependent: :destroy
	has_many :services, dependent: :destroy

	def self.create_sug_rules(id, type, current_user)
		SugRule.nsx_mgr_auth(current_user)

		# -------------------------------------
		# ---------- Suggested Rules ----------
		# -------------------------------------

		sec_payload = "{"

		time_then = Time.now.to_i - (30 * 24 * 60 * 60) # weeks ago in seconds
		time_now = Time.now.to_i

		parent_object = nil
		if type == 'sec_group'
			parent_object = current_user.sec_groups.find_by(id)
		else type == "app"
			parent_object = App.find_by(id: id)
		end

		parent_object.sug_rules.delete_all # if something exists clear it/them

		if type == 'sec_group'
			sec_payload += '
			"group_1": {
				"entity": {
					"entity_type": "NSXSecurityGroup",
					"entity_id": "''' + parent_object.object_entity_id + '''"
				}
			}, "time_range": 
				{
					"start_time": "' + String(time_then) + '",
					"end_time": "'+ String(time_now) + '"
				}'
		elsif type == "app"
			sec_payload += '
				"group_1": {
					"entity": {
						"entity_type": "Application",
						"entity_id": "' + parent_object.app_vrni_id + '"
					}
				}, "time_range": 
				{
					"start_time": "' + String(time_then) + '",
					"end_time": "'+ String(time_now) + '"
				}'
		else # currently the code in this else block doesn't work but it seems like the app above does the job for it (tier rules are being returned (search span == 2 weeks))
			num = 1
			parent_object.tiers.each do |tier|
				group = "group_" + num.to_s
				sec_payload += '
				"'+ group +'": {
					"entity": {
						"entity_type": "Tier",
						"entity_id": "' + tier.tier_vrni_id + '"
					}
				},'
				num += 1
			end
			sec_payload += '"time_range": {"start_time": "' + String(time_then) + '","end_time": "'+ String(time_now) + '"}'
		end
		sec_payload += "}"


		@current_vrni = Setting.current_vrni
		
		url = "https://" + @current_vrni + "/api/ni/micro-seg/recommended-rules"
		response = HTTParty.post(url, :body => sec_payload, :headers => @auth_header)
		loadedJson = JSON.parse(response.body)

		###########################################*/
		# ------------ Create Sug Rule ---------- #*/
		###########################################*/

		if loadedJson && loadedJson['results'] && loadedJson['results'].count > 0

			loadedJson['results'].each_with_index do |rule, index|

				sug_rule = SugRule.new(
						action: rule['action'].upcase
				)

				sug_rule.name = "rule_" + index.to_s

				rule['sources'].each do |source|
					entity_id = source['entity_id']
					entity_type = source['entity_type']
					# if source['entity_type'].to_s == "Tier"
					# 	entity_type = "SecurityGroup"
					# end
					url = ("https://" + @current_vrni + "/api/ni/entities/names/" + entity_id)
					resp = HTTParty.get(url, :headers => @auth_header)
					obj_name = (String(JSON.parse(resp.body)['name']))
					temp = Vm.find_by(vm_name: obj_name)
					if temp
						obj_id = temp.vm_moid
						source = Source.create!(entity_id: entity_id, sour_type: entity_type, sour_name: obj_name, sour_value: obj_id)
						sug_rule.sources << source
						
					else
						# This is triggered for Tiers
						source = Source.create!(entity_id: entity_id, sour_type: entity_type, sour_name: obj_name, sour_value: "")
						sug_rule.sources << source
											
					end
				end

				rule['destinations'].each do |destination|
					entity_id = destination['entity_id']
					entity_type = destination['entity_type']
					if destination['entity_type'].to_s == "Tier"
						entity_type = "SecurityGroup"
					end
					url = ("https://" + @current_vrni + "/api/ni/entities/names/" + entity_id)
					resp = HTTParty.get(url, :headers => @auth_header)
					obj_name = (String(JSON.parse(resp.body)['name']))
					temp = Vm.find_by(vm_name: obj_name)
					if temp
						obj_id = temp.vm_moid
						destination = Destination.create!(entity_id: entity_id, dest_type: entity_type, dest_name: obj_name, dest_value: obj_id)
						sug_rule.destinations << destination
						
					else
						destination = Destination.create!(entity_id: entity_id, dest_type: entity_type, dest_name: obj_name, dest_value: "")
						sug_rule.destinations << destination
						
					end
				end

				begin
					ActiveRecord::Base.transaction do
						rule['protocols'].each do |protocol|
							sug_rule.protocols.first_or_initialize(name: protocol).save
						end
					end
				rescue => e
				  # something went wrong, transaction rolled back
				end
				begin
					ActiveRecord::Base.transaction do
						rule['port_ranges'].each do |port_range|
							startport = port_range['start']
							endport = port_range['end']
							sug_rule.port_ranges.first_or_initialize(startport: startport, endport: endport).save
						end
					end
				rescue => e
				  # something went wrong, transaction rolled back
				end

				check = parent_object.sug_rules.find_by_id(sug_rule.id)
				if !parent_object.sug_rules.find_by_id(sug_rule.id)
					parent_object.sug_rules << sug_rule
					parent_object.save
				else
					temp = sug_rule.to_s + " already exists"
					puts(temp)
				end

				sug_rule.save
			end
			return parent_object.sug_rules
		end
	end

	def self.delete_associated_rule(rule)
	    rule.destroy
	end
end
