class App < ApplicationRecord
	has_many :tiers, dependent: :destroy

	has_many :user_apps
	has_many :users #, through: :user_apps

	has_many :temp_fire_rules

	require 'CSV'

	HTTParty::Basement.default_options.update(verify: false)
	@app_entity_id = ""

	@app_array = []
	@app_entity_id_array = []

	def self.find_vim_id
		myheaders = {'Content-Type' => 'application/xml'}
		auth =  {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
		nsx_ip_addr = Setting.current_nsx
		url = 'https://'+ nsx_ip_addr + '//api/2.0/services/securitygroup/scope/globalroot-0/members/'
		# get every object in the system, tags, vms, vncs ...
		response = HTTParty.get(
			url,
			:headers => myheaders,
			:basic_auth => auth
			)
		obj_hash = {}
		parsable_json = Nokogiri::XML.parse(response.body)
		Vm.delete_all
		parsable_json.xpath('//basicinfo').each do |x|
			puts(x.xpath('objectId').text)
			Vm.create!(vm_moid: x.xpath('objectId').text, vm_name: x.xpath('name').text)
		end
	end

	def self.import(file)
		App.delete_all
		App.nsx_mgr_auth(current_user) # get the authentication for the nsx mgr
		App.find_vim_id
		num = 0
		app = ""
		tier = ""
		CSV.foreach(file, headers: true) do |row|
			
			if row[0].to_s.length > 0
				# create and save an app
				App.new_app(row[0])
				app = App.create!(name: row[0], app_vrni_id: @app_entity_id)
				@app_array << app
			end

			if row[1].to_s.length > 0
				# add an tier to the app
				tier = Tier.create!(name: row[1])
				app.tiers << tier
			end

			if row[2].to_s.length > 0
				# add an object to the tier
				# Find the Security Group ID
				url = "https://10.10.97.32/api/ni/search"
				if row[3] == "SecurityGroup"
					sec_gp_payload = '''{
					"entity_type": "NSXSecurityGroup",
					"filter": "name = ''' + row[2] + '''"
					} '''
				else
					sec_gp_payload = '''{
					"entity_type": "VirtualMachine",
					"filter": "name = ''' + row[2] + '''"
					} '''
				end
				response = HTTParty.post(url, body: sec_gp_payload, headers: @auth_header)
				object_entity_id = ""
				loadedJson = JSON.parse(response.body)
				if loadedJson['results']
					loadedJson['results'].each do |a| # {"results":[{"entity_id":"x","entity_type":"NSXSecurityGroup","time":y}],"total_count":1,"start_time":z,"end_time":w}
						object_entity_id = a['entity_id']
					end
				end

				moid = Vm.find_by(vm_name: row[2])
				if (!moid.nil?) # safty net, perchance the analyst put an incorrect object name within the csv file
					object = AppObj.create!(obj_name: row[2], obj_type: row[3], obj_entity_id: object_entity_id, obj_id: moid.vm_moid)
					tier.app_objs << object
				end
				# ""
			end
		end

		@app_array.each do |a|
			a.tiers.each do |t|
				App.create_tier(t, a.app_vrni_id)
				# App.add_sug_rules(t)
			end
		end
	end

	

	# ----------------------------------------------------------------------------------------- #
									# CREATING NEW APPS
	# ----------------------------------------------------------------------------------------- #


	def self.new_app(app_name)
		create_and_get_app_id(app_name)
	end

	def self.create_and_get_app_id(app_name)
		# url for api to create a application 
		url = "https://10.10.97.32/api/ni/groups/applications"
		# required body to create an application in vrni
		request_body = '{"name": "' + app_name + '"}'

		response = HTTParty.post(
			url,
			:headers => @auth_header,
			:body => request_body
			)

		data = JSON.parse(String(response.body))

		if(String(data).include? "Duplicate")
			get_vrni_app_id(app_name)
		else
			@app_entity_id_array << data
			@app_entity_id = data['entity_id']
		end
		# ""
	end

	def self.get_vrni_app_id(app_name)
		url = "https://10.10.97.32/api/ni/groups/applications"
		response = HTTParty.get(
			url,
			:headers => @auth_header
			)
		data = JSON.parse(response.body)
		entity_array = []
		nextitem = false 
		for k,v in data
			if k == "results"
				for i in v
					url = ("https://10.10.97.32/api/ni/entities/names/" + i['entity_id'])
					resp = HTTParty.get(url, :headers => @auth_header)
					if(JSON.parse(resp.body)['name'] == app_name )
						@app_entity_id = i['entity_id']
						@app_entity_id_array << @app_entity_id
					end
					# ""
				end
			end
		end
	end

	def self.delete_app(app_name)
		url = "https://10.10.97.32/api/ni/groups/applications"
		response = HTTParty.get(
			url,
			:headers => @auth_header
			)
		data = JSON.parse(response.body)
		entity_array = []
		nextitem = false 
		for k,v in data
			if k == "results"
				for i in v
					url = ("https://10.10.97.32/api/ni/entities/names/" + i['entity_id'])
					resp = HTTParty.get(url, :headers => @auth_header)
					if(JSON.parse(resp.body)['name'] == app_name )
						delete_vrni_app(i['entity_id'])
					end
				end
			end
		end
	end

	def self.delete_vrni_app(app_id)
		url = "https://10.10.97.32/api/ni/groups/applications/" + app_id
			#delete vrni app
			response = HTTParty.delete(
				url,
				:headers => @auth_header
				)
			@app_entity_id_array << response.body
		end



									# END OF CREATING APPS
	# ----------------------------------------------------------------------------------------- #

	def self.create_tier(tier, app_id)

		body = '{
		"name": "' + tier.name + '",
		"group_membership_criteria" : ['

			num = 0
			for i in tier.app_objs
				if i.obj_type == "SecurityGroup"
					body += '{
					"membership_type": "SearchMembershipCriteria",
					"search_membership_criteria": {
						"entity_type" : "VirtualMachine",
						"filter": "security_groups.name = ' + i.obj_name + '"
					}
					},'
				elsif i.obj_type == "VirtualMachine"
					if i.obj_name.include? "."
						body += '{
						"membership_type": "SearchMembershipCriteria",
						"search_membership_criteria": {
							"entity_type" : "VirtualMachine",
							"filter": "ip_addresses.ip_address = ' + i.obj_name + '"
						}
						},'
					else
						body += '{
						"membership_type": "SearchMembershipCriteria",
						"search_membership_criteria": {
							"entity_type" : "VirtualMachine",
							"filter": "Name = ' + i.obj_name + '"
						}
						},'
					end
				elsif i.obj_type == "Host"
					vbody += '{
					"membership_type": "SearchMembershipCriteria",
					"search_membership_criteria": {
						"entity_type" : "VirtualMachine",
						"filter": "host.Name = ' + i.obj_name + '"
					}
					},'
				end
			end

			body = body[0...-1]
			body += ']}'

			url = "https://10.10.97.32/api/ni/groups/applications/" + app_id + "/tiers"

			response = HTTParty.post(
				url,
				:headers => @auth_header,
				:body => body
				)

			vrni_id = JSON.parse(response.body)['entity_id']
			tier.update(tier_vrni_id: vrni_id)
		end

		def self.add_sug_rules(tier)

			sec_payload = "{"
			for x in entity_array
				entity_id = x[0]
				entity_type = x[1]
				temp_gp = '''
				"group_'''+ String(n) +'''": {
					"entity": {
						"entity_type": "''' + entity_type + '''",
						"entity_id": "''' + entity_id + '''"
					}
					},
					'''
					sec_payload += temp_gp
					n += 1
				end

				time_now = String(Time.now.to_i)
				sec_payload += '''
				"time_range": {
					"start_time": "1516998984",
					"end_time": "''' + time_now + '''"
				}
				}'''

				url = "https://10.10.97.32/api/ni/micro-seg/recommended-rules"
				response = HTTParty.post(url, :body => sec_payload, :headers => auth_header)

		dest_text = "" # what we will create to insert into the HTTP Request Body
		source_text = "" # what we will create to insert into the HTTP Request Body
		loadedJson = JSON.parse(response.body)

		if loadedJson['results'].count > 0
			loadedJson['results'].each do |a|

				# "--------- Sources --------"
				a.each do |b|
					b.each do |c|
						entity_id = c['entity_id'].to_s

						url_sec_gp_url = ("https://10.10.97.32/api/ni/entities/security-groups/" + entity_id)
						source_sec_gp = HTTParty.get(url_sec_gp_url, :headers => auth_header)
						source_json = JSON.parse(source_sec_gp.body)

							# print("\n------source--------------\n")

							for d in source_json['results']
								entity_id = d['entity_id']
								source_temp_txt = '''
								<source>
								<value>''' + source_sec_gp_name + '''</value>
								<type>Ipv4Address</type>
								<isValid>true</isValid>
								</source>'''
							end
							source_text += source_temp_txt
						end
						print("\n--------------------\n")
					end
					url = "https://10.10.97.32/api/ni/entities/security-groups/" + source_id
					source_vms = requests.get(url, :headers =>auth_header)
					source_json = JSON.parse(source_vms)
					for b in source_json['direct_members']
						entity_id = b['entity_id']

						url = ("https://10.10.97.32/api/ni/entities/vms/" + entity_id)
						resp = requests.get(url, :headers =>auth_header)
						source_vm_json = JSON.parse(resp)
						source_vm_network_info = source_vm_json['ip_addresses']
						for c in source_vm_network_info
							source_vm_ip = String(c['ip_address'])
							source_temp_txt = '''
							<source>
							<value>''' + source_vm_ip + '''</value>
							<type>Ipv4Address</type>
							<isValid>true</isValid>
							</source>'''
							source_text += source_temp_txt
						end
					end
				end

				# "--------- Destinations --------"
				dest_id = a['destinations'][0]['entity_id']
				url = "https://10.10.97.32/api/ni/entities/security-groups/" + dest_id
				dest_vms = requests.get(url, :headers => auth_header)
				dest_json = JSON.parse(dest_vms)
				for b in dest_json['direct_members']
					entity_id = b['entity_id']

					url = ("https://10.10.97.32/api/ni/entities/vms/" + entity_id)
					resp = requests.get(url, :headers => auth_header)
					dest_vm_json = JSON.parse(resp)
					dest_vm_network_info = dest_vm_json['ip_addresses']
					for c in dest_vm_network_info
						dest_vm_ip = String(c['ip_address'])
						dest_temp_txt = '''
						<destination>
						<value>''' + dest_vm_ip + '''</value>
						<type>Ipv4Address</type>
						<isValid>true</isValid>
						</destination>'''
						dest_text += dest_temp_txt
					end
				end

				# "--------- Services --------"
				end_port = String(a['port_ranges'][0]['end'])
				protocol_name = String(a['protocols'][0])

				service_text = '''
				<service>
				<isValid>true</isValid>
				<destinationPort>''' + end_port + '''</destinationPort>
				<protocol>6</protocol>
				<protocolName>''' + protocol_name + '''</protocolName>
				</service>'''

				# "--------- Action --------"
				action = a['action'].lower()
			end
	end
end