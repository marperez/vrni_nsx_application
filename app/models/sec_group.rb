class SecGroup < ApplicationRecord

	has_many :user_sec_group
	has_many :users
	has_many :users , through: :user_sec_group

	has_many :sec_group_templates
	has_many :rule_templates, through: :sec_group_templates

	has_many :nested_sec_gps, dependent: :destroy
	has_many :app_objs, dependent: :destroy
	has_many :sug_rules, dependent: :destroy
	has_many :flows, dependent: :destroy

	has_many :temp_fire_rules
	# only use dependent destroy on the has_many, or you will have orphans if a child is deleted. The parents will also be removed but the other children will be spared

	require 'nokogiri'

	HTTParty::Basement.default_options.update(verify: false)


	require 'set'
	def self.retrieve_sec_groups(current_user)

		nsx_mgr_auth(current_user)
		find_old_groups_array = SecGroup.pluck(:name)

		nsx_ip_addr = Setting.current_nsx
		# Get Security Group General Info
		auth = {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
		headers = {'Content-Type' => 'application/xml'}
		url = "https://" + nsx_ip_addr + "/api/2.0/services/securitygroup/scope/globalroot-0"

		retrieve_sec_groups_nsx_response = HTTParty.get(url, :headers => headers, :basic_auth => auth)

		doc = Nokogiri.XML(retrieve_sec_groups_nsx_response.body) do |config|
			config.default_xml.noblanks
		end

		xml_body = doc.to_xml(:indent => 2)
		@current_vrni = Setting.current_vrni

		url = "https://" + @current_vrni + "/api/ni/entities/security-groups" # security groups available for VRNI suggested rules
		query = { "size" => 10000 }

		response = HTTParty.get(url, headers: @auth_header, :query => query )
		vrni_groups = JSON.parse(response.body)['results']

		comparison_array = []

		# The following will only create the security groups if both VRNI and NSX agree
		vrni_hash = {}
		if vrni_groups.nil?
			return "settings_path"
		end
		
		vrni_groups.each do |vrni_item|
			url = ("https://" + @current_vrni + "/api/ni/entities/names/" + vrni_item['entity_id'])
			resp = HTTParty.get(url, headers: @auth_header)
			item_name = JSON.parse(resp.body)
			item_hash = {item_name['name'] => item_name['entity_id']}
			comparison_array << item_name['name']
			vrni_hash.merge!(item_hash)
		end

		result_array = []
		result_array = find_old_groups_array - comparison_array
		if result_array
			ActiveRecord::Base.transaction do
				result_array.each do |x|
					group = SecGroup.find_by(name: x)
					SecGroup.destroy(group.id)
				end
			end
		end

		ActiveRecord::Base.transaction do
			doc.xpath('//securitygroup').each do |vrni_item| # VRNI and NSX Manager are time delayed
				if vrni_hash.has_key?(vrni_item.xpath('name').text)
					add_members_to_a_security_group(vrni_item, vrni_hash, current_user)
				else
					# ProblemL generally we either in the 10 mins window for VRNI or it is having issues communicating with the NSX Manager.
				end
			end
		end
	end

	def self.add_members_to_a_security_group(sg, vrni_hash, current_user)

		@vm_set = Set.new

		@nsx_ip_addr = Setting.current_nsx
		entity_id = vrni_hash.values_at(sg.xpath('name').text)
		entity_id = entity_id.first

		sec_group = SecGroup.find_by(object_entity_id: entity_id)
		if !sec_group.nil?
			sec_group.update(name: sg.xpath('name').text, object_id: sg.xpath('objectId').text, object_entity_id: entity_id)
			sec_group = SecGroup.find_by(object_entity_id: entity_id)
		else
			sec_group = SecGroup.create(name: sg.xpath('name').text, object_id: sg.xpath('objectId').text, object_entity_id: entity_id)
		end

		# Part 1 is for objects that use security tags as group qualifiers, Part 2 is for those with static assignment or not assigned
		membership_by_tag = sg.xpath('dynamicMemberDefinition/dynamicSet/dynamicCriteria/value').text
		if membership_by_tag != "" # Part 1
			vmware_tag_obj = Vm.find_by(vm_name: membership_by_tag, vm_type: "SecurityTag")
			if !vmware_tag_obj.nil?
				tag_moid = vmware_tag_obj.vm_moid

				url = 'https://' + @nsx_ip_addr  + '/api/2.0/services/securitytags/tag/' + tag_moid + '/vm'
				myheaders = {'Content-Type' => 'application/xml'}
				auth = {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
				all_tag_vms_response = HTTParty.get(url, :headers => myheaders, :basic_auth => auth )

				all_tag_item_array = Nokogiri.XML(all_tag_vms_response.body)
				all_tag_item_array = all_tag_item_array.xpath('//basicinfolist/basicinfo')
				
				all_tag_item_array.each do |member|
					@vm_set.add('"' + member.xpath('name').text + '"')
					SecGroup.add_members_to_sec_group_locally(sec_group, member)
				end
				sec_group.save
			end
		else # Part 2
			if sg.xpath('member') && sg.xpath('member').length > 0
				SecGroup.transaction do
					sg.xpath('member').each do |member|
						@vm_set.add('"' + member.xpath('name').text + '"')
						SecGroup.add_members_to_sec_group_locally(sec_group, member)
					end
				end
			end
		end

		# Clear out Virtual Machines that were manually deleted on nsx but are still in our local db
		vmware_object_grouping = ""
		if @vm_set.size() > 0
			@vm_set.each do |vmware_object|
				vmware_object_grouping += (vmware_object + ", ")
			end
			vmware_object_grouping = vmware_object_grouping[0...-2]

			delete_statement = "delete from app_objs where sec_group_id = #{sec_group.id} and obj_name not in (#{vmware_object_grouping})"
			ActiveRecord::Base.connection.execute(delete_statement)
		end
	end

	def self.add_members_to_sec_group_locally(sec_group, member)

		entity_type = member.xpath('objectTypeName').text
		entity_value = member.xpath('objectId').text

		api_method = " "
		if entity_type == "Ipv4Address" || entity_type == "Ipv6Address"
			api_method = "ip_addresses.ip_address"
		else
			api_method = "vendor_id" # moid
		end

		# TO DO
		if entity_type == "SecurityGroup"
			entity_type = "NSXSecurityGroup"
		elsif entity_type == "ClusterComputeResource"
			entity_type = "Cluster"
		elsif entity_type == "Datacenter"
			entity_type = "VCDatacenter"
		end	

		if entity_type == "VirtualApp" || entity_type == "VirtualWire"
			# these are not available options for VRNI, see pgs. 26 - 27
			# https://vdc-download.vmware.com/vmwb-repository/dcr-public/b4af5fd2-1109-4a95-9dbb-32e5893263e7/eba7302a-485b-4f16-a749-e055521969b7/vRealize-Network-Insight-API-Guide-3.6.0.pdf
		else
			find_entity_id_payload = '{"entity_type": "' + entity_type + '","filter": "' + api_method + ' = ' + entity_value + '"}'
		end

		entity_id = ""
		if(find_entity_id_payload)
			@current_vrni = Setting.current_vrni
			url = "https://" + @current_vrni + "/api/ni/search"
			find_entity_id_response = HTTParty.post(url, body: find_entity_id_payload, headers: @auth_header)
			entity_info = JSON.parse(find_entity_id_response.body)['results'][0]

			# TODO Make this faster
			new_obj = nil
			if(entity_info)
				entity_id = entity_info['entity_id']
				# if a match then create the security group item
				if sec_group.app_objs.where(obj_id: member.xpath('objectId').text).exists?
					existing_app_obj = sec_group.app_objs.where(obj_id: member.xpath('objectId').text)
					existing_app_obj.update(
						obj_entity_id: entity_id,
						obj_id: member.xpath('objectId').text,
						obj_type: member.xpath('objectTypeName').text,
						obj_name: member.xpath('name').text
						)
					add_tag_relationship_to_app_obj(sec_group.name, existing_app_obj)	
				else
					new_app_obj = AppObj.new(
						obj_entity_id: entity_id,
						obj_id: member.xpath('objectId').text,
						obj_type: member.xpath('objectTypeName').text,
						obj_name: member.xpath('name').text
						)
					new_app_obj.save
					sec_group.app_objs << new_app_obj	
					add_tag_relationship_to_app_obj(sec_group.name, new_app_obj)		
				end
			end
		end
		sec_group.save
	end

	def self.add_tag_relationship_to_app_obj(tag_name, app_object)
		security_tag = SecTag.create(tag_name: tag_name)
		app_object.first.sec_tags << security_tag
		app_object.first.save
	end

	def self.add_sec_gp_to_nsx(current_user)

		@nsx_ip_addr = Setting.current_nsx

		App.all.each do |app|
			app_name = app.name

			parent_body = ""
			child_body = ""

			app_obj_id = create_sec_gp_and_get_object_id(app_name, current_user)
			Vm.create(vm_moid: app_obj_id, vm_name: app_name)
			# -----------------------------
			# GET THE REFERENCE TO THE APP
			tier_body = 
			'<securitygroup>
			<objectId>' + app_obj_id + '</objectId>
			<objectTypeName>SecurityGroup</objectTypeName>
			<revision>1</revision>
			<type>
			<typeName>SecurityGroup</typeName>
			</type>
			<name>' + app_name + '</name>
			<scope>
			<id>globalroot-0</id>
			<objectTypeName>GlobalRoot</objectTypeName>
			<name>Global</name>
			</scope>'

			id_array = [] # necessary for the object section
			array_index = 0 # necessary for the object section

			app.tiers.each do |tier|

				# ---------------------------------
				# ADD EACH CHILD TO THE APP PARENT
				tier_name = tier.name
				tier_obj_id = create_sec_gp_and_get_object_id(tier_name, current_user)
				id_array.push(tier_obj_id) # necessary for the object section
				tier_body += 
				'<member>
				<objectId>' + tier_obj_id + '</objectId>
				<objectTypeName>SecurityGroup</objectTypeName>
				<type>
				<typeName>SecurityGroup</typeName> # Tier type needs to be created ?
				</type>
				<name>' + tier_name + '</name>
				<scope>
				<id>globalroot-0</id>
				<objectTypeName>GlobalRoot</objectTypeName>
				<name>Global</name>
				</scope>
				</member>'
			end

			# --------------------------
			# CREATE THE TIER SEC GROUP
			tier_body += '</securitygroup>'
			url = 'https://' + @nsx_ip_addr + '/api/2.0/services/securitygroup/bulk/' + app_obj_id
			myheaders = {'Content-Type' => 'application/xml'}
			auth = {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
			res_tier = HTTParty.put(
				url,
				:headers => myheaders,
				:basic_auth => auth,
				:body => tier_body
				)

			# --------------------------
			# IF IT ALREADY EXISTS
			if res_tier.to_s.include? "older version" # ... The object securitygroup-96 used in this operation has an older version 1 than the current system version 5 ... 
				find_version = String(res_tier)[100...-100].gsub(/[^0-9]/,"").to_i # we need to one up the last version or NSX will complain
				version = find_version + 1

				tier_body = 
				'<securitygroup>
				<objectId>' + app_obj_id + '</objectId>
				<objectTypeName>SecurityGroup</objectTypeName>
				<revision>' + version.to_s + '</revision>
				<type>
				<typeName>SecurityGroup</typeName>
				</type>
				<name>' + app_name + '</name>
				<scope>
				<id>globalroot-0</id>
				<objectTypeName>GlobalRoot</objectTypeName>
				<name>Global</name>
				</scope>'

				id_array = [] # necessary for the object section
				array_index = 0 # necessary for the object section

				app.tiers.each do |tier|

					# ---------------------------------
					# ADD EACH CHILD TO THE APP PARENT
					tier_sec_name = tier.name
					tier_sec_obj_id = create_sec_gp_and_get_object_id(tier_sec_name)

					tier.update(sec_gp_moid: tier_sec_obj_id) # for later when we call nsx manager to create the rules

					# ""
					id_array.push(tier_sec_obj_id) # necessary for the object section
					tier_body += 
					'<member>
					<objectId>' + tier_sec_obj_id + '</objectId>
					<objectTypeName>SecurityGroup</objectTypeName>
					<type>
					<typeName>SecurityGroup</typeName> # Tier type needs to be created ?
					</type>
					<name>' + tier_sec_name + '</name>
					<scope>
					<id>globalroot-0</id>
					<objectTypeName>GlobalRoot</objectTypeName>
					<name>Global</name>
					</scope>
					</member>'
				end

				# --------------------------
				# CREATE THE TIER SEC GROUP
				tier_body += '</securitygroup>'
				url = 'https://' + @nsx_ip_addr + '/api/2.0/services/securitygroup/bulk/' + app_obj_id
				myheaders = {'Content-Type' => 'application/xml'}
				auth = {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
				res_tier = HTTParty.put(
					url,
					:headers => myheaders,
					:basic_auth => auth,
					:body => tier_body
					)
			end

			# ""

			app.tiers.each do |tier|

				# -------------------------
				# FIND THE ID OF EACH TIER
				tier_sec_obj_id = id_array[array_index]
				array_index += 1

				# ---------------------------------------------------------
				# ADD ALL THE APP OBJECTS TO THEIR PARENT (TIER) SEC GROUP
				app_obj = 
				'<securitygroup>
				<objectId>' + tier_sec_obj_id + '</objectId>
				<objectTypeName>SecurityGroup</objectTypeName>
				<revision>1</revision>
				<type>
				<typeName>SecurityGroup</typeName>
				</type>
				<name>' + tier.name + '</name>
				<scope>
				<id>globalroot-0</id>
				<objectTypeName>GlobalRoot</objectTypeName>
				<name>Global</name>
				</scope>'

				tier.app_objs.each do |object|
					app_obj += 
					'<member>
					<objectId>' + object.obj_id + '</objectId>
					<objectTypeName>VirtualMachine</objectTypeName>
					<type>
					<typeName>' + object.obj_type + '</typeName>
					</type>
					<name>' + object.obj_name + '</name>
					<scope>
					<id>globalroot-0</id>
					<objectTypeName>GlobalRoot</objectTypeName>
					<name>Global</name>
					</scope>
					</member>'
				end
				app_obj += '</securitygroup>'



				url = 'https://' + @nsx_ip_addr + '/api/2.0/services/securitygroup/bulk/' + tier_sec_obj_id

				myheaders = {'Content-Type' => 'application/xml'}
				auth = {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
				res_obj = HTTParty.put(
					url,
					:headers => myheaders,
					:basic_auth => auth,
					:body => app_obj
					)

				# --------------------------
				# IF IT ALREADY EXISTS
				if res_obj.to_s.include? "older version" # ... The object securitygroup-96 used in this operation has an older version 1 than the current system version 5 ... 
					find_version = String(res_obj)[100...-100].gsub(/[^0-9]/,"").to_i # we need to one up the last version or NSX will complain
					version = find_version + 1

					app_obj = 
					'<securitygroup>
					<objectId>' + tier_sec_obj_id + '</objectId>
					<objectTypeName>SecurityGroup</objectTypeName>
					<revision>' + version.to_s + '</revision>
					<type>
					<typeName>SecurityGroup</typeName>
					</type>
					<name>' + tier.name + '</name>
					<scope>
					<id>globalroot-0</id>
					<objectTypeName>GlobalRoot</objectTypeName>
					<name>Global</name>
					</scope>'

					tier.app_objs.each do |object|
						app_obj += 
						'<member>
						<objectId>' + object.obj_id + '</objectId>
						<objectTypeName>VirtualMachine</objectTypeName>
						<type>
						<typeName>' + object.obj_type + '</typeName>
						</type>
						<name>' + object.obj_name + '</name>
						<scope>
						<id>globalroot-0</id>
						<objectTypeName>GlobalRoot</objectTypeName>
						<name>Global</name>
						</scope>
						</member>'
					end
					app_obj += '</securitygroup>'

					url = 'https://' + @nsx_ip_addr + '/api/2.0/services/securitygroup/bulk/' + tier_sec_obj_id

					myheaders = {'Content-Type' => 'application/xml'}
					auth = {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
					res_obj = HTTParty.put(
						url,
						:headers => myheaders,
						:basic_auth => auth,
						:body => app_obj
						)
				end
			end
		end
	end

	def self.add_members_to_sec_group(current_user, parent_name, parent_obj_id, member_array)
		@nsx_ip_addr = Setting.current_nsx
		@parent = SecGroup.find_by(name: parent_name)

		payload =  '<securitygroup>
		<objectId>' + parent_obj_id + '</objectId>
		<objectTypeName>SecurityGroup</objectTypeName>
		<revision>1</revision>
		<type>
		<typeName>SecurityGroup</typeName>
		</type>
		<name>' + parent_name + '</name>
		<scope>
		<id>globalroot-0</id>
		<objectTypeName>GlobalRoot</objectTypeName>
		<name>Global</name>
		</scope>'
		member_array.each do |member|
			if member.is_a?(AppObj)
				payload += 
				'<member>
				<objectId>' + member.obj_id + '</objectId>
				<objectTypeName>VirtualMachine</objectTypeName>
				<type>
				<typeName>' + member.obj_type + '</typeName>
				</type>
				<name>' + member.obj_name + '</name>
				<scope>
				<id>globalroot-0</id>
				<objectTypeName>GlobalRoot</objectTypeName>
				<name>Global</name>
				</scope>
				</member>'
			elsif member.is_a?(NestedSecGp)
				payload += 
				'<member>
				<objectId>' + member.object_id + '</objectId>
				<objectTypeName>VirtualMachine</objectTypeName>
				<type>
				<typeName>' + member.object_type + '</typeName>
				</type>
				<name>' + member.object_name + '</name>
				<scope>
				<id>globalroot-0</id>
				<objectTypeName>GlobalRoot</objectTypeName>
				<name>Global</name>
				</scope>
				</member>'
			end
		end

		payload += '</securitygroup>'

		if parent_obj_id.include?("exists") && parent_obj_id.include?("same")
			url = 'https://' + @nsx_ip_addr + '/api/2.0/services/securitygroup/bulk/' + parent_obj_id

			myheaders = {'Content-Type' => 'application/xml'}
			auth = {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
			res_obj = HTTParty.put(
				url,
				:headers => myheaders,
				:basic_auth => auth,
				:body => payload
				)
		end
	end

	def self.create_sec_gp_and_get_object_id(sg_name, current_user)

		existing_sec = SecGroup.find_by(name: sg_name)
		if existing_sec.nil?

			myheaders = {'Content-Type' => 'application/xml'}
			auth = {:username => "admin", :password => "Deloitte.1!"}
			url = 'https://' + @nsx_ip_addr + '/api/2.0/services/securitytags/tag'

			body = '
			<securityTag>
			<objectTypeName>SecurityTag</objectTypeName>
			<type>
			<typeName>SecurityTag</typeName>
			</type>
			<name>' + sg_name + '</name>
			<isUniversal>false</isUniversal>
			<description>This tag is created by API</description>
			<extendedAttributes>
			</extendedAttributes>
			</securityTag>'

			response_from_sec_tag_create = HTTParty.post(
				url,
				:headers => myheaders,
				:basic_auth => auth,
				:body => body
				)
			Vm.create(vm_moid: response_from_sec_tag_create.parsed_response, vm_name: sg_name, vm_type: "SecurityTag")


			sg_body = '
			<securitygroup>    
			<objectId></objectId>
			<objectTypeName>SecurityGroup</objectTypeName>
			<type>
			<typeName>SecurityGroup</typeName>
			</type>    
			<description></description>
			<name>' + sg_name + '</name>
			<revision>0</revision>
			<dynamicMemberDefinition>
			<dynamicSet>
			<dynamicCriteria>
			<operator>OR</operator>
			<key>VM.SECURITY_TAG</key>
			<criteria>contains</criteria>
			<value>' + sg_name + '</value>
			<isValid>true</isValid>
			</dynamicCriteria>
			</dynamicSet>
			</dynamicMemberDefinition>
			</securitygroup>'

			@nsx_ip_addr = Setting.current_nsx
			url = 'https://' + @nsx_ip_addr + '/api/2.0/services/securitygroup/bulk/globalroot-0'
			myheaders = {'Content-Type' => 'application/xml'}
			auth = {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
			response_from_sec_gp_create = HTTParty.post(
				url,
				:headers => myheaders,
				:basic_auth => auth,
				:body => sg_body
				)

			Vm.create(vm_moid: response_from_sec_gp_create.parsed_response, vm_name: sg_name, vm_type: "SecurityGroup")

			# ------------------------------------------
			# SEE IF A PARTICULAR SECURITY GROUP EXISTS

			sg_obj_id = response_from_sec_gp_create.body.to_s
			if sg_obj_id.include? "<error><details>Another object with same name"

				url = 'https://' + @nsx_ip_addr + '/api/2.0/services/securitygroup/scope/globalroot-0'
				myheaders = {'Content-Type' => 'application/xml'}
				auth = {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
				res = HTTParty.get(
					url,
					:headers => myheaders,
					:basic_auth => auth
					)

				parsable_json = Nokogiri::XML(res.body)
				parsable_json.xpath('//securitygroup').each do |x|
					if sg_name == x.xpath('name').text
						sg_obj_id = x.xpath('objectId').text
					end
				end
			end
			parent_obj_id = ""

			if Vm.find_by(vm_name: sg_name).nil?
				Vm.find_all_sys_objs(current_user)
			end

			if !parent_obj_id.include?("exists") && !parent_obj_id.include?("same")
				object = Vm.find_by(vm_name: sg_name)
				sg_obj_id = object.vm_moid
			end
		else
			sg_obj_id = existing_sec.object_id
		end

		return sg_obj_id
	end

	def self.import_group(file, current_user)
		num = 0
		parent_new = nil
		child_new = nil
		array_p = []

		parent_ref_obj = nil
		child_ref_obj = nil

		@nsx_ip_addr = Setting.current_nsx

		parent_obj_id = ""
		parent_name = ""
		child_name = ""
		child_obj_id = ""
		CSV.foreach(file, headers: true) do |row|
			object = nil
			type = ""
			if row[0].to_s.length > 0
				# create a parent sec group and grab object_id, through the NSX MGR API
				parent_obj_id = create_sec_gp_and_get_object_id(row[0], current_user)
				parent_name = row[0]
				type = "SecurityGroup"

				# find entity_id through the VRNI API
				entity_id = get_entity_id_via_VRNI(type, parent_name)
				# create a parent sec group in the local database

				parent_new = SecGroup.find_by(name: parent_name)
				if parent_new.nil?
					parent_new = SecGroup.new( # it is important to have this second to the cretion in Service Composer (NSX)
						name: parent_name,
						object_id: parent_obj_id,
						object_entity_id: entity_id,
						newly_created: true
						)

					array_p << parent_new
				end
			end

			if row[1].to_s.length > 0

				entity_id = get_entity_id_via_VRNI(row[2], row[1])
				virtual_machine_moid = Vm.find_by(vm_name: row[1])
				if (virtual_machine_moid)

					object = parent_new.app_objs.find_by(obj_name: row[1])
					if object.nil?
						object = AppObj.create!(obj_name: row[1], obj_type: row[2], obj_entity_id: entity_id, obj_id: virtual_machine_moid)
						parent_new.app_objs << object
						parent_new.save
					end



					sec_tag = Vm.find_by(vm_name: parent_new.name, vm_type: "SecurityTag")
					SecTag.create(tag_name: parent_new.name)

					url = 'https://' + @nsx_ip_addr + '/api/2.0/services/securitytags/tag/' + sec_tag.vm_moid + '/vm/' + virtual_machine_moid.vm_moid

					myheaders = {'Content-Type' => 'application/xml'}
					auth = {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
					
					# ------------------------------------------------------------------------------------------------------------------------------------------------------------
					attatch_tag_response = HTTParty.put(
						url,
						:headers => myheaders,
						:basic_auth => auth
						)
				end
			end
		end
	end

	def self.add_tag_to_VM(file, current_user)
		parent_ref_obj = nil
		child_ref_obj = nil
		nsx_ip_addr = Setting.current_nsx
		parent_obj_id = ""
		parent_name = ""
		child_name = ""
		child_obj_id = ""
		incomming_tag_name = ""

		CSV.foreach(file, headers: true) do |row|
			object = nil
			type = ""
			if row[0].to_s.length > 0
				child_name = row[0]
				virtual_machine = Vm.find_by(vm_name: row[0])
			end
			n = 1
			while n < 4 # there are a max of 3 tags per vm
				if row[n].to_s.length > 0
					incomming_tag_name = row[n]
					sec_tag = Vm.where("LOWER(vm_name) = '#{incomming_tag_name.downcase}' AND vm_type = 'SecurityTag'");
					sec_group = Vm.where("LOWER(vm_name) = '#{incomming_tag_name.downcase}' AND vm_type = 'SecurityGroup'");
						# .find_by(vm_name: incomming_tag_name, vm_type: "SecurityTag")
					sec_tag = sec_tag.first
					if sec_tag.nil?
						if !sec_group.nil? # we have a group without a tag: create a tag
							security_tag = create_a_sec_tag_in_nsx(sec_group.name, nsx_ip_addr)
							create_a_sec_tag_locally(incomming_tag_name, virtual_machine, sec_group)
						end
					else
						entity_id = get_entity_id_via_VRNI("VirtualMachine", virtual_machine.vm_name)
						create_a_sec_tag_locally(incomming_tag_name, virtual_machine, sec_group)
						attach_virtual_machine_to_tag_on_nsx(sec_tag.vm_moid, virtual_machine.vm_moid, nsx_ip_addr, current_user)
					end
				end
				n += 1
			end
		end
	end

	def self.create_a_sec_tag_locally(incomming_tag_name, virtual_machine, sec_group)
		sec_tag_obj = SecTag.create(tag_name: incomming_tag_name)
		# app_obj = AppObj.create(obj_type: "VirtualMachine", obj_name: incomming_tag_name, obj_entity_id: entity_id)

		app_objs = AppObj.find_by(obj_name: virtual_machine.vm_name)
		if app_objs.kind_of?(Array)
			app_objs.each do |object|
				object.sec_tags << sec_tag_obj
			end
		else
			app_objs.sec_tags << sec_tag_obj
		end
		# sec_group.app_objs << app_obj
	end

	def self.attach_virtual_machine_to_tag_on_nsx(sec_tag_moid, virtual_machine_moid, nsx_ip_addr)
		url = 'https://' + nsx_ip_addr + '/api/2.0/services/securitytags/tag/' + sec_tag_moid + '/vm/' + virtual_machine_moid

		myheaders = {'Content-Type' => 'application/xml'}
		auth = {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}

		attatch_tag_response = HTTParty.put(
			url,
			:headers => myheaders,
			:basic_auth => auth
		)
	end

	def self.create_a_sec_tag_in_nsx(sec_group_name, nsx_ip_addr)
		myheaders = {'Content-Type' => 'application/xml'}
		auth = {:username => "admin", :password => "Deloitte.1!"}
		url = 'https://' + nsx_ip_addr + '/api/2.0/services/securitytags/tag'

		body = '
		<securityTag>
		<objectTypeName>SecurityTag</objectTypeName>
		<type>
		<typeName>SecurityTag</typeName>
		</type>
		<name>' + sec_group_name + '</name>
		<isUniversal>false</isUniversal>
		<description>This tag is created by API</description>
		<extendedAttributes>
		</extendedAttributes>
		</securityTag>'

		response_from_sec_tag_create = HTTParty.post(
			url,
			:headers => myheaders,
			:basic_auth => auth,
			:body => body
			)

		return response_from_sec_tag_create.parsed_response
	end


	def self.translate_type_from_NSX_to_VRNI(object_nsx_type)
		case(object_nsx_type)
		when "Cluster"
			puts("no need")
		when "LogicalSwitch"
			puts("there is no apparent VRNI support for a " + object_nsx_type)
		when "Network"
			puts("there is no apparent VRNI support for a " + object_nsx_type)
		when "VirtualApp"
			puts("there is no apparent VRNI support for a " + object_nsx_type)
		when "Datacenter"
			object_nsx_type = "VCDatacenter"
		when "IPSet"
			object_nsx_type = "NSXIPSet"
		when "VirtualMachine"
			puts("no need")
		when "MACSet"
			puts("there is no apparent VRNI support for a " + object_nsx_type)
		when "SecurityTag"
			puts("no need")
		when "Vnic"
			puts("no need")
		when "ResourcePool"
			puts("no need")
		when "DistributedVirtualPortGroup"
			puts("no need")
		when "SecurityGroup"
			object_nsx_type = "NSXSecurityGroup"
		end
		return object_nsx_type
	end

	def self.get_entity_id_via_VRNI(type, obj_name)
		entity_id = ""
		vrni_type = translate_type_from_NSX_to_VRNI(type)
		@current_vrni = Setting.current_vrni
		url = "https://" + @current_vrni + "/api/ni/search"
		sec_gp_payload = '''{
		"entity_type": "' + vrni_type + '",
		"filter": "name = ''' + obj_name + '''"
		} '''

		response = HTTParty.post(url, body: sec_gp_payload, headers: @auth_header)
		parsable_results = JSON.parse(response.body)
		if parsable_results['results'] # example: {"results":[{"entity_id":"x","entity_type":"NSXSecurityGroup","time":y}],"total_count":1,"start_time":z,"end_time":w}
			parsable_results['results'].each do |a| 
				entity_id = a['entity_id']
			end
		end

		return entity_id
	end

	def self.search_groups(search)
		return SecGroup.where("name like '%#{search}%' OR object_id like '%#{search}%' OR object_entity_id like '%#{search}%'");
	end
end
