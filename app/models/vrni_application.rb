class VrniApplication < ApplicationRecord
	belongs_to :setting

	def self.find_entity_id(entity_type, entity_value)

		api_method = ""
		find_entity_id_payload = ""

		if entity_type == "Ipv4Address" || entity_type == "Ipv6Address"
			api_method = "ip_addresses.ip_address"
		else
			api_method = "vendor_id" # moid
		end

		if entity_type == "SecurityGroup"  # translate NSX names to VRNI, why does VMware do this? who knows
			entity_type = "NSXSecurityGroup"
		elsif entity_type == "ClusterComputeResource"
			entity_type = "Cluster"
		elsif entity_type == "Datacenter"
			entity_type = "VCDatacenter"
		elsif entity_type == "VirtualWire"
			entity_type = "DistributedVirtualPortgroup"
		end 

		if entity_type == "VirtualApp"
		# unsure what to do here
		else
			find_entity_id_payload = '{"entity_type": "' + entity_type + '","filter": "' + api_method + ' = ' + entity_value + '"}'
		end

		entity_id = ""
		@current_vrni = Setting.current_vrni
		if(find_entity_id_payload)
			url = "https://" + @current_vrni + "/api/ni/search"
			find_entity_id_response = HTTParty.post(url, body: find_entity_id_payload, headers: @fr_auth_header)
			entity_info = JSON.parse(find_entity_id_response.body)

			if(entity_info['results'])
			  entity_info = entity_info['results'][0]
			  if(entity_info)
			    entity_id = entity_info['entity_id']
			  end
			end
		end

		return entity_id
	end
end
