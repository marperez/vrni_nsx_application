class Tier < ApplicationRecord
	belongs_to :app, optional: true
	has_many :app_objs, dependent: :destroy
	has_many :sug_rules, dependent: :destroy
end
