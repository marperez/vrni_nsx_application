class User < ApplicationRecord
	has_secure_password

	validates :name, presence: true
	validates :email, uniqueness: true, presence: true, format: /\w+@\w+\.{1}[a-zA-Z]{2,}/
	validates :password, length: { minimum: 8 }, allow_nil: true

	def condition_testing?
		!(:password_digest).nil?
	end

	has_many :rule_histories

	has_many :user_sec_groups
	has_many :sec_groups
	has_many :sec_groups, through: :user_sec_groups

	has_many :user_apps
	has_many :apps #, through: :user_apps

	has_one :setting

	def self.current_user
		return RequestStore.store[:user]
	end

	def self.current(user)
		RequestStore.store[:user] = user # Replaced Thread.current[:user]
	end

	@mispelt_groups_array = Set.new

	def self.import_users(csv)
		user = nil
		user_is_new = true
		CSV.foreach(csv, headers: true) do |row|
			object = nil
			type = ""

			if row[0] != "Name"
				if User.find_by(name: row[0])
					# do nothing
				else
					user = User.new
					user.name = row[0]
					user.email = row[1]
					user.password = "VMware.1!"
					user.password_confirmation = "VMware.1!"
					user.save
				end
			end
		end
		return @mispelt_groups_array
	end
end
