class Service < ApplicationRecord
	belongs_to :fire_rule, optional: true
	belongs_to :sug_rule, optional: true
	belongs_to :temp_fire_rules, optional: true
	belongs_to :rule_histories, optional: true
end
