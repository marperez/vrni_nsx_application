class Vm < ApplicationRecord
	# VirtualMachine, Vnic, SecurityTag, SecurityGroup, DistributedVir, VirtualWire, MACSet, ClusterCompute, Datacenter, VirtualApp, Network, IPSet, Application  

	def self.search_applications(search)
		Vm.where("(vm_name LIKE '%s' OR vm_moid LIKE '%s') AND vm_type not in ('SecurityTag', 'MACSet') AND vm_type = 'Application' " % ["%#{search}%", "%#{search}%"]) 
	end

	def self.search(search)
		Vm.where("(vm_name LIKE '%s' OR vm_moid LIKE '%s') AND vm_type not in ('SecurityTag', 'MACSet') " % ["%#{search}%", "%#{search}%"]) 
	end

	def self.search_security_tags(search)
		Vm.where("(vm_name LIKE '%s' OR vm_moid LIKE '%s') AND vm_type = 'SecurityTag'" % ["%#{search}%", "%#{search}%"]) 
	end

	require 'nokogiri'
	HTTParty::Basement.default_options.update(verify: false)

	def self.find_all_sys_objs(current_user)
		myheaders = {'Content-Type' => 'application/xml'}
		auth = {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
		nsx_ip_addr = Setting.current_nsx

		obj_hash = {}
		ActiveRecord::Base.transaction do
			ActiveRecord::Base.connection.execute("delete from vms")
		end

		insert_statement = 'INSERT INTO vms (vm_moid, vm_name, vm_type, created_at, updated_at) VALUES '
		execute_query_if_contains_elememts = false
		utc_time_today = Time.now.to_i
		
		# get all the system objects from this api
		objects_json = get_system_objects_as_json(auth, myheaders, nsx_ip_addr, utc_time_today)

		# get all the system applications from this api
		applications_json = get_system_applications_as_json(auth, myheaders, nsx_ip_addr, utc_time_today)

		objects_json.xpath('//basicinfo').each do |x|
			insert_statement += "('%s', '%s', '%s', '%s', '%s')," % [x.xpath('objectId').text, x.xpath('name').text, x.xpath('type/typeName').text, utc_time_today, utc_time_today]
			execute_query_if_contains_elememts = true
		end

		applications_json.xpath('//application').each do |x|
			insert_statement += "('%s', '%s', '%s', '%s', '%s')," % [x.xpath('objectId').text, x.xpath('name').text, x.xpath('type/typeName').text, utc_time_today, utc_time_today]
			execute_query_if_contains_elememts = true
		end

		if execute_query_if_contains_elememts # important if this is a fresh environment
			ActiveRecord::Base.connection.execute(insert_statement[0...-1])
		end
	end

	def self.get_system_applications_as_json(auth, myheaders, nsx_ip_addr, utc_time_today)
		url = "https://%s/api/2.0/services/application/scope/globalroot-0" % [nsx_ip_addr]

		response = HTTParty.get(
			url,
			:headers => myheaders,
			:basic_auth => auth
			)

		parsable_json = Nokogiri::XML.parse(response.body)

		return parsable_json
	end

	def self.get_system_objects_as_json(auth, myheaders, nsx_ip_addr, utc_time_today)
		url = 'https://%s/api/2.0/services/securitygroup/scope/globalroot-0/members/' % [nsx_ip_addr]

		response = HTTParty.get(
			url,
			:headers => myheaders,
			:basic_auth => auth
			)

		parsable_json = Nokogiri::XML.parse(response.body)

		return parsable_json
	end


	def self.create_mac_set(current_user, new_mac_address_set_name, encountered_address_value)
		myheaders = {'Content-Type' => 'application/xml'}
		auth = {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
		nsx_ip_addr = Setting.current_nsx
		url = 'https://' + nsx_ip_addr + '/api/2.0/services/macset/scope/globalroot-0'
		# get every object in the system, tags, vms, vncs ...
		body = '<macset>
					<type>
						<typeName>MACSet</typeName>
					</type>
					<description>An encountered flow Mac Address</description>
					<name>%s</name>
					<revision>0</revision>
					<objectTypeName>MACSet</objectTypeName>
					<value>%s</value>
				</macset>' % [new_mac_address_set_name, encountered_address_value]
		body = body.gsub!("\n","").gsub!("\t","")
		response = HTTParty.post(url, :headers => myheaders, :body => body, :basic_auth => auth )
		return response
	end

	def self.find_section_id(auth, headers, group_name, nsx_manager_ip)
        section_id = ""
        nsx_sections_url = "https://%s/api/4.0/firewall/globalroot-0/config" % [nsx_manager_ip]
        all_l3_dfw_sections = HTTParty.get(nsx_sections_url, headers: headers, :basic_auth => auth)
        sections = all_l3_dfw_sections.parsed_response.fetch('firewallConfiguration').fetch('layer3Sections').fetch('section')
        sections.each do |section|
          if section.fetch('name') == group_name
            return section_id = section.fetch('id')
          end
        end
    end

end
