class TempFireRule < ApplicationRecord
	belongs_to :sec_group, optional: true
	belongs_to :app, optional: true
	
	has_many :protocols, dependent: :destroy
	has_many :applied_tos
	has_many :destinations, dependent: :destroy
	has_many :sources, dependent: :destroy
	has_many :port_ranges, dependent: :destroy
	has_many :services, dependent: :destroy
end
