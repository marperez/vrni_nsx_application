class AppObj < ApplicationRecord
	belongs_to :tier, optional: true # used for VRNI Apps and Tiers 

	belongs_to :nested_sec_gps, optional: true # the normal path - sec gp has many nested gps which inturn have many app objects (vms, datastores, hosts ...)
	belongs_to :sec_group, optional: true # a security group can have many objects that are not Nested Security Groups

	has_many :vms

	has_many :sec_tags
	# only use dependent destroy on the has_many, or you will have orphans if a child is deleted. The parents will also be removed but the other children will be spared


	def self.get_app_members(app_id)
	    # find all vms in the security group
	    # find all the nested vms and nested security groups
	    app = App.find_by(id: app_id)
	    puts(app.app_vrni_id + "--- app_id ---")

	    @history_array = []
	    if(app)
		    app.tiers.each do |tier|
			    tier.app_objs.each do |object|

					if object.obj_type == "VirtualMachine"

						obid = object.obj_id
						obty = object.obj_type
						obna = object.obj_name
						vm_bool = true
						get_fw_rules(obid, obty, obna, vm_bool)

					else 
						@nsx_ip_addr = Setting.current_nsx
						url = "https://" + @nsx_ip_addr + "/api/2.0/services/securitygroup/" + object.obj_id
						auth =  {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
						headers = {'Content-Type' => 'application/xml'}
						resp = HTTParty.get(url, :headers => headers, :basic_auth => auth)

						doc = Nokogiri::XML(resp.body)

						outer_array = []
						doc.xpath('//securitygroup').each do |sg|
							if sg.xpath('member').length > 0
								sg.xpath('member').each do |member|
									inner_array = []
									inner_array.append(member.xpath('objectId').text)
									inner_array.append(member.xpath('objectTypeName').text)
									inner_array.append(member.xpath('name').text)
									outer_array << inner_array
								end
							end
						end

						outer_array.each do |vm|
							obid = vm[0]
							obty = vm[1]
							obna = vm[2]
							vm_bool = false
							get_fw_rules(obid, obty, obna, vm_bool)
				        end
					end
				end
		    end
		end
	end

	def self.get_fw_rules(moid, obty, obna, vm_bool)

	    fw_rules = []
	    fw_rules << FireRule.joins(:sources).where("sources.sour_value = " + "'" + moid + "'")
	    .select("fire_rules.id, fire_rules.name, fire_rules.action, fire_rules.direction, fire_rules.pack_type")
	    fw_rules << FireRule.joins(:destinations).where("destinations.dest_value = " + "'" + moid + "'")
	    .select("fire_rules.id, fire_rules.name, fire_rules.action, fire_rules.direction, fire_rules.pack_type")

		if vm_bool 
			fw_rules.each do |rules|
				rule = rules.first
				if ( rule && ( !@history_array.include? rule.name ) )
					@history_array << rule.name
					AppObj.create_existing_rule(rule, obna)
				end
			end
		end
	end

	def self.create_existing_rule(rule, obna)
		app_related_rule = TempFireRule.create!(
          name: rule.name,
          action: rule.action,
          direction: rule.direction,
          pack_type: rule.pack_type,
          rule_vm_id: obna
        )

		rule.applied_tos.each do |ap|
		  # applied = AppliedTo.find_by(apl_value: ap.apl_value.text) # this checks if it exists
		  # if(applied)
		      applied = AppliedTo.create!(
		        apl_name: ap.apl_name,
		        apl_value: ap.apl_value,
		        apl_type: ap.apl_type
		      )
		  # end
		  app_related_rule.applied_tos << applied
		end

		rule.sources.each do |so|
			source = rule.sources.find_by(sour_value: so.sour_value)
			if(source.nil?)
				source = Source.create!(
					sour_exclude: so.sour_exclude,
					sour_value: so.sour_value,
					sour_type: so.sour_type,
					sour_is_valid: so.sour_is_valid
				)
			end
			source.save
			app_related_rule.sources << source
		end

		rule.destinations.each do |de|
			destination = rule.destinations.find_by(dest_value: de.dest_value)
			if(destination.nil?)
				destination = Destination.create!(
					dest_exclude: de.dest_exclude,
					dest_value: de.dest_value,
					dest_type: de.dest_type,
					dest_is_valid: de.dest_is_valid
				)
			end
			app_related_rule.destinations << destination
		end

		rule.services.each do |se|
			service = rule.services.find_by(serv_name: se.serv_name)
			if(service.nil?)
				service = Service.create!(
					serv_name: se.serv_name,
					serv_value: se.serv_value,
					serv_type: se.serv_type,
					serv_is_valid: se.serv_is_valid
				)
			end
			app_related_rule.services << service
		end

		# now we save the rule
		app_related_rule.save
		# ""
	end


end
