class FireRule < ApplicationRecord
	has_many :protocols, dependent: :destroy
	has_many :applied_tos, dependent: :destroy
	has_many :destinations, dependent: :destroy
	has_many :sources, dependent: :destroy
	has_many :port_ranges, dependent: :destroy
	has_many :services, dependent: :destroy

	belongs_to :rule_template, optional: true

	@history_array = []
	@nsx_ip_addr = ""


	def self.get_rules_outside_of_sec_groups
	    return FireRule.joins(:applied_tos, :sources, :destinations).where("applied_tos.apl_value = 'DISTRIBUTED_FIREWALL'")
	end

	def self.get_nsx_rules(rule_id, current_user, type)

		nsx_ip_addr = Setting.current_nsx
		url = "https://" + nsx_ip_addr + "/api/4.0/firewall/globalroot-0/config"
		auth =  {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
		headers = {'Content-Type' => 'application/xml'}
		resp = HTTParty.get(url, :headers => headers, :basic_auth => auth)

		etag_value = ""
		section_id = ""

		history_item = RuleHistory.find_by_id(rule_id)
		if history_item.rule_name == ""
			history_item.rule_name = "rule_" + history_item.id
		end

		doc = Nokogiri::XML(resp.body)

		case(type)
		when "undo",
			RuleHistory.remove_nsx_rule(current_user, doc, history_item, nsx_ip_addr)
		when "refresh"
			RuleHistory.refresh_user_history(current_user, doc)
		end

	end

	def self.get_associated_fw_rules(vm_id)
		fw_rules = []
		fw_rules << FireRule.joins(:sources).where("sources.dest_value = vm_id OR sources.dest_value = any" )
		fw_rules << FireRule.joins(:destinations).where("destinations.dest_value = vm_id OR destinations.dest_value = any" )
		return fw_rules
	end

	def self.get_fire_rules_for_sec_group(object_array, user_group, current_user)

		@history_array = []
		object_array.first.app_objs.each do |object|
			if object.obj_type == "VirtualMachine"

				obid = object.obj_id
				obty = object.obj_type
				obna = object.obj_name
				vm_bool = true
				get_and_add_rules_to_temp_tb(obid, obty, obna, vm_bool, user_group)

			else 
				nsx_ip_addr = Setting.current_nsx
				url = "https://" + nsx_ip_addr + "/api/2.0/services/securitygroup/" + object.obj_id
				auth =  {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
				headers = {'Content-Type' => 'application/xml'}
				resp = HTTParty.get(url, :headers => headers, :basic_auth => auth)

				doc = Nokogiri::XML(resp.body)

				outer_array = []
				doc.xpath('//securitygroup').each do |sg|
					if sg.xpath('member').length > 0
						sg.xpath('member').each do |member|
							inner_array = []
							inner_array.append(member.xpath('objectId').text)
							inner_array.append(member.xpath('objectTypeName').text)
							inner_array.append(member.xpath('name').text)
							outer_array << inner_array
						end
					end
				end

				outer_array.each do |vm|
					obid = vm[0]
					obty = vm[1]
					obna = vm[2]
					vm_bool = false
					get_and_add_rules_to_temp_tb(obid, obty, obna, vm_bool, user_group)
				end
			end
		end
		return TempFireRule.includes(:applied_tos, :sources, :destinations, :services, :port_ranges).where(:sec_group_id => user_group.id)
	end

	def self.get_and_add_rules_to_temp_tb(obid, obty, obna, vm_bool, user_group)

		fw_rules = []
		fw_rules << FireRule.joins(:sources).where("sources.sour_value = " + "'" + obid + "'")
		.select("fire_rules.id, fire_rules.name, fire_rules.action, fire_rules.direction, fire_rules.pack_type")
		fw_rules << FireRule.joins(:destinations).where("destinations.dest_value = " + "'" + obid + "'")
		.select("fire_rules.id, fire_rules.name, fire_rules.action, fire_rules.direction, fire_rules.pack_type")

		fw_rules.each do |rules| # a nested array is returned
			rules.each do |rule|
				if (!@history_array.include? rule.name)
					@history_array << rule.name
					newrule = TempFireRule.create!(
						name: rule.name,
						action: rule.action,
						direction: rule.direction,
						pack_type: rule.pack_type,
						rule_vm_id: obna
						)

					rule.applied_tos.each do |ap|
						applied = AppliedTo.create!(
							apl_name: ap.apl_name,
							apl_value: ap.apl_value,
							apl_type: ap.apl_type
							)
						newrule.applied_tos << applied
					end

					rule.sources.each do |so|
						source = Source.create!(
							sour_exclude: so.sour_exclude,
							sour_value: so.sour_value,
							sour_name: so.sour_name,
							sour_type: so.sour_type,
							sour_is_valid: so.sour_is_valid
							)
						source.save
						newrule.sources << source
					end

					rule.destinations.each do |de|
						destination = Destination.create!(
							dest_exclude: de.dest_exclude,
							dest_value: de.dest_value,
							dest_name: de.dest_name,
							dest_type: de.dest_type,
							dest_is_valid: de.dest_is_valid
							)
						newrule.destinations << destination
					end

					rule.services.each do |se|
						service = Service.create!(
							serv_name: se.serv_name,
							serv_value: se.serv_value,
							serv_type: se.serv_type,
							serv_is_valid: se.serv_is_valid
							)
						newrule.services << service
					end

					rule.port_ranges.each do |se|
						port_range = PortRange.create!(startport: se.startport,	endport: se.endport)
						newrule.port_ranges << port_range
					end

					rule.port_ranges.each do |se|
						port_range = PortRange.create!(name: se.name)
						newrule.port_ranges << port_range
					end

					user_group.temp_fire_rules << newrule
					user_group.save
				end
			end
		end
	end

	def self.insert_all_fire_rules(current_user, section_name)

		FireRule.includes(:protocols,:applied_tos,:destinations,:sources,:port_ranges,:services).where('rule_template_id is null').destroy_all
		
		@nsx_ip_addr = Setting.current_nsx
		nsx_mgr_auth(current_user)

		HTTParty::Basement.default_options.update(verify: false)

		auth =  {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}

		url = "https://" + @nsx_ip_addr + "/api/4.0/firewall/globalroot-0/config" # gets all firewall rules

		resp = HTTParty.get(url, :basic_auth => auth)

		
		doc = Nokogiri::XML(resp.body)
		fire_rule_array = []
		doc.xpath('//layer3Sections/section').each do |sg|

			if sg.attribute('name').text == section_name
				sg.xpath('rule').each do |r|
					fire_rule_array << FireRule.create_firewall_rule_locally(r)
				end
				return fire_rule_array
			elsif section_name == ""
				sg.xpath('rule').each do |r|
					FireRule.create_firewall_rule_locally(r)
				end
			else
				# do nothing, the section name exists but we are at the wrong index	
			end

		end
		return fire_rule_array
	end

	def self.create_firewall_rule_locally(r)
		# fire_rule = FireRule.find_by(name: r.xpath('name').text)
		# if (fire_rule.nil?)
		fire_rule = FireRule.create( 
			name: r.xpath('name').text,
			action: r.xpath('action').text,
			direction: r.xpath('direction').text,
			pack_type: r.xpath('pack_type').text)
		# else
		# 	fire_rule = FireRule.update(
		# 		name: r.xpath('name').text,
		# 		action: r.xpath('action').text,
		# 		direction: r.xpath('direction').text,
		# 		pack_type: r.xpath('pack_type').text)
		# end

		r.xpath('appliedToList').each do |ap|
			AppliedTo.transaction do
				# applied = fire_rule.applied_tos.where(apl_name: ap.xpath('appliedTo/name').text, apl_value: ap.xpath('appliedTo/value').text, apl_type: ap.xpath('appliedTo/type').text)
				# if (!applied)
				applied = AppliedTo.create(
					apl_name: ap.xpath('appliedTo/name').text,
					apl_value: ap.xpath('appliedTo/value').text,
					apl_type: ap.xpath('appliedTo/type').text
					)
				fire_rule.applied_tos << applied
				# else
				# 	applied = AppliedTo.update(
				# 		apl_name: ap.xpath('appliedTo/name').text
				# end
			end
		end

		if fire_rule.protocols.count != 0
			destination = Destination.create!(
				dest_exclude: "",
				dest_value: "any",
				dest_type: "",
				dest_name: "",
				dest_is_valid: ""
				)
			fire_rule.destinations << destination 
		end

		Source.transaction do
			r.xpath('sources/source').each do |so|
				test_array = []
				# source_instance = fire_rule.sources.find_by(sour_value: so.xpath('value').text)
				# if(!source_instance)
				excluded = ""
				r.xpath('sources').each do |source|
					excluded = source.attr("excluded")
				end
					# source = Source.find_by(sour_value: so.xpath('value').text)
					entity_name = so.xpath('name').text # test123
					entity_value = so.xpath('value').text # security-group-37
					entity_type = so.xpath('type').text # SecurityGroup

					api_method = " "
					if entity_type == "Ipv4Address" || entity_type == "Ipv6Address"
						api_method = "ip_addresses.ip_address"
					else
						api_method = "vendor_id" # moid
					end

					if entity_type == "SecurityGroup"
						entity_type = "NSXSecurityGroup"
					elsif entity_type == "ClusterComputeResource"
						entity_type = "Cluster"
					elsif entity_type == "Datacenter"
						entity_type = "VCDatacenter"
					elsif entity_type == "VirtualWire"
						entity_type = "DistributedVirtualPortgroup"
					end	

					if entity_type == "VirtualApp"
						# unsure what to do here
					else
						find_entity_id_payload = '{"entity_type": "' + entity_type + '","filter": "' + api_method + ' = ' + entity_value + '"}'
					end

					entity_id = ""
					if(find_entity_id_payload)
						current_vrni = Setting.current_vrni
						url = "https://" + current_vrni + "/api/ni/search"
						find_entity_id_response = HTTParty.post(url, body: find_entity_id_payload, headers: @fr_auth_header)
						test_array.push(find_entity_id_response.body)
						entity_info = JSON.parse(find_entity_id_response.body)
						if(entity_info['results'])
							entity_info = entity_info['results'][0]
							if(entity_info)
								entity_id = entity_info['entity_id']
							end
						end
					end
					sour_is_valid = so.xpath('isValid').text
					if sour_is_valid.nil?
						sour_is_valid = "true"
					end

					if (entity_id.to_s != "")
						source_instance = Source.create!(
							entity_id: entity_info['entity_id'],
							sour_exclude: excluded,
						sour_value: entity_value, #  The object ID (moid), however this would have been inputed manually at some point in the past
						sour_type: entity_type,
						sour_name: so.xpath('name').text,
						sour_is_valid: sour_is_valid)
					else
						source_instance = Source.create!(
							sour_exclude: excluded,
						sour_value: entity_value, #  The object ID (moid), however this would have been inputed manually at some point in the past
						sour_type: entity_type,
						sour_name: so.xpath('name').text,
						sour_is_valid: sour_is_valid)
					end

					fire_rule.sources << source_instance 
					fire_rule.save
				# else
				# 	source_instance = Source.update(
				# 	sour_name: so.xpath('name').text)
				# end
			end
		end

		if fire_rule.sources.count == 0 # provide an empty source with a value of 'any', which will tell the final method to ignore this section
			source = Source.create!(
				sour_exclude: "",
				sour_value: "any",
				sour_type: "",
				sour_name: "",
				sour_is_valid: ""
				)
			source.save
			fire_rule.sources << source 
		end

		Destination.transaction do
			r.xpath('destinations/destination').each do |de|
			# destination_instance = fire_rule.destinations.find_by(dest_value: de.xpath('value').text)
			# if(!destination_instance)
				# destination = Destination.find_by(dest_value: de.xpath('value').text)
				# we need to find the entity id which is not given above
				entity_name = de.xpath('name').text # test123
				entity_value = de.xpath('value').text # security-group-37
				entity_type = de.xpath('type').text # SecurityGroup

				api_method = " "
				if entity_type == "Ipv4Address" || entity_type == "Ipv6Address"
					api_method = "ip_addresses.ip_address"
				else
					api_method = "vendor_id" # moid
				end

				if entity_type == "SecurityGroup"
					entity_type = "NSXSecurityGroup"
				elsif entity_type == "ClusterComputeResource"
					entity_type = "Cluster"
				elsif entity_type == "Datacenter"
					entity_type = "VCDatacenter"
				elsif entity_type == "VirtualWire"
					entity_type = "DistributedVirtualPortgroup"
				end	

				if entity_type == "VirtualApp"
					# entity_type = "Group"
					# entity_value = "VirtualApp-" + entity_value
					# find_entity_id_payload = '{"entity_type": "VirtualMachine","filter": " name = ' + entity_value + '"}'
				else
					find_entity_id_payload = '{"entity_type": "' + entity_type + '","filter": "' + api_method + ' = ' + entity_value + '"}'
				end

				excluded = r.xpath('destinations').attr("excluded")
				validated = de.xpath('isValid').text
				if validated.nil?
					validated = "true"
				end

				entity_id = ""
				if(find_entity_id_payload)

					current_vrni = Setting.current_vrni
					url = "https://" + current_vrni + "/api/ni/search"
					find_entity_id_response = HTTParty.post(url, body: find_entity_id_payload, headers: @fr_auth_header)
					entity_info = JSON.parse(find_entity_id_response.body)
					if(entity_info['results'])
						entity_info = entity_info['results'][0]
						if(entity_info)
							entity_id = entity_info['entity_id']
						end
					end
				end

				if(entity_id.to_s != "")
					destination_instance = Destination.create!(
						entity_id: entity_id,
						dest_exclude: excluded,
						dest_value: entity_value, # moid, however this would have been inputed manually at some point
						dest_type: entity_type,
						dest_name: de.xpath('name').text,
						dest_is_valid: validated
						)
				else
					destination_instance = Destination.create!(
						dest_exclude: excluded,
						dest_value: entity_value, # moid, however this would have been inputed manually at some point
						dest_type: entity_type,
						dest_name: de.xpath('name').text,
						dest_is_valid: validated
						)
				end

				fire_rule.destinations << destination_instance
				fire_rule.save
			# else
			# 	destination_instance = Destination.update(dest_name: de.xpath('name').text)
			# end
			end
		end

		if fire_rule.destinations.count == 0
			destination = Destination.create!(
				dest_exclude: "",
				dest_value: "any",
				dest_type: "",
				dest_name: "",
				dest_is_valid: ""
				)
			fire_rule.destinations << destination 
		end



		r.xpath('services/service').each do |se|
			# The services section in the response will change according to the type
			if se.xpath('sourcePort').text != ""
				# range = fire_rule.port_ranges.find_by(startport: se.xpath('sourcePort').text, endport: se.xpath('destinationPort').text)
				# if (!range)
				range = PortRange.create!(
					startport: se.xpath('sourcePort').text,
					endport: se.xpath('destinationPort').text
					)
				fire_rule.port_ranges << range
				fire_rule.save
				# else
				# 	# leave as is
				# end

				# protocol = fire_rule.protocols.find_by(name: se.xpath('protocolName').text)
				# if (!protocol)
				protocol = Protocol.create!(
					name: se.xpath('protocolName').text
					)
				fire_rule.protocols << protocol
				fire_rule.save
				# else
				# 	# leave as is
				# end
			end

			if se.xpath('name').text != ""
				# service = fire_rule.services.find_by(serv_name: se.xpath('name').text)
				# if(!service)
				service_text_name = se.xpath('name').text
				if(service_text_name == "")
					service_text_name = "any"
				end
				service = Service.create(
					serv_is_valid: "true",
					serv_name: service_text_name,
					serv_value: se.xpath('value').text,
					serv_type: se.xpath('type').text
					)
				fire_rule.services << service
				fire_rule.save
				# end
			end
		end

		if fire_rule.services.count == 0
			service = Service.create!(
				serv_name: "",
				serv_value: "any",
				serv_type: "",
				serv_is_valid: ""
				)
			fire_rule.services << service
		end

		# now we clean up and save the rule
		fire_rule.save
		return fire_rule
	end

	def self.find_vim_id
		myheaders = {'Content-Type' => 'application/xml'}
		auth =  {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
		@nsx_ip_addr = Setting.current_nsx
		url = 'https://' + @nsx_ip_addr + '/api/2.0/services/securitygroup/scope/globalroot-0/members/'
		# get every object in the system, tags, vms, vncs ...
		response = HTTParty.get(
			url,
			:headers => myheaders,
			:basic_auth => auth
			)
		obj_hash = {}
		parsable_json = Nokogiri::XML.parse(response.body)
		Vm.delete_all
		parsable_json.xpath('//basicinfo').each do |x|
			puts(x.xpath('objectId').text)
			vm = Vm.first_or_initialize(vm_moid: x.xpath('objectId').text, vm_name: x.xpath('name').text)
			vm.save # first_or_initialize calls 'New, not Create'
		end
	end

	@all_known_mac_addresses_hash = Hash.new
	def self.parcel_selected_fw_rules_for_nsx(current_group_id, current_user)
		current_group = SecGroup.find_by_id(current_group_id)

		if !current_group.nil?
			@all_known_mac_addresses_hash.clear

			nsx_manager_ip = Setting.current_nsx

			auth = {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}   
			myheaders = {'Content-Type' => 'application/xml'}
			nsx_url = "https://" + nsx_manager_ip + "/api/2.0/services/macset/scope/globalroot-0"
			nsx_response = HTTParty.get(nsx_url,  headers: myheaders, :basic_auth => auth)

			all_known_mac_addresses = Nokogiri::XML(nsx_response.body).xpath("//list/macset")

			all_known_mac_addresses.each do |mac_address|
				@all_known_mac_addresses_hash[mac_address.xpath('name').text] = mac_address.xpath('objectId').text
			end

			rules_to_send = []
			rules_to_send << current_group.sug_rules.where(submit_sug: 't') # only accept rules that were activated (in green)
			
			current_group.rule_templates.uniq.each do |template|
				rules_to_send << template.fire_rules
			end

			select_statement = 'SELECT * FROM flows WHERE flows.sec_group_id = %s AND flows.allow_deny is not null' % [current_group.id]
			rules_to_send << ActiveRecord::Base.connection.execute(select_statement)

			return FireRule.send_selected_rules_to_nsx(current_user, rules_to_send, "sec_group_section", current_group_id, nsx_manager_ip)
		end
	end

	def self.send_selected_rules_to_nsx(current_user, source_array, rule_or_section, group_id, nsx_manager_ip) # the group_id parameter is only necessary for the first push to nsx

		if rule_or_section == "sec_group_section"
			current_group = SecGroup.find_by_id(group_id)
		end
		response = ""
		new_section = false
		history_item_array = []
		array_of_rules_to_send = []
		rules_to_include = Set.new

		if source_array.size > 0

			if current_group.section_id == "" || current_group.section_id.nil?
				new_section = true
			end

			rule_payloads = ""

			source_array.each do |rule_array| # the 4 sources of rules each have their own array inside this 'source_array'
				rule_array.each do |rule|

					# if !rule.is_a?(Hash) and defined? rule.first
					# 	rule = rule.first
					# end

					if !rule.nil?
						service_text = ""
						source_text = ""
						destination_text = ""
						applied_to_text = ""

						if rule.is_a?(SugRule)
							case(rule.name)
							when nil
								rule_name = "rule_%s" % [rule.id.to_s]
							else
								rule_name = rule.name
							end
						elsif rule.is_a?(Hash)
							rule_name = "Flow_rule_%s_%s" % [rule['source'], rule['destination']]
						elsif rule.is_a?(RuleHistory)
							rule_name = rule.rule_name # refers to the name seen on the selected NSX Manager
						else
							if rule.name != "" || !rule.name.nil?
								rule_name = rule.name
							else
								rule_name = "rule_%s" % [rule.id.to_s]
								rule.name = rule_name
								rule.save
							end
						end

						if !rules_to_include.include?(rule_name)
							rules_to_include.add(rule_name)

							if rule.is_a?(Hash) && rule_or_section == "sec_group_section" # the rule is from the flows table

								case(rule['allow_deny'])
								when true, "t"
									rule_action = "allow"
								when false, "f"
									rule_action = "deny"
								end

								# find and combine all the componentes for the api call
								applied_to_text = 
								'<appliedToList>
								<appliedTo>
								<name>"%s"</name>
								<value>%s</value>
								<type>SecurityGroup</type>
								<isValid>true</isValid>
								</appliedTo>
								</appliedToList>' % [current_group.name, current_group.object_id]

								output_text = Flow.find_flow_source_dest(current_user, @all_known_mac_addresses_hash, rule['source'], "source")
								source_text = "%s%s%s" % ['<sources excluded="false">', output_text, '</sources>']

								output_text = Flow.find_flow_source_dest(current_user, @all_known_mac_addresses_hash, rule['destination'], "destination")
								destination_text = "%s%s%s" % ['<destinations excluded="false">', output_text, '</destinations>']


								if rule['port'] == "0"
									service_text = 
									'<services>
									<service>
									<protocol>%s</protocol>
									</service>
									</services>' % [rule['protocol']]
								else
									service_text = 
									'<services>
									<service>
									<destinationPort>%s</destinationPort>
									<protocol>%s</protocol>
									</service>
									</services>' % [rule['port'], rule['protocol']]
								end

								this_payload = '
								<rule disabled="false" logged="true">
								<name>%s</name>
								<action>%s</action>
								%s%s%s%s
								<direction>inout</direction>
								<packetType>any</packetType>  
								</rule>' % [rule_name, rule_action, applied_to_text, source_text, destination_text, service_text]

								rule_payloads += this_payload

								array_of_rules_to_send

							else

								case(rule.action)
								when rule.action.nil?
									rule.action = "allow"
								when "block"
									rule.action = "deny"
								else 
									# leave be
								end	

								if rule_or_section == "sec_group_section"

									if rule_name.nil? || rule_name == ""
										rule_name = "GUI_rule_" + rule.id.to_s

										history_item = RuleHistory.create!(
											rule_name: rule_name = "rule_" + rule.id.to_s,
											username: User.current_user.name, 
											user_id: User.current_user.id)
									else
										history_item = RuleHistory.create!(
											rule_name: rule.name,
											username: User.current_user.name, 
											user_id: User.current_user.id)
									end

									history_item_array << history_item
									#  Give the new history item the same nested relationships as the rule we are pushing to NSX
									applied = AppliedTo.create(apl_name: current_group.name, apl_value: current_group.object_id, apl_type: "SecurityGroup")
									history_item.applied_tos << applied
									history_item.save

									applied_to_text = 
									"<appliedToList>
									<appliedTo>
									<name>'" + current_group.name + "'</name>
									<value>" + current_group.object_id + "</value>
									<type>SecurityGroup</type>
									<isValid>true</isValid>
									</appliedTo>
									</appliedToList>"

									history_item.action = rule.action
									history_item.save

									rule.protocols.each do |x|
										history_item.protocols << x
									end
									rule.port_ranges.each do |x|
										history_item.port_ranges << x
									end
									rule.sources.each do |x|
										history_item.sources << x
									end
									rule.destinations.each do |x|
										history_item.destinations << x
									end
									rule.services.each do |x|
										history_item.services << x
									end

									history_item.save # this clone will take the place 
								else # this is for the methods of redo and undo a historical rule

									applied_to_text = 
									"<appliedToList>
									<appliedTo>
									<name>'" + rule.applied_tos.first.apl_name + "'</name>
									<value>" + rule.applied_tos.first.apl_value + "</value>
									<type>SecurityGroup</type>
									<isValid>true</isValid>
									</appliedTo>
									</appliedToList>"
								end

								rule.protocols.each do |protocol|
									rule.port_ranges.each do |port|
										service_temp_txt = '''<service>'''
										service_temp_txt +=  "<destinationPort>" + port.endport + "</destinationPort>"
										service_temp_txt +=  "<sourcePort>" + port.startport + "</sourcePort>"
										service_temp_txt += "<protocolNum>'" + find_prot_num(protocol.name) + "'</protocolNum>"
										service_temp_txt += "<protocol>'" + protocol.name + "'</protocol></service>"
										service_text += service_temp_txt
									end
								end

								rule.services.each do |service|
									if service.serv_value != "any"
										service_temp_txt = "<service>"
										service_temp_txt +=  "<value>" + service.serv_value + "</value>"
										service_temp_txt +=  "<type>" + service.serv_type + "</type>"
										service_temp_txt += "</service>"
										service_text += service_temp_txt
									end
								end

								if service_text != ""
									service_text = "<services>" + service_text + "</services>"
								end

							    # -----------------------------------------
							    # Find all Sources
							    # -----------------------------------------
							    source_text += ""

							    rule.sources.each do |sour|
							    	if sour.sour_type.to_s == "Tier"
							    		obj = Vm.find_by(vm_name: sour.sour_name, vm_type: sour.sour_type)
							    		tier = Tier.find_by(name: sour.sour_name)
							    		sour_is_valid = sour.sour_is_valid
							    		if sour_is_valid.nil?
							    			sour_is_valid = "true"
							    		end
							    		if(sour.sour_name.to_s.include? tier.name)
							    			obj_value = tier.sec_gp_moid
							    			source_temp_txt = '
							    			<source>
							    			<name>"' + obj.vm_name + '"</name>
							    			<value>' + obj_value + '</value>
							    			<type>SecurityGroup</type>
							    			<isValid>' + sour_is_valid + '</isValid>
							    			</source>' 
							    			source_text += source_temp_txt
							    		end
							    	elsif sour.sour_value == "any"
						    			source_temp_txt = "" # by adding nothing an any rule will apply to this field
						    		else

						    			sour_is_valid = sour.sour_is_valid
						    			if sour_is_valid.nil?
						    				sour_is_valid = "true"
						    			end

						    			if sour.sour_type == "NSXSecurityGroup" or sour.sour_type == "SecurityTag"
						    				type = "SecurityGroup"
						    			else
						    				type = sour.sour_type
						    			end

						    			obj = Vm.find_by(vm_name: sour.sour_name, vm_type: type)

						    			source_temp_txt = '''
						    			<source>
						    			<name>"' + obj.vm_name + '"</name>
						    			<value>' + obj.vm_moid + '</value>
						    			<type>' + type + '</type>
						    			<isValid>' + sour_is_valid + '</isValid>
						    			</source>' 
						    			source_text += source_temp_txt
						    		end
						    	end

						    	if source_text != ""
						    		source_text = ('<sources excluded="false">' + source_text + '</sources>')
						    	end

							    # -----------------------------------------
							    # Find all Destinations
							    # -----------------------------------------

							    rule.destinations.each do |dest|

							    	dest_is_valid = dest.dest_is_valid
							    	if dest_is_valid.nil?
							    		dest_is_valid = "true"
							    	end

							    	if dest.dest_type == "NSXSecurityGroup" or dest.dest_type == "SecurityTag"
							    		type = "SecurityGroup"
							    	else
							    		type = dest.dest_type
							    	end

							    	obj = Vm.find_by(vm_name: dest.dest_name, vm_type: type)

							    	if dest.dest_value != "any"
							    		dest_temp_txt = '
							    		<destination>
							    		<name>"' + obj.vm_name + '"</name>
							    		<value>' + obj.vm_moid + '</value>
							    		<type>' + type + '</type>
							    		<isValid>' + dest_is_valid + '</isValid>
							    		</destination>' 
							    		destination_text += dest_temp_txt
							    	end
							    end

							    if destination_text != ""
							    	destination_text = ('<destinations excluded="false">' + destination_text + '</destinations>')
							    end

							    if rule.action.nil?
							    	rule.action = "allow"
							    	rule.save
							    end

							    this_payload = '
							    <rule disabled="false" logged="true">
							    <name>' + rule_name + '</name>
							    <action>' + rule.action + '</action>' + applied_to_text + source_text + destination_text + service_text + '''
							    <direction>inout</direction>
							    <packetType>any</packetType>  
							    </rule>'
							    rule_payloads += this_payload

							    array_of_rules_to_send << this_payload

							end
						end
					end
				end
			end

			main_payload = ""
			nsx_response = ""
			auth = {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}   

			if rule_or_section == "sec_group_section"
				get_and_post_headers = {'Content-Type' => 'application/xml'}

				nsx_sections_url = "https://" + nsx_manager_ip + "/api/4.0/firewall/globalroot-0/config/layer3sections"
				param_group_name = {:name => current_group.name}
				all_l3_dfw_sections = HTTParty.get(nsx_sections_url, :query => param_group_name, headers: get_and_post_headers, :basic_auth => auth)

	        	if(new_section == true) # Create section
		        	# someone manually deleted or updated this section on NSX via the vSphere GUI
		        	if current_group.section_etag != "" and all_l3_dfw_sections.parsed_response.include?("error")
		        		current_group.section_id = ""
		        		current_group.section_etag = ""
		        		current_group.save
		        	end

		        	nsx_url = "https://" + nsx_manager_ip + "/api/4.0/firewall/globalroot-0/config/layer3sections"
		        	main_payload = "<section name='%s'>%s</section>" % [current_group.name, rule_payloads]
		        	nsx_response = HTTParty.post(nsx_url, body: main_payload, headers: get_and_post_headers, :basic_auth => auth)

			    else # Update section
			    	if all_l3_dfw_sections.parsed_response.include?("error")
			    		nsx_url = "https://" + nsx_manager_ip + "/api/4.0/firewall/globalroot-0/config/layer3sections"
			    		main_payload = "<section name='%s'>%s</section>" % [current_group.name, rule_payloads]
			    		nsx_response = HTTParty.post(nsx_url, body: main_payload, headers: get_and_post_headers, :basic_auth => auth)
			    	else

			    		byebug
			    		if (all_l3_dfw_sections.parsed_response.fetch('sections').fetch('section').has_key?('rule'))
				    		section_rules_array = all_l3_dfw_sections.parsed_response.fetch('sections').fetch('section').fetch('rule')
				    		trimmed_nsx_rules = ""

				    		if section_rules_array.is_a?(Hash)
				    			section_rules_array = [section_rules_array]
				    		end

							section_rules_array.each do |rule|
								rule_in_xml = rule.to_xml(:root => 'rule', skip_types: true).to_s[38..-1]
								modifying_rule = ""

				    			# if rules_to_include.include?(rule_in_xml.slice(/name(.*?)name/, 1)[1...-2])
				    			if rules_to_include.include?(rule.fetch('name'))
					    			# do nothing, the rule will be overwritten
					    		else
					    			modifying_rule = rule_in_xml.gsub("\n", "")
					    			if !modifying_rule.nil?
						    			if modifying_rule.include?('<source>      <source>') # created during the ruby hash to json transfer
						    				modifying_rule = modifying_rule.gsub!('<source>      <source>', '<source>').gsub!('</source>    </source>', '</source>')
						    			end
						    		else
						    			modifying_rule = rule_in_xml.gsub("\n", "")
						    		end

						    		if !modifying_rule.nil?
						    			if modifying_rule.include?('<destination>      <destination>') # created during the ruby hash to json transfer
						    				modifying_rule = modifying_rule.gsub!('<destination>      <destination>', '<destination>').gsub!('</destination>    </destination>', '</destination>')
						    			end
						    		else
						    			modifying_rule = rule_in_xml.gsub("\n", "")
						    		end

						    		if !modifying_rule.nil?
						    			if modifying_rule.include?('<service>      <service>') # created during the ruby hash to json transfer
						    				modifying_rule = modifying_rule.gsub!('<service>      <service>', '<service>').gsub!('</service>    </service>', '</service>')
						    			end
						    		else
						    			modifying_rule = rule_in_xml.gsub("\n", "")
						    		end

						    		trimmed_nsx_rules += modifying_rule
						    	end
						    end
						else 
							print('no rules showing')
						end
					    # updated_group = ApplicationRecord.set_rule_and_group_nsx_vars(current_user, all_l3_dfw_sections, current_group.id, "security_group")
						updated_group_etag = get_generation_number(nsx_manager_ip, current_group, get_and_post_headers, auth, current_user)

					    if_match_headers = {'Content-Type' => 'application/xml', 'If-Match' => updated_group_etag}
					    nsx_url = "https://%s/api/4.0/firewall/globalroot-0/config/layer3sections/%s" % [nsx_manager_ip, current_group.section_id]
					    main_payload = "<section  generationNumber='%s' id='%s' name='%s'>%s%s</section>" % [updated_group_etag, current_group.section_id, current_group.name, rule_payloads, trimmed_nsx_rules]
					    nsx_response = HTTParty.put(nsx_url, body: main_payload, headers: if_match_headers, :basic_auth => auth)

					    if updated_group_etag != "" and all_l3_dfw_sections.parsed_response.include?("error") # create instead of update, should never occur
					    	nsx_url = "https://" + nsx_manager_ip + "/api/4.0/firewall/globalroot-0/config/layer3sections"
					    	main_payload = "<section name='%s'>%s</section>" % [current_group.name, rule_payloads]
					    	nsx_response = HTTParty.post(nsx_url, body: main_payload, headers: get_and_post_headers, :basic_auth => auth)
					    end
					end
				end

				ApplicationRecord.set_rule_and_group_nsx_vars(current_user, all_l3_dfw_sections, current_group.id, "security_group")

			elsif rule_or_section == "update_rule"
				history_item = rule_array[0]
				if_match_headers = {'Content-Type' => 'application/xml', 'If-Match' => history_item.section_etag }
				nsx_url = "https://" + nsx_manager_ip + "/api/4.0/firewall/globalroot-0/config/layer3sections/" + history_item.section_id + "/rules"
				nsx_response = HTTParty.post(nsx_url, body: rule_payloads, headers: if_match_headers, :basic_auth => auth)

			else 	
				history_item = rule_array[0]
				if_match_headers = {'Content-Type' => 'application/xml', 'If-Match' => history_item.section_etag }
				nsx_url = "https://" + nsx_manager_ip + "/api/4.0/firewall/globalroot-0/config/layer3sections/" + history_item.section_id + "/rules"
				nsx_response = HTTParty.post(nsx_url, body: main_payload, headers: if_match_headers, :basic_auth => auth)

			end

			print("--------------    nsx_response.   --------------")
			print("--------------" + nsx_response.body + "--------------")
			print("--------------" + main_payload + "--------------")

			variable = nsx_response.response.inspect.scan(/\d/).join("").to_i

			if (variable >= 200 && variable < 300)
				response = "Successfully created to NSX Manager Distributed Firewall."

				if rule_or_section == "sec_group_section"
					# update the generation number
					history_item_array.each do |rule|
						rule.undo_rule = false
						rule.save
					end
				elsif rule_or_section == "update_rule"
					ApplicationRecord.set_rule_and_group_nsx_vars(current_user, nsx_response, 0, "rule_history")
					rule_array[0].undo_rule = false
					rule_array[0].save
				end

				NotificationsMailer.task_sucessfully_completed_firewall_rules(User.current_user).deliver_now
			elsif variable == 403
				response = "Your NSX Manager username/password combination is incorrent, go to settings to change this."
			elsif variable == 400
				response = "The body of the request was incorrect, please contact the Deloitte Engineer."
			elsif variable == 401
				response = "The NSX User currently set does not have privaliges to complete this action."
			else 
				response = "Nothing happened, please select and try again"
			end
		end

		return response
	end

	def find_prot_num(nname) 
		number = 0
		# some switch statement to set number = to the actual number by the name given
		return number
	end

	def self.get_generation_number(nsx_manager_ip, current_group, get_and_post_headers, auth, current_user)
		nsx_sections_url = "https://" + nsx_manager_ip + "/api/4.0/firewall/globalroot-0/config/layer3sections"
		param_group_name = {:name => current_group.name}
		all_l3_dfw_sections = HTTParty.get(nsx_sections_url, :query => param_group_name, headers: get_and_post_headers, :basic_auth => auth)
		temp = ApplicationRecord.set_rule_and_group_nsx_vars(current_user, all_l3_dfw_sections, current_group.id, "security_group")
		generationNumber = temp.section_etag
		return generationNumber
	end

	def self.nsx_mgr_auth(current_user)
		myheaders = {'Content-Type' => 'application/json', 'Accept' =>'application/json'}
		usr = current_user.setting.vrni_username
		pwd = current_user.setting.vrni_password
		
		current_vrni = Setting.current_vrni
		url = 'https://' + current_vrni + '/api/ni/auth/token'
		# "------------------------------------------------------------------------------------------")
		payload = '{ "username": "' + usr + '",' + '"password": "' + pwd + '",' +  '"domain":  {' + 	'"domain_type": "LOCAL"' + '}' + '}'
		response = HTTParty.post(url, :headers => myheaders, :body => payload)
		# "------------------------------------------------------------------------------------------")
		data = JSON.parse(String(response.body))
		auth = data["token"]
		network_insight = 'NetworkInsight ' + String(auth)
		@fr_auth_header = {"content-type"=>"application/json", 'Authorization' => network_insight}
	end
end
