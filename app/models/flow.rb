class Flow < ApplicationRecord
	belongs_to :nested_sec_gps, required: false
	belongs_to :sec_groups, required: false

	require 'set'

	@nsx_ip_addr = ""

	def self.create_hash_set_and_set_parent(parent_group)
		@obj_list = Set.new
		@parent_group = parent_group
		@nsx_ip_addr = Setting.current_nsx
	end

	def self.group_already_combed(id, object, current_user)
		if @obj_list.include?(object) == false
			@obj_list.add(object)
			find_virtual_machine(id, object, current_user)
		else			
			# This group is a duplicate, look again at your architecture
		end
	end

	def self.find_flows_for_a_sec_group(parent, current_user)
		create_hash_set_and_set_parent(parent) # for security group nesting if you want to allow this feature in the furture with the NestedSecGroups Class
		delete_statement = "delete from flows where sec_group_id = #{parent.id}" # clear for the data refresh
		ActiveRecord::Base.connection.execute(delete_statement)

		# the parent should always have child groups for improved organization and microsegmentation thus group_already_combed is not needed
		Flow.transaction do
			if !parent.nil?
				parent.app_objs.each do |child_obj|
					if child_obj.obj_type.downcase != "securitygroup"
						find_and_add_vm_flows(child_obj.obj_id, current_user) # find all the flows and add them to the parent group
					else
						group_already_combed(child_obj.obj_id, current_user) # check if it is a duplicate
					end
				end
			end
		end
	end

	def self.find_virtual_machine(moid, securitygroup, current_user) # if we encounter a vm try to find vnics or vms as we can only find flows for these, datacenters are not viable, the docs are erronous
		myheaders = {'Content-Type' => 'application/xml'}
		auth = {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
		@nsx_ip_addr = Setting.current_nsx
		url = 'https://' + @nsx_ip_addr + '/api/2.0/services/securitygroup/' + moid
		response = HTTParty.get(
			url,
			:headers => myheaders,
			:basic_auth => auth
			)

		doc = Nokogiri::XML(response.body)
		doc.xpath('//securitygroup').each do |base|
			base.xpath('member').each do |base_child|
				child_moid = base_child.xpath('objectId').text
				child_type = base_child.xpath('objectTypeName').text.downcase
				if child_type != "securitygroup"
					find_and_add_vm_flows(child_moid, current_user) # find all the flows and add them to the parent group
				else
					group_already_combed(child_moid, current_user) # check if it is a duplicate
				end
			end
		end
	end

	def self.find_and_add_vm_flows(moid, current_user)
		nsx_ip_addr = Setting.current_nsx
		myheaders = {'Content-Type' => 'application/xml'}
		auth = {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
		utc_time_today = Time.now.to_i
		utc_time_last_month = (utc_time_today - 1209600) # = 2 weeks back in seconds as utc time is in seconds

		Flow.inset_flows(2, moid, nsx_ip_addr, myheaders, auth, utc_time_today, utc_time_last_month)
		Flow.inset_flows(3, moid, nsx_ip_addr, myheaders, auth, utc_time_today, utc_time_last_month)		
	end

	def self.inset_flows(osi_layer, moid, nsx_ip_addr, myheaders, auth, utc_time_today, utc_time_last_month)
		if osi_layer == 3
			url = "https://" + nsx_ip_addr + "/api/2.1/app/flow/flowstats?contextId=" + moid + "&flowType=LAYER3&startTime=" + utc_time_last_month.to_s + "&endTime=" + utc_time_today.to_s + "&pageSize=300"
			xpath_layer_conversion = "//flowStatsLayer3"
		else
			url = "https://" + nsx_ip_addr + "/api/2.1/app/flow/flowstats?contextId=" + moid + "&flowType=LAYER2&startTime=" + utc_time_last_month.to_s + "&endTime=" + utc_time_today.to_s + "&pageSize=300" # layer 2
			xpath_layer_conversion = "//flowStatsLayer2"
		end
		response = HTTParty.get(url, :headers => myheaders, :basic_auth => auth )
		doc = Nokogiri::XML(response.body)

		execute_query_if_contains_elememts = false
		insert_statement = 'INSERT INTO flows (osi_layer, blocked, protocol, protocol_name, sent_bytes, received_bytes, source, destination, port, sessions, time_out, time_rec, task_group_name, created_at, updated_at, sec_group_id) VALUES '
		doc.xpath(xpath_layer_conversion).each do |page|
			execute_query_if_contains_elememts = true
			protocolName = Flow.protocol(page.xpath('protocol').text)
			if page.xpath('protocol').text.to_i > 255
				protocol = "255" # protocol numbers only go up to 255
			else
				protocol = page.xpath('protocol').text
			end
			blocked = Flow.blocked(page.xpath('blocked').text)

			time_out = "n/a"
			time_rec = "n/a"

			if page.xpath('startTime').text.to_i != 0
				time_out = Time.at(page.xpath('endTime').text[0...-3].to_i)
				time_out = time_out.strftime("l:%M%P %b #{time_out.day.ordinalize}")
			end

			if page.xpath('endTime').text.to_i != 0
				time_rec = Time.at(page.xpath('endTime').text[0...-3].to_i)
				time_rec = time_rec.strftime("%l:%M%P %b #{time_rec.day.ordinalize}")
			end

			insert_statement += "('Layer %s', '%s', %s, '%s', %s, '%s','%s', '%s', %s, %s, '%s', '%s', '%s', '%s', '%s', %s)," % [
				osi_layer,
				blocked,
				protocol,
				protocolName,
				page.xpath('sourceBytes').text, 
				page.xpath('destinationBytes').text, 
				page.xpath('source').text, 
				page.xpath('destination').text, 
				page.xpath('destinationPort').text, 
				page.xpath('sessions').text, 
				time_out, 
				time_rec, 
				@parent_group.name, 
				utc_time_today, 
				utc_time_today, 
				@parent_group.id]

		end
		if execute_query_if_contains_elememts
			ActiveRecord::Base.connection.execute(insert_statement[0...-1])
		end
	end

	def self.protocol(protocol)

		case protocol
		when "0"	
			protocol = "HOPOPT"
		when "1"
			protocol = "ICMP"
		when "2"	
			protocol = "IGMP"
		when "3"	
			protocol = "GGP"
		when "4"	
			protocol = "IP-in-IP"
		when "5"	
			protocol = "ST"
		when "6"	
			protocol = "TCP"
		when "7"	
			protocol = "CBT"
		when "8"	
			protocol = "EGP"
		when "9"	
			protocol = "IGP"
		when "10"	
			protocol = "BBN-RCC-MON"
		when "11"	
			protocol = "NVP-II"
		when "12"	
			protocol = "PUP"
		when "13"	
			protocol = "ARGUS"
		when "14"	
			protocol = "EMCON"
		when "15"	
			protocol = "XNET"
		when "16"	
			protocol = "CHAOS"
		when "17"	
			protocol = "UDP"
		when "18"	
			protocol = "MUX"
		when "19"	
			protocol = "DCN-MEAS"
		when "20"	
			protocol = "HMP"
		when "21"	
			protocol = "PRM"
		when "22"	
			protocol = "XNS-IDP"
		when "23"	
			protocol = "TRUNK-1"
		when "24"	
			protocol = "TRUNK-2"
		when "25"	
			protocol = "LEAF-1"
		when "26"	
			protocol = "LEAF-2"
		when "27"	
			protocol = "RDP"
		when "28"	
			protocol = "IRTP"
		when "29"	
			protocol = "ISO-TP4"
		when "30"	
			protocol = "NETBLT"
		when "31"	
			protocol = "MFE-NSP"
		when "32"	
			protocol = "MERIT-INP"
		when "33"	
			protocol = "DCCP"
		when "34"	
			protocol = "3PC"
		when "35"	
			protocol = "IDPR"
		when "36"	
			protocol = "XTP"
		when "37"	
			protocol = "DDP"
		when "38"	
			protocol = "IDPR-CMTP"
		when "39"	
			protocol = "TP++"
		when "40"	
			protocol = "IL"
		when "41"	
			protocol = "IPv6"
		when "42"	
			protocol = "SDRP"
		when "43"	
			protocol = "IPv6-Route"
		when "44"	
			protocol = "IPv6-Frag"
		when "45"	
			protocol = "IDRP"
		when "46"	
			protocol = "RSVP"
		when "47"	
			protocol = "GREs"
		when "48"	
			protocol = "DSR"
		when "49"	
			protocol = "BNA"
		when "50"	
			protocol = "ESP"
		when "51"	
			protocol = "AH"
		when "52"	
			protocol = "I-NLSP"
		when "53"	
			protocol = "SWIPE"
		when "54"	
			protocol = "NARP"
		when "55"	
			protocol = "MOBILE"
		when "56"	
			protocol = "TLSP"
		when "57"	
			protocol = "SKIP"
		when "58"	
			protocol = "IPv6-ICMP"
		when "59"	
			protocol = "IPv6-NoNxt"
		when "60"	
			protocol = "IPv6-Opts"
		when "62"	
			protocol = "CFTP"
		when "64"	
			protocol = "SAT-EXPAK"
		when "65"	
			protocol = "KRYPTOLAN"
		when "66"	
			protocol = "RVD"
		when "67"	
			protocol = "IPPC"	
		when "69"	
			protocol = "SAT-MON"
		when "70"	
			protocol = "VISA"
		when "71"	
			protocol = "IPCU"
		when "72"	
			protocol = "CPNX"
		when "73"	
			protocol = "CPHB"
		when "74"	
			protocol = "WSN"
		when "75"	
			protocol = "PVP"
		when "76"	
			protocol = "BR-SAT-MON"
		when "77"	
			protocol = "SUN-ND"
		when "78"	
			protocol = "WB-MON"
		when "79"	
			protocol = "WB-EXPAK"
		when "80"	
			protocol = "ISO-IP"
		when "81"	
			protocol = "VMTP"
		when "82"	
			protocol = "SECURE-VMTP"
		when "83"	
			protocol = "VINES"
		when "84"	
			protocol = "TTP"
		when "84"	
			protocol = "IPTM"
		when "85"	
			protocol = "NSFNET-IGP"
		when "86"	
			protocol = "DGP"
		when "87"	
			protocol = "TCF"
		when "88"	
			protocol = "EIGRP"
		when "89"	
			protocol = "OSPF"
		when "90"	
			protocol = "Sprite-RPC"
		when "91"	
			protocol = "LARP"
		when "92"	
			protocol = "MTP"
		when "93"	
			protocol = "AX.25"
		when "94"	
			protocol = "OS"
		when "95"	
			protocol = "MICP"
		when "96"	
			protocol = "SCC-SP"
		when "97"	
			protocol = "ETHERIP"
		when "98"	
			protocol = "ENCAP"
		when "100"	
			protocol = "GMTP"
		when "101"	
			protocol = "IFMP"
		when "102"	
			protocol = "PNNI"
		when "103"	
			protocol = "PIM"
		when "104"	
			protocol = "ARIS"
		when "105"	
			protocol = "SCPS"
		when "106"	
			protocol = "QNX"
		when "107"	
			protocol = "A/N"
		when "108"	
			protocol = "IPComp"
		when "109"	
			protocol = "SNP"
		when "110"	
			protocol = "Compaq-Peer"
		when "111"	
			protocol = "IPX-in-IP"
		when "112"	
			protocol = "VRRP"
		when "113"	
			protocol = "PGM"
		when "115"	
			protocol = "L2TP"
		when "116"	
			protocol = "DDX"
		when "117"	
			protocol = "IATP"
		when "118"	
			protocol = "STP"
		when "119"	
			protocol = "SRP"
		when "120"	
			protocol = "UTI"
		when "121"	
			protocol = "SMP"
		when "122"	
			protocol = "SM"
		when "123"	
			protocol = "PTP"
		when "124"	
			protocol = "IS-IS over IPv4"
		when "125"	
			protocol = "FIRE"
		when "126"	
			protocol = "CRTP"
		when "127"	
			protocol = "CRUDP"
		when "128"	
			protocol = "SSCOPMCE"
		when "129"	
			protcol = "IPLT"
		when "130"	
			protcol = "SPS"
		when "131"	
			protcol = "PIPE"
		when "132"	
			protcol = "SCTP"
		when "133"	
			protcol = "FC"
		when "134"	
			protcol = "RSVP-E2E-IGNORE"
		when "135"	
			protcol = "Mobility Header"
		when "136"	
			protcol = "UDPLite"
		when "137"	
			protcol = "MPLS-in-IP"
		when "138"	
			protcol = "manet"
		when "139"	
			protcol = "HIP"
		when "140"	
			protcol = "Shim6"
		when "141"	
			protcol = "WESP"
		when "142"	
			protcol = "ROHC"
		else 
			protocol = "L3_OTHERS"
		end 

		return protocol
	end


	def self.blocked(blocked)

		case blocked
		when "0"
			blocked = "Allow"
		when "1"
			blocked = "General Block"
		else "2"
			blocked = "Spoof Grd Block"
		end

		return blocked
	end


	def self.find_flow_source_dest(current_user, mac_set_hash, input, origin)
		input_type = ""
		api_text = ""
		is_a_ip = IPAddr.new(input) rescue false

		if is_a_ip == false # it is a mac address that needs to be converted to a unique mac set for NSX 
			if input.length == 17
				input_type = "MACSet"
			end
		else #  output: '#<IPAddr: IPv6:fe80:0000:0000:0000:0250:56ff:fe94:cb0f/ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff>'
			if is_a_ip.to_s.include?(":")
				input_type = "Ipv6Address"
			else
				input_type = "Ipv4Address"
			end
		end

		if input_type != ""

			if input_type == "MacSet" and !mac_set_hash.keys.include?(input)
				input = Vm.create_mac_set(current_user, input, input)
			end

			api_text = '
			<' + origin + '>
			<value>' + input + '</value>
			<type>' + input_type + '</type>
			<isValid>true</isValid>
			</' + origin + '>'
		end

		return api_text
	end
end
