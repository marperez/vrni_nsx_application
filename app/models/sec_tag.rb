class SecTag < ApplicationRecord
	belongs_to :app_obj, optional: true
	validates :tag_name, uniqueness: { scope: :app_obj_id }

end
