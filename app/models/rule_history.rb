class RuleHistory < ApplicationRecord
	belongs_to :user
	has_many :protocols, dependent: :destroy
	has_many :destinations, dependent: :destroy
	has_many :sources, dependent: :destroy
	has_many :port_ranges, dependent: :destroy
	has_many :services, dependent: :destroy
	has_many :applied_tos

	def self.remove_nsx_rule(current_user, history_item)
		nsx_ip_addr = Setting.current_nsx

		url = "https://" + nsx_ip_addr + "/api/4.0/firewall/globalroot-0/config"
		auth =  {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
		headers = {'Content-Type' => 'application/xml'}
		
		byebug
		resp = HTTParty.get(url, :headers => headers, :basic_auth => auth)
		ApplicationRecord.set_rule_and_group_nsx_vars(current_user, resp, history_item, "rule_history")

		headers = {'Content-Type' => 'application/xml','If-Match' => history_item.section_etag}

		url = "https://" + nsx_ip_addr + "/api/4.0/firewall/globalroot-0/config/layer3sections/"+ history_item.section_id + "/rules/" + history_item.nsx_rule_id
		auth =  {username: current_user.setting.nsx_username, password: current_user.setting.nsx_password}
		delete_rule_response = HTTParty.delete(url, :headers => headers, :basic_auth => auth)

		print("---------------------------")
		print(delete_rule_response)
		print(url)
		print(auth)
		http_code = delete_rule_response.response.inspect.scan(/\d/).join("").to_i
		if http_code >= 200 && http_code < 300
			history_item.undo_rule = true
			history_item.save
		end

	end

	def self.resubmit_nsx_rule(rule_id, current_user)
		history_item = RuleHistory.find_by_id(rule_id)
		history_item.undo_rule = false
		history_item.save


		mgr_ip = Setting.current_nsx

		resp = ApplicationRecord.get_dfw_sections(current_user)
		ApplicationRecord.set_rule_and_group_nsx_vars(current_user, resp, rule_id, "rule_history")

		@rules = [history_item]
		return FireRule.send_selected_rules_to_nsx(current_user, @rules, "update_rule", 0, mgr_ip)
	end

	def self.refresh_user_history(current_user, doc)
		etag_value = ""
		section_id = ""
		section_name = ""
		rule_nsx_id = ""

		id_array = []
		doc.xpath('//firewallConfiguration/layer3Sections/section').each do |section|

			correct_section = false
			section.xpath('//rule').each do |rule|
				rule_name = rule.xpath('name').text
				rule = RuleHistory.find_by(name: rule_name)
				if condition
					
				end

			end
		end
	end

end
