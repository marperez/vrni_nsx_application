class Setting < ApplicationRecord
	belongs_to :user
	has_many :vrni_applications
	has_many :nsx_managers

	accepts_nested_attributes_for :nsx_managers, :allow_destroy => true
	accepts_nested_attributes_for :vrni_applications, :allow_destroy => true


	def self.current_nsx
		ip = User.current_user.setting.nsx_managers.where(:current => true).select(:ip_addr).first
		if ip
			ip = ip.ip_addr
			return ip
		else
			return ""
		end
	end

	def self.current_vrni_user(user)
		ip = user.setting.vrni_applications.where(:current => true).select(:vrni_addr).first
		if ip
			ip = ip.vrni_addr
			return ip
		else
			return ""
		end
	end
  
	def self.current_vrni
		ip = User.current_user.setting.vrni_applications.where(:current => true).select(:vrni_addr).first
		if ip
			ip = ip.vrni_addr
			return ip
		else
			return ""
		end
	end

end
