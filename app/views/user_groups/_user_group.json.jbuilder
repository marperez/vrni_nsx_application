json.extract! user_group, :id, :user_id, :sec_group_id, :created_at, :updated_at
json.url user_group_url(user_group, format: :json)
