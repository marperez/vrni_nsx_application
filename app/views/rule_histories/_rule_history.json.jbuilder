json.extract! rule_history, :id, :name, :action, :direction, :pack_type, :user_id, :created_at, :updated_at
json.url rule_history_url(rule_history, format: :json)
