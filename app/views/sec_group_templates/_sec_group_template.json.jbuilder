json.extract! sec_group_template, :id, :sec_group_id, :created_at, :updated_at
json.url sec_group_template_url(sec_group_template, format: :json)
