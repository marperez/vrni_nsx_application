require 'test_helper'

class AppObjsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @app_obj = app_objs(:one)
  end

  test "should get index" do
    get app_objs_url
    assert_response :success
  end

  test "should get new" do
    get new_app_obj_url
    assert_response :success
  end

  test "should create app_obj" do
    assert_difference('AppObj.count') do
      post app_objs_url, params: { app_obj: {  } }
    end

    assert_redirected_to app_obj_url(AppObj.last)
  end

  test "should show app_obj" do
    get app_obj_url(@app_obj)
    assert_response :success
  end

  test "should get edit" do
    get edit_app_obj_url(@app_obj)
    assert_response :success
  end

  test "should update app_obj" do
    patch app_obj_url(@app_obj), params: { app_obj: {  } }
    assert_redirected_to app_obj_url(@app_obj)
  end

  test "should destroy app_obj" do
    assert_difference('AppObj.count', -1) do
      delete app_obj_url(@app_obj)
    end

    assert_redirected_to app_objs_url
  end
end
