require 'test_helper'

class SecGroupTemplatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sec_group_template = sec_group_templates(:one)
  end

  test "should get index" do
    get sec_group_templates_url
    assert_response :success
  end

  test "should get new" do
    get new_sec_group_template_url
    assert_response :success
  end

  test "should create sec_group_template" do
    assert_difference('SecGroupTemplate.count') do
      post sec_group_templates_url, params: { sec_group_template: { sec_group_id: @sec_group_template.sec_group_id } }
    end

    assert_redirected_to sec_group_template_url(SecGroupTemplate.last)
  end

  test "should show sec_group_template" do
    get sec_group_template_url(@sec_group_template)
    assert_response :success
  end

  test "should get edit" do
    get edit_sec_group_template_url(@sec_group_template)
    assert_response :success
  end

  test "should update sec_group_template" do
    patch sec_group_template_url(@sec_group_template), params: { sec_group_template: { sec_group_id: @sec_group_template.sec_group_id } }
    assert_redirected_to sec_group_template_url(@sec_group_template)
  end

  test "should destroy sec_group_template" do
    assert_difference('SecGroupTemplate.count', -1) do
      delete sec_group_template_url(@sec_group_template)
    end

    assert_redirected_to sec_group_templates_url
  end
end
