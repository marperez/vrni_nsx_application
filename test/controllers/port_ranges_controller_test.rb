require 'test_helper'

class PortRangesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @port_range = port_ranges(:one)
  end

  test "should get index" do
    get port_ranges_url
    assert_response :success
  end

  test "should get new" do
    get new_port_range_url
    assert_response :success
  end

  test "should create port_range" do
    assert_difference('PortRange.count') do
      post port_ranges_url, params: { port_range: {  } }
    end

    assert_redirected_to port_range_url(PortRange.last)
  end

  test "should show port_range" do
    get port_range_url(@port_range)
    assert_response :success
  end

  test "should get edit" do
    get edit_port_range_url(@port_range)
    assert_response :success
  end

  test "should update port_range" do
    patch port_range_url(@port_range), params: { port_range: {  } }
    assert_redirected_to port_range_url(@port_range)
  end

  test "should destroy port_range" do
    assert_difference('PortRange.count', -1) do
      delete port_range_url(@port_range)
    end

    assert_redirected_to port_ranges_url
  end
end
