require 'test_helper'

class SecGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sec_group = sec_groups(:one)
  end

  test "should get index" do
    get sec_groups_url
    assert_response :success
  end

  test "should get new" do
    get new_sec_group_url
    assert_response :success
  end

  test "should create sec_group" do
    assert_difference('SecGroup.count') do
      post sec_groups_url, params: { sec_group: {  } }
    end

    assert_redirected_to sec_group_url(SecGroup.last)
  end

  test "should show sec_group" do
    get sec_group_url(@sec_group)
    assert_response :success
  end

  test "should get edit" do
    get edit_sec_group_url(@sec_group)
    assert_response :success
  end

  test "should update sec_group" do
    patch sec_group_url(@sec_group), params: { sec_group: {  } }
    assert_redirected_to sec_group_url(@sec_group)
  end

  test "should destroy sec_group" do
    assert_difference('SecGroup.count', -1) do
      delete sec_group_url(@sec_group)
    end

    assert_redirected_to sec_groups_url
  end
end
