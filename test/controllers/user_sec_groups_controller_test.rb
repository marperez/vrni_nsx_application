require 'test_helper'

class UserSecGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user_sec_group = user_sec_groups(:one)
  end

  test "should get index" do
    get user_sec_groups_url
    assert_response :success
  end

  test "should get new" do
    get new_user_sec_group_url
    assert_response :success
  end

  test "should create user_sec_group" do
    assert_difference('UserSecGroup.count') do
      post user_sec_groups_url, params: { user_sec_group: { sec_group_id: @user_sec_group.sec_group_id, user_id: @user_sec_group.user_id } }
    end

    assert_redirected_to user_sec_group_url(UserSecGroup.last)
  end

  test "should show user_sec_group" do
    get user_sec_group_url(@user_sec_group)
    assert_response :success
  end

  test "should get edit" do
    get edit_user_sec_group_url(@user_sec_group)
    assert_response :success
  end

  test "should update user_sec_group" do
    patch user_sec_group_url(@user_sec_group), params: { user_sec_group: { sec_group_id: @user_sec_group.sec_group_id, user_id: @user_sec_group.user_id } }
    assert_redirected_to user_sec_group_url(@user_sec_group)
  end

  test "should destroy user_sec_group" do
    assert_difference('UserSecGroup.count', -1) do
      delete user_sec_group_url(@user_sec_group)
    end

    assert_redirected_to user_sec_groups_url
  end
end
