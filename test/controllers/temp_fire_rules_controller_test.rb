require 'test_helper'

class TempFireRulesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @temp_fire_rule = temp_fire_rules(:one)
  end

  test "should get index" do
    get temp_fire_rules_url
    assert_response :success
  end

  test "should get new" do
    get new_temp_fire_rule_url
    assert_response :success
  end

  test "should create temp_fire_rule" do
    assert_difference('TempFireRule.count') do
      post temp_fire_rules_url, params: { temp_fire_rule: {  } }
    end

    assert_redirected_to temp_fire_rule_url(TempFireRule.last)
  end

  test "should show temp_fire_rule" do
    get temp_fire_rule_url(@temp_fire_rule)
    assert_response :success
  end

  test "should get edit" do
    get edit_temp_fire_rule_url(@temp_fire_rule)
    assert_response :success
  end

  test "should update temp_fire_rule" do
    patch temp_fire_rule_url(@temp_fire_rule), params: { temp_fire_rule: {  } }
    assert_redirected_to temp_fire_rule_url(@temp_fire_rule)
  end

  test "should destroy temp_fire_rule" do
    assert_difference('TempFireRule.count', -1) do
      delete temp_fire_rule_url(@temp_fire_rule)
    end

    assert_redirected_to temp_fire_rules_url
  end
end
