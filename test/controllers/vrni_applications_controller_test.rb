require 'test_helper'

class VrniApplicationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @vrni_application = vrni_applications(:one)
  end

  test "should get index" do
    get vrni_applications_url
    assert_response :success
  end

  test "should get new" do
    get new_vrni_application_url
    assert_response :success
  end

  test "should create vrni_application" do
    assert_difference('VrniApplication.count') do
      post vrni_applications_url, params: { vrni_application: { setting_id: @vrni_application.setting_id, vrni_addr: @vrni_application.vrni_addr } }
    end

    assert_redirected_to vrni_application_url(VrniApplication.last)
  end

  test "should show vrni_application" do
    get vrni_application_url(@vrni_application)
    assert_response :success
  end

  test "should get edit" do
    get edit_vrni_application_url(@vrni_application)
    assert_response :success
  end

  test "should update vrni_application" do
    patch vrni_application_url(@vrni_application), params: { vrni_application: { setting_id: @vrni_application.setting_id, vrni_addr: @vrni_application.vrni_addr } }
    assert_redirected_to vrni_application_url(@vrni_application)
  end

  test "should destroy vrni_application" do
    assert_difference('VrniApplication.count', -1) do
      delete vrni_application_url(@vrni_application)
    end

    assert_redirected_to vrni_applications_url
  end
end
