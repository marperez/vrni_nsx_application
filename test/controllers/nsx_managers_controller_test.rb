require 'test_helper'

class NsxManagersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @nsx_manager = nsx_managers(:one)
  end

  test "should get index" do
    get nsx_managers_url
    assert_response :success
  end

  test "should get new" do
    get new_nsx_manager_url
    assert_response :success
  end

  test "should create nsx_manager" do
    assert_difference('NsxManager.count') do
      post nsx_managers_url, params: { nsx_manager: { ip_addr: @nsx_manager.ip_addr, setting_id: @nsx_manager.setting_id } }
    end

    assert_redirected_to nsx_manager_url(NsxManager.last)
  end

  test "should show nsx_manager" do
    get nsx_manager_url(@nsx_manager)
    assert_response :success
  end

  test "should get edit" do
    get edit_nsx_manager_url(@nsx_manager)
    assert_response :success
  end

  test "should update nsx_manager" do
    patch nsx_manager_url(@nsx_manager), params: { nsx_manager: { ip_addr: @nsx_manager.ip_addr, setting_id: @nsx_manager.setting_id } }
    assert_redirected_to nsx_manager_url(@nsx_manager)
  end

  test "should destroy nsx_manager" do
    assert_difference('NsxManager.count', -1) do
      delete nsx_manager_url(@nsx_manager)
    end

    assert_redirected_to nsx_managers_url
  end
end
