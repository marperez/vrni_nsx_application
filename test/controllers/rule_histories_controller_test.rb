require 'test_helper'

class RuleHistoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rule_history = rule_histories(:one)
  end

  test "should get index" do
    get rule_histories_url
    assert_response :success
  end

  test "should get new" do
    get new_rule_history_url
    assert_response :success
  end

  test "should create rule_history" do
    assert_difference('RuleHistory.count') do
      post rule_histories_url, params: { rule_history: { action: @rule_history.action, direction: @rule_history.direction, name: @rule_history.name, pack_type: @rule_history.pack_type, user_id: @rule_history.user_id } }
    end

    assert_redirected_to rule_history_url(RuleHistory.last)
  end

  test "should show rule_history" do
    get rule_history_url(@rule_history)
    assert_response :success
  end

  test "should get edit" do
    get edit_rule_history_url(@rule_history)
    assert_response :success
  end

  test "should update rule_history" do
    patch rule_history_url(@rule_history), params: { rule_history: { action: @rule_history.action, direction: @rule_history.direction, name: @rule_history.name, pack_type: @rule_history.pack_type, user_id: @rule_history.user_id } }
    assert_redirected_to rule_history_url(@rule_history)
  end

  test "should destroy rule_history" do
    assert_difference('RuleHistory.count', -1) do
      delete rule_history_url(@rule_history)
    end

    assert_redirected_to rule_histories_url
  end
end
