require 'test_helper'

class NestedSecGpsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @nested_sec_gp = nested_sec_gps(:one)
  end

  test "should get index" do
    get nested_sec_gps_url
    assert_response :success
  end

  test "should get new" do
    get new_nested_sec_gp_url
    assert_response :success
  end

  test "should create nested_sec_gp" do
    assert_difference('NestedSecGp.count') do
      post nested_sec_gps_url, params: { nested_sec_gp: { object_id: @nested_sec_gp.object_id, object_type: @nested_sec_gp.object_type, sec_group_id: @nested_sec_gp.sec_group_id } }
    end

    assert_redirected_to nested_sec_gp_url(NestedSecGp.last)
  end

  test "should show nested_sec_gp" do
    get nested_sec_gp_url(@nested_sec_gp)
    assert_response :success
  end

  test "should get edit" do
    get edit_nested_sec_gp_url(@nested_sec_gp)
    assert_response :success
  end

  test "should update nested_sec_gp" do
    patch nested_sec_gp_url(@nested_sec_gp), params: { nested_sec_gp: { object_id: @nested_sec_gp.object_id, object_type: @nested_sec_gp.object_type, sec_group_id: @nested_sec_gp.sec_group_id } }
    assert_redirected_to nested_sec_gp_url(@nested_sec_gp)
  end

  test "should destroy nested_sec_gp" do
    assert_difference('NestedSecGp.count', -1) do
      delete nested_sec_gp_url(@nested_sec_gp)
    end

    assert_redirected_to nested_sec_gps_url
  end
end
