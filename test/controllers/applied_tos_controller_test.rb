require 'test_helper'

class AppliedTosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @applied_to = applied_tos(:one)
  end

  test "should get index" do
    get applied_tos_url
    assert_response :success
  end

  test "should get new" do
    get new_applied_to_url
    assert_response :success
  end

  test "should create applied_to" do
    assert_difference('AppliedTo.count') do
      post applied_tos_url, params: { applied_to: {  } }
    end

    assert_redirected_to applied_to_url(AppliedTo.last)
  end

  test "should show applied_to" do
    get applied_to_url(@applied_to)
    assert_response :success
  end

  test "should get edit" do
    get edit_applied_to_url(@applied_to)
    assert_response :success
  end

  test "should update applied_to" do
    patch applied_to_url(@applied_to), params: { applied_to: {  } }
    assert_redirected_to applied_to_url(@applied_to)
  end

  test "should destroy applied_to" do
    assert_difference('AppliedTo.count', -1) do
      delete applied_to_url(@applied_to)
    end

    assert_redirected_to applied_tos_url
  end
end
