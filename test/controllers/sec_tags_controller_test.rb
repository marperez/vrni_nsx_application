require 'test_helper'

class SecTagsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sec_tag = sec_tags(:one)
  end

  test "should get index" do
    get sec_tags_url
    assert_response :success
  end

  test "should get new" do
    get new_sec_tag_url
    assert_response :success
  end

  test "should create sec_tag" do
    assert_difference('SecTag.count') do
      post sec_tags_url, params: { sec_tag: { tag_name: @sec_tag.tag_name } }
    end

    assert_redirected_to sec_tag_url(SecTag.last)
  end

  test "should show sec_tag" do
    get sec_tag_url(@sec_tag)
    assert_response :success
  end

  test "should get edit" do
    get edit_sec_tag_url(@sec_tag)
    assert_response :success
  end

  test "should update sec_tag" do
    patch sec_tag_url(@sec_tag), params: { sec_tag: { tag_name: @sec_tag.tag_name } }
    assert_redirected_to sec_tag_url(@sec_tag)
  end

  test "should destroy sec_tag" do
    assert_difference('SecTag.count', -1) do
      delete sec_tag_url(@sec_tag)
    end

    assert_redirected_to sec_tags_url
  end
end
