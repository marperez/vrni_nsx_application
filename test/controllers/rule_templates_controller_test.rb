require 'test_helper'

class RuleTemplatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rule_template = rule_templates(:one)
  end

  test "should get index" do
    get rule_templates_url
    assert_response :success
  end

  test "should get new" do
    get new_rule_template_url
    assert_response :success
  end

  test "should create rule_template" do
    assert_difference('RuleTemplate.count') do
      post rule_templates_url, params: { rule_template: { name: @rule_template.name } }
    end

    assert_redirected_to rule_template_url(RuleTemplate.last)
  end

  test "should show rule_template" do
    get rule_template_url(@rule_template)
    assert_response :success
  end

  test "should get edit" do
    get edit_rule_template_url(@rule_template)
    assert_response :success
  end

  test "should update rule_template" do
    patch rule_template_url(@rule_template), params: { rule_template: { name: @rule_template.name } }
    assert_redirected_to rule_template_url(@rule_template)
  end

  test "should destroy rule_template" do
    assert_difference('RuleTemplate.count', -1) do
      delete rule_template_url(@rule_template)
    end

    assert_redirected_to rule_templates_url
  end
end
