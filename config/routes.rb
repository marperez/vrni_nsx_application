Rails.application.routes.draw do


	resources :user_groups

	namespace :security_files do
		resources :sec_tags
		resources :sec_group_templates
		resources :flows
	end

	namespace :protocol_files do
		resources :nsx_managers
	end

	namespace :admin_files do
		resources :settings
		resources :vrni_applications
	end

	namespace :user_files do
	  resources :users
	end

	namespace :fire_wall_files do
		resources :temp_fire_rules
		resources :rule_histories
		resources :rule_templates
		resources :fire_rules
		resources :sug_rule
	end

	namespace :security_files do
		resources :sec_groups
	end
	
	resources :services
	resources :applied_tos
	resources :nested_sec_gps
	resources :app_objs
	resources :vms

	root 'user_files/users#home'

	resources :app do

		namespace :fire_wall_files do
			resources :temp_fire_rules do
				resources :port_ranges
				resources :protocols
				resources :applied_tos
				resources :destinations
				resources :sources
				resources :services
			end
		end

		collection { post :import } # import function's routes for the app csv file
	    resources :tiers do

	    	namespace :fire_wall_files do
		    	resources :sug_rules do
		    		resources :port_ranges
		    		resources :protocols
		    		resources :applied_tos
		    		resources :destinations
		    		resources :sources
		    		resources :services
		    	end
		    end

	    	resources :app_objs do
	    		namespace :fire_wall_files do
			  		resources :fire_rules do
						resources :port_ranges
						resources :protocols
						resources :applied_tos
						resources :destinations
						resources :sources
						resources :services
					end
				end
			end
		end
	end

	resources :sec_groups do
		collection {post :import_groups_and_objs} # import function's routes for the app csv file

		namespace :fire_wall_files do
			resources :sug_rules do
				collection { patch :add_sug_rules }
				resources :port_ranges
				resources :protocols
				resources :applied_tos
				resources :destinations
				resources :sources
				resources :services
			end
		end
		
		resources :nested_sec_gps do
			resources :app_objs
		end		
	end

	namespace :fire_wall_files do
		resources :fire_rules do
			collection { get :import_fw_rules }
			resources :port_ranges
			resources :protocols
			resources :applied_tos
			resources :destinations
			resources :sources
			resources :services
		end
	end
	
	namespace :fire_wall_files do
		resources :temp_fire_rules do

			resources :port_ranges
			resources :protocols
			resources :applied_tos
			resources :destinations
			resources :sources
			resources :services

		end
	end

	resources :sec_groups do
		namespace :fire_wall_files do
			resources :temp_fire_rules do

				resources :port_ranges
				resources :protocols
				resources :applied_tos
				resources :destinations
				resources :sources
				resources :services

			end
		end
	end


	resources :sec_groups do
		resources :flows
	end

	resources :nested_sec_gps do
		resources :flows
	end


###########################
#   Custom Routes Below
###########################


#   Flows 
	get '/allow_flow/:id', to: 'security_files/flows#allow_flow', as: "allow_flow"
	get '/deny_flow/:id', to: 'security_files/flows#deny_flow', as: "deny_flow"
	delete '/remove_flow/:id', to: 'security_files/flows#remove_flow_from_nsx', as: "remove_flow_from_nsx"

#   Application
	post '/filter/:column_array/object/:object_class', to: 'application#filter_columns', as: "filter_columns"
	get '/columns/select', to: 'application#select_filter_columns', as: "select_filter_columns"
	get '/filter/:row_totals/columns/:column_array/:origin', to: 'application#add_column_sort_modal_item', as: "add_column_sort_modal_item"

# 	Histories
	get '/remove_rule/:rule_id', to: "fire_wall_files/rule_histories#remove_nsx_rule", as: "remove_nsx_rule"
	get '/resubmit_rule/:rule_id', to: "fire_wall_files/rule_histories#resubmit_nsx_rule", as: "resubmit_nsx_rule"

#   rule_table
	get '/change_name', to: "application#add_to_table_column"

#   Rule Creation
	get '/sysobj/:template_id', to: "application#add_to_table_column", as: "add_protocol_by_num_to_table"
	get '/sgp_task/:task_id/:user_id', to: 'user_files/users#attach_sec_group_user', as: "attach_sec_group_user"
	get '/new_template_rule/:template_id/:rule_id', to: 'fire_wall_files/rule_templates#add_rules_to_template', as: "add_rules_to_template"
	post '/new_rule/:type/:which_action/:template_id/:rule_id/:which_controller/:vmware_object_id', to: 'application#add_to_table_column', as: "add_to_table_column"
	get '/delete_item/:template_id/:rule_id/:item_type/:item_id/:which_controller', to: "application#delete_rule_item", as: "delete_template_rule_item"

#   Templates
    post '/template/:id/name', to: 'fire_wall_files/rule_templates#set_template_name', as: "set_template_name"
	get '/new_template/', to: 'fire_wall_files/rule_templates#create_a_template', as: "create_a_template"
	get '/template/:id/new_rule/:rule_id', to: 'fire_wall_files/rule_templates#cancel_rule_to_template', as: "cancel_rule_to_template"
	get '/template/:id/rules', to: 'fire_wall_files/rule_templates#see_template_rules', as: "see_template_rules_lite"
	get '/template/:id/rules/:rule', to: 'fire_wall_files/rule_templates#see_template_rules', as: "see_template_rules"
	get '/template/:id', to: 'fire_wall_files/rule_templates#edit_template', as: "edit_template"
	delete '/template/:id', to: 'fire_wall_files/rule_templates#destroy', as: "delete_a_template"
	delete 'template/:template_id/rule/:rule_id', to: 'fire_wall_files/rule_templates#delete_a_template_rule', as: "delete_a_template_rule"
	get 'template/:template_id/rule/:rule_id', to: 'fire_wall_files/rule_templates#edit_a_template_rule', as: "edit_a_template_rule"
	# get 'search_templates', to: "fire_wall_files/rule_templates#search_templates"
	post 'template/:template_id', to: "fire_wall_files/rule_templates#add_template_to_group", as: "add_template_to_group"
	get '/search_templates', to: "fire_wall_files/rule_templates#search_templates", as: "search_templates"
	delete '/remove_template/:template_id', to: "fire_wall_files/rule_templates#remove_template_from_group", as: "remove_template_from_group"

#   User Setting
	get '/settings', to: 'admin_files/settings#index', as: "settings_index"
	patch '/edit_manager/:nsx', to: 'admin_files/settings#update_nsx', as: "nsx_manager_edit"
	patch '/edit_manager/:vrni', to: 'admin_files/settings#update_vrni', as: "vrin_machine_edit"
	post '/settings_error', to: 'admin_files/settings#settings_error', as: "settings_error"
	post '/settings_save', to: 'admin_files/settings#settings_save', as: "settings_save"
	post '/settings_cancel', to: 'admin_files/settings#settings_cancel', as: "settings_cancel"
	patch '/settings_update/:id/:type', to: 'admin_files/settings#settings_update', as: "settings_update"

#   Children of Settings Class
	post '/vrni_applications', to: 'admin_files/vrni_applications#create', as: "add_vrni"
	post '/nsx_managers', to: 'protocol_files/nsx_managers#create', as: "add_nsx"
	put '/edit_vrni/:id', to: 'admin_files/vrni_applications#destroy', as: "edit_vrni"
	put '/edit_nsx/:id', to: 'protocol_files/nsx_managers#destroy', as: "edit_nsx"
	delete '/delete_vrni/:id', to: 'admin_files/vrni_applications#destroy', as: "delete_vrni"
	delete '/delete_nsx/:id', to: 'protocol_files/nsx_managers#destroy', as: "delete_nsx"
	post '/make_current_vrni/:id', to: 'admin_files/vrni_applications#current', as: "make_current_vrni_ipaddr" 
	post '/make_current_nsx/:id', to: 'protocol_files/nsx_managers#current', as: "make_current_nsx_ipaddr" 

#   Sessions
	get '/signup' => 'user_files/users#new'
	post '/login' => 'sessions#create'
	get '/login', to: 'sessions#new'
	get '/logout' => 'sessions#destroy'
	post '/user' => 'user_files/users#create'
	get '/home', to: 'user_files/users#home', as: "home"
	get '/ui', to: 'user_files/users#admin_home', as: "admin_home"
	get '/login_button', to: 'user_files/users#login_button', as: "login_button"
	get '/signup_button', to: 'user_files/users#signup_button', as: "signup_button"

#   Users
	get '/user/:id/notify', to: 'user_files/users#notification_email', as: 'notification_email'
	get '/email', to: "user_files/users#remind_each_user", as: "remind_each_user"
	post '/import_users', to: "user_files/users#import_users", as: "import_users"
	post '/create_user', to: "user_files/users#manual_new_user", as: "manual_new_user"
	get '/delete_user/:id', to: 'user_files/users#delete_user', as: "delete_user"
	get '/search_groups', to: 'user_files/users#search_security_groups', as: "search_security_groups"
	post '/new_user', to: 'user_files/users#create', as: "create_a_user"
	get '/add_sec_to_user/:task_id/user/:user_id', to: 'security_files/sec_groups#add_sec_to_user', as: "add_sec_to_user"
	patch '/user_update/:id', to: 'user_files/users#update', as: "update_a_user"

#   Suggested Rules
	post "/sug_save", to: "fire_wall_files/sug_rules#edit_sug_save", as: "edit_sug_save"
	post "/sug_cancel", to: "fire_wall_files/sug_rules#edit_sug_cancel", as: "edit_sug_cancel"

	patch "/add_sug_rules/:id", to: "fire_wall_files/sug_rules#add_sug_rules", as: "add_sug_rules" # change the value of suggested rule submit_sug tag
	get "/add_sug_rules/:id", to: "fire_wall_files/sug_rules#add_sug_rules", as: "add_sug_rules_get" # change the value of suggested rule submit_sug tag
	get "/create_fw_rules", to: "fire_wall_files/sug_rules#create_fw_rules", as: "create_fw_rules"
	get '/delete_item/:rule_id/:type/:type_id/:which_controller', to: "fire_wall_files/sug_rules#delete_sug_item", as: "delete_sug_item"
	post '/create_sug_rule', to: 'fire_wall_files/sug_rules#add_new_sug_rule', as: 'add_new_sug_rule'

#   Apps - DISREGARD: apps have been made redundant and so removed from front end 
	get "/app/:id/get_app_fw_rules", to: "app#get_app_fw_rules", as: "get_app_fw_rules" # get route for imported app rules
	get "/create_app_sec_gp_and_fw_rules", to: "app#create_app_sec_gp_and_fw_rules", as: "sec_gp_and_fw_rules"

#   Email
	get '/new_confirmation_email', to: "confirm_mail_mailer#new_confirmation_email", as: "new_confirmation_email"

#   Admin Dash
    get '/dashboard/:type', to: "user_files/users#admin_dashboard", as: "admin_dashboard"
    get '/all_histories', to: "user_files/users#admin_dash_all_histories", as: "admin_dash_all_histories"
    get '/metrics', to: "user_files/users#admin_dash_sys_and_tasks", as: "admin_dash_sys_and_tasks"
    get '/all_flows', to: "user_files/users#admin_dash_flows", as: "admin_dash_flows"

#   Security Tags
	post '/search_sec_tags', to: "sec_tags#search_security_tags", as: "search_security_tags"

#   Security Groups - now the only immediate child of users (VRNI apps are redundant)
	get '/sec_gps/', to: "security_files/sec_groups#index", as: "get_user_sec_group_home"
	get "/sec_group/:id/get_assoc_fw_rules", to: "security_files/sec_groups#get_assoc_fw_rules", as: "get_assoc_fw_rules" # get route for existing security group rules
	get "/sec_group/:id/override_or_create_rules", to: "security_files/sec_groups#override_or_create_rules", as: "override_or_create_rules"
	get '/sug_rule/:id/modify', to: 'security_files/sec_groups#add_more_sys_objects', as: "add_more_sys_objects"
	get '/sug_rule/:id/edit_show', to: 'fire_wall_files/sug_rules#open_sys_obj_partial', as: "open_sys_obj_partial"
	post '/new_group', to: "security_files/sec_groups#create_sg", as: "create_sg"
	post '/apply_sec_tag', to: "security_files/sec_groups#apply_sec_tag", as: "apply_sec_tag"
	get '/add_to_ports', to: 'protocol_files/port_ranges#add_to_ports_column', as: "add_to_ports_column"
	get '/add_to_ports/:template_id/:vmware_obj_id/:rule_id/:which_controller', to: 'protocol_files/port_ranges#add_to_service_table', as: "add_to_service_table"



	get '/search/:type/:template_id/:rule_id/:which_controller', to: 'application#show_widget', as: "show_widget"
	get '/searchbar/:template_id/:rule_id', to: 'vm#search_vmware_sys_objs', as: 'search_vmware_sys_objs'
	get '/delete/sug/:id', to: 'security_files/sec_groups#delete_assoc_sug_rule', as: "delete_sug_rule"
	get '/secgp/:id/dest/tk', to: "security_files/sec_groups#user_sec_group_destroy", as: "delete_task"
	get '/secgp/:id/dest/sg', to: "security_files/sec_groups#delete_security_group", as: "delete_security_group"

end
